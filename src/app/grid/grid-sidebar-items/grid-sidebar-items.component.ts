import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs";
import * as GridSelectors from "../../grid/store/grid.selectors";
import {GridItem} from "../store/grid.reducer";
import {GridSidebarItemsFormComponent} from "../grid-sidebar-items-form/grid-sidebar-items-form.component";

@Component({
  selector: 'app-grid-sidebar-items',
  standalone: true,
  imports: [CommonModule, GridSidebarItemsFormComponent],
  templateUrl: './grid-sidebar-items.component.html',
  styleUrls: ['./grid-sidebar-items.component.scss']
})
export class GridSidebarItemsComponent {
  private store = inject(Store);

  selectedItem$!: Observable<GridItem | null>;

  ngOnInit() {
    this.selectedItem$ = this.store.pipe(select(GridSelectors.selectSelectedItem));
  }
}
