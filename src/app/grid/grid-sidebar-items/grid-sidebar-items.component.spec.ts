import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridSidebarItemsComponent } from './grid-sidebar-items.component';

describe('GridSidebarItemsComponent', () => {
  let component: GridSidebarItemsComponent;
  let fixture: ComponentFixture<GridSidebarItemsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GridSidebarItemsComponent]
    });
    fixture = TestBed.createComponent(GridSidebarItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
