import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridSidebarContainerComponent } from './grid-sidebar-container.component';

describe('GridSidebarContainerComponent', () => {
  let component: GridSidebarContainerComponent;
  let fixture: ComponentFixture<GridSidebarContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GridSidebarContainerComponent]
    });
    fixture = TestBed.createComponent(GridSidebarContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
