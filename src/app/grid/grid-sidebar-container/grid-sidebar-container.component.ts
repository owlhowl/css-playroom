import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Observable} from "rxjs";
import {GridContainer} from "../store/grid.reducer";
import {select, Store} from "@ngrx/store";
import * as GridSelectors from "../store/grid.selectors";
import {GridSidebarContainerFormComponent} from "../grid-sidebar-container-form/grid-sidebar-container-form.component";

@Component({
  selector: 'app-grid-sidebar-container',
  standalone: true,
  imports: [CommonModule, GridSidebarContainerFormComponent],
  templateUrl: './grid-sidebar-container.component.html',
  styleUrls: ['./grid-sidebar-container.component.scss']
})
export class GridSidebarContainerComponent implements OnInit {
  private store = inject(Store);
  gridContainer$!: Observable<GridContainer>;

  ngOnInit() {
    this.gridContainer$ = this.store.pipe(select(GridSelectors.selectGridContainer));
  }
}
