import {Component, inject, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Store} from "@ngrx/store";
import {CodeService} from "../shared/services/code.service";
import {Observable} from "rxjs";
import {ActiveTab} from "./store/grid.reducer";
import * as GridSelectors from "../grid/store/grid.selectors";
import * as GridActions from "../grid/store/grid.actions";
import {CodeContainerComponent} from "../shared/ui/code-container/code-container.component";
import {GridSidebarComponent} from "./grid-sidebar/grid-sidebar.component";
import {GridContainerComponent} from "./grid-container/grid-container.component";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-grid',
  standalone: true,
  imports: [CommonModule, CodeContainerComponent, GridSidebarComponent, GridContainerComponent],
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit, OnDestroy {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);
  private store = inject(Store);
  private codeService = inject(CodeService);

  activeTab$!: Observable<ActiveTab>;
  showCodeContainer$!: Observable<boolean>;
  gridHtml$ = this.codeService.gridHtml$;
  gridCss$ = this.codeService.gridCss$;

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Grid');
    this.metaTagService.addTags([
      {name: 'description', content: 'cssplayroom.com is your first and best source for the css information you’re looking for. Add div items, set css grid properties, visualize the output and get the css code'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Grid, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
    this.activeTab$ = this.store.select(GridSelectors.selectActiveTab);
    this.showCodeContainer$ = this.store.select(GridSelectors.selectShowCodeContainer);
  }

  ngOnDestroy() {
    this.store.dispatch(GridActions.resetDefaults());
  }
}
