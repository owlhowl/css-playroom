import {
  AfterContentChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  inject,
  OnDestroy,
  OnInit
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {map, max, Observable, Subject, takeUntil, tap} from "rxjs";
import {GridContainer, GridItem} from "../store/grid.reducer";
import * as GridSelectors from "../store/grid.selectors";
import {faXmark} from "@fortawesome/free-solid-svg-icons";
import * as GridActions from "../store/grid.actions";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

@Component({
  selector: 'app-grid-container',
  standalone: true,
  imports: [CommonModule, FontAwesomeModule],
  templateUrl: './grid-container.component.html',
  styleUrls: ['./grid-container.component.scss']
})
export class GridContainerComponent implements OnInit, AfterViewInit, AfterContentChecked, OnDestroy {
  private cdref = inject(ChangeDetectorRef);
  private store = inject(Store);
  gridContainer$!: Observable<GridContainer>;
  gridItems$!: Observable<GridItem[]>;
  selectedItem$!: Observable<GridItem | null>;
  gridContainer!: GridContainer;
  gridTemplateColumns!: string;
  gridTemplateRows!: string;
  gridAutoColumns!: string;
  gridAutoRows!: string;
  extraRows = '';
  extraColumns = '';
  totalContainers: any[] = [];

  faDelete = faXmark;
  destroy$ = new Subject<void>;

  ngOnInit() {
    this.gridContainer$ = this.store.pipe(
        select(GridSelectors.selectGridState),
        tap(state => {
          this.gridContainer = state.container;
          this.gridTemplateColumns = this.gridContainer.gridTemplateColumns.map((item) => {
            return item.value + item.unit;
          }).join(' ');
          this.gridTemplateRows = this.gridContainer.gridTemplateRows.map((item) => {
            return item.value + item.unit;
          }).join(' ');
          this.gridAutoColumns = this.gridContainer.gridAutoColumns.map((item) => {
            return item.value + item.unit;
          }).join(' ');
          this.gridAutoRows = this.gridContainer.gridAutoRows.map((item) => {
            return item.value + item.unit;
          }).join(' ');
        }),
        map(state => state.container),
    );

    this.gridItems$ = this.store.pipe(
        select(GridSelectors.selectGridState),
        tap(state => {
          this.extraColumns = '';
          this.extraRows = '';
            let colLength = this.gridContainer.gridTemplateColumns.length;
            let rowLength = this.gridContainer.gridTemplateRows.length;
            state.items.map(item => {
                if (item.properties.gridColumnEnd > colLength + 1) {
                    colLength = item.properties.gridColumnEnd - 1;
                }
                if (item.properties.gridRowEnd > rowLength + 1) {
                    rowLength = item.properties.gridRowEnd - 1;
                }
            });

            if (colLength > this.gridContainer.gridTemplateColumns.length && this.gridContainer.gridAutoFlow == 'row') {
              // this.extraColumns = '';
              let extra = colLength - this.gridContainer.gridTemplateColumns.length;
              for (let i = 0; i < Math.floor(extra/this.gridContainer.gridAutoColumns.length); i++) {
                this.extraColumns += this.gridContainer.gridAutoColumns.map((item) => {
                  return item.value + item.unit;
                }).join(' ') + ' ';
              }
              for (let j = 0; j < extra % this.gridContainer.gridAutoColumns.length; j++) {
                this.extraColumns += ' ' + this.gridContainer.gridAutoColumns[j].value + this.gridContainer.gridAutoColumns[j].unit;
              }
            }

            if (rowLength > this.gridContainer.gridTemplateRows.length && this.gridContainer.gridAutoFlow == 'column') {
              this.extraRows = '';
              let extra = rowLength - this.gridContainer.gridTemplateRows.length;
              for (let i = 0; i < Math.floor(extra/this.gridContainer.gridAutoRows.length); i++) {
                this.extraRows += this.gridContainer.gridAutoRows.map((item) => {
                  return item.value + item.unit;
                }).join(' ') + ' ';
              }
              for (let j = 0; j < extra % this.gridContainer.gridAutoRows.length; j++) {
                this.extraRows += ' ' + this.gridContainer.gridAutoRows[j].value + this.gridContainer.gridAutoRows[j].unit;
              }
            }

              const maxCount =  colLength * rowLength;
              if (state.items.length > maxCount) {
                if (this.gridContainer.gridAutoFlow == 'column') {
                  this.totalContainers = Array(Math.ceil(state.items.length / rowLength) * rowLength);
                } else {
                  this.totalContainers = Array(Math.ceil(state.items.length / colLength) * colLength);
                }
              } else {
                this.totalContainers = Array(maxCount??0);
              }
            }),
        map(state => state.items)
    );

    this.selectedItem$ = this.store.pipe(select(GridSelectors.selectSelectedItem));
  }

  ngAfterViewInit() {
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  setSelectedItem(index: string) {
    this.store.dispatch(GridActions.setActiveTab({value: 'items'}));
    this.store.dispatch(GridActions.setSelectedItem({id: index}));
  }

  removeGridItem(id: string) {
    this.store.dispatch(GridActions.removeGridItem({id: id}));
  }
}
