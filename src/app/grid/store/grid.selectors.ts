import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromGrid from "../../grid/store/grid.reducer";

export const selectGridState = createFeatureSelector<fromGrid.State>(
    fromGrid.gridFeatureKey
);

export const selectActiveTab = createSelector(
    selectGridState,
    (state: fromGrid.State) => state.activeTab,
);

export const selectShowCodeContainer = createSelector(
    selectGridState,
    (state: fromGrid.State) => state.showCodeContainer,
);

export const selectGridContainer = createSelector(
    selectGridState,
    (state: fromGrid.State) => state.container,
);

export const selectGridItems = createSelector(
    selectGridState,
    (state: fromGrid.State) => state.items,
);


export const selectSelectedItem = createSelector(
    selectGridState,
    (state: fromGrid.State) => state.selectedItem,
);
