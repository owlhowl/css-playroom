import { createAction, props } from "@ngrx/store";
import {ActiveTab, GridContainer, GridItem, GridItemProperties} from "./grid.reducer";

export const setActiveTab = createAction(
    '[Grid] Set active tab',
    props<{value: ActiveTab}>(),
);

export const addGridItem = createAction(
    '[Grid] Add grid item',
);

export const resetDefaults = createAction(
    '[Grid] Reset defaults',
);

export const toggleCodeContainer = createAction(
    '[Grid] Toggle Code Container',
);

export const setGridContainer = createAction(
    '[Grid] Set grid container',
    props<{value: GridContainer}>(),
);

export const setSelectedItem = createAction(
    '[Grid] Set selected item',
    props<{id: string}>()
)

export const removeGridItem = createAction(
    '[Grid] Remove grid item',
    props<{id: string}>()
)

export const updateSelectedItem = createAction(
    '[Grid] Update selected item',
    props<{value: GridItemProperties}>(),
);
