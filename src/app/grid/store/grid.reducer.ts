import {createReducer, on} from '@ngrx/store';
import {UnitPicker} from "../../shared/models/unit-picker";
import * as GridActions from "./grid.actions";
import {v4 as uuidv4} from "uuid";


const gridDisplay = ['grid', 'inline-grid'] as const;
const gridJustifyItems = ['stretch', 'start', 'end', 'center'] as const;
const gridAlignItems = ['stretch', 'start', 'end', 'center', 'baseline'] as const;
const gridJustifyContent = [
    'start', 'end', 'center', 'stretch', 'space-around', 'space-between', 'space-evenly'
] as const;
const gridAlignContent = [
    'start', 'end', 'center', 'stretch', 'space-around', 'space-between', 'space-evenly'
] as const;
const gridAutoFlow = ['row', 'column'] as const;
const gridJustifySelf = ['initial', 'stretch', 'start', 'end', 'center'] as const;
const gridAlignSelf = ['initial', 'stretch', 'start', 'end', 'center'] as const;

export type GridDisplay = typeof gridDisplay[number];
export type GridJustifyItems = typeof gridJustifyItems[number];
export type GridAlignItems = typeof gridAlignItems[number];
export type GridJustifyContent = typeof gridJustifyContent[number];
export type GridAlignContent = typeof gridAlignContent[number];
export type GridAutoFlow = typeof gridAutoFlow[number];
export type GridJustifySelf = typeof gridJustifySelf[number];
export type GridAlignSelf = typeof gridAlignSelf[number];

export const gridSelectProperties = {
  display: [...gridDisplay],
  justifyItems: [...gridJustifyItems],
  alignItems: [...gridAlignItems],
  justifyContent: [...gridJustifyContent],
  alignContent: [...gridAlignContent],
  gridAutoFlow: [...gridAutoFlow],
}

export const gridItemProperties = {
  justifySelf: [...gridJustifySelf],
  alignSelf: [...gridAlignSelf],
}

export interface GridContainer {
  gridTemplateColumns: UnitPicker[],
  columnLines: string|null[],
  gridTemplateRows: UnitPicker[],
  rowLines: string|null[],
  gridTemplateAreas: string[][],
  columnGap: UnitPicker,
  rowGap: UnitPicker,
  display: GridDisplay,
  justifyItems: GridJustifyItems,
  alignItems: GridAlignItems,
  justifyContent: GridJustifyContent,
  alignContent: GridAlignContent,
  gridAutoColumns: UnitPicker[],
  gridAutoRows: UnitPicker[],
  gridAutoFlow: GridAutoFlow,
}

export interface GridItemProperties {
  gridArea: string,
  gridColumnStart: number,
  gridRowStart: number,
  gridColumnEnd: number,
  gridRowEnd: number,
  justifySelf: GridJustifySelf | null,
  alignSelf: GridAlignSelf | null,
}

export interface GridItem {
  id: string,
  properties: GridItemProperties,
}

export type ActiveTab = 'container' | 'items';

export const gridFeatureKey = 'grid';

export interface State {
  container: GridContainer;
  items: GridItem[],
  selectedItem: GridItem | null,
  activeTab: ActiveTab,
  showCodeContainer: boolean,
}

function computeItemPosition(
    existingItems: GridItem[], nrRows: number, nrColumns: number, flow: GridAutoFlow
) {
  let maxColumns = nrColumns, maxRows = nrRows;
  existingItems.map(item => {
    if (item.properties.gridColumnEnd > maxColumns + 1) {
      maxColumns = item.properties.gridColumnEnd - 1;
    }
    if (item.properties.gridRowEnd > maxRows + 1) {
      maxRows = item.properties.gridRowEnd - 1;
    }
  });
  const grid: number[][] = new Array(maxRows).fill(0).map(() => new Array(maxColumns).fill(0));
  if (flow == 'row') {
    for (const item of existingItems) {
      for (let row = item.properties.gridRowStart - 1; row < item.properties.gridRowEnd - 1; row++) {
        for (let col = item.properties.gridColumnStart - 1; col < item.properties.gridColumnEnd -1; col++) {
          grid[row][col] = 1;
        }
      }
    }

    for (let row = 0; row < maxRows; row++) {
      for (let col = 0; col < maxColumns; col++) {
        if (grid[row][col] === 0) {
          return [col + 1, row + 1, col + 2, row + 2];
        }
      }
    }
  } else {
    for (const item of existingItems) {
      for (let col = item.properties.gridColumnStart - 1; col < item.properties.gridColumnEnd - 1; col++) {
        for (let row = item.properties.gridRowStart - 1; row < item.properties.gridRowEnd - 1; row++) {
          grid[row][col] = 1;
        }
      }
    }

    for (let col = 0; col < maxColumns; col++) {
      for (let row = 0; row < maxRows; row++) {
        if (grid[row][col] === 0) {
          return [col + 1, row + 1, col + 2, row + 2];
        }
      }
    }
  }

  let gridColumnStart, gridRowStart, gridColumnEnd, gridRowEnd;
  if (flow == 'column') {
    gridColumnStart = maxColumns + 1;
    gridRowStart = 1;
    gridColumnEnd = maxColumns + 2;
    gridRowEnd = 2;
  } else {
    gridColumnStart = 1;
    gridRowStart = maxRows + 1;
    gridColumnEnd = 2;
    gridRowEnd = maxRows + 2;
  }

  return [gridColumnStart, gridRowStart, gridColumnEnd, gridRowEnd];
}

const initialState: State = {
  container: {
    gridTemplateColumns: [
      {value: 1, unit: 'fr'},
      {value: 1, unit: 'fr'},
      {value: 1, unit: 'fr'},
      {value: 1, unit: 'fr'},
    ],
    columnLines: [],
    gridTemplateRows: [
      {value: 1, unit: 'fr'},
      {value: 1, unit: 'fr'},
      {value: 1, unit: 'fr'},
    ],
    rowLines: [],
    gridTemplateAreas: [],
    columnGap: {
      value: 0,
      unit: 'px',
    },
    rowGap: {
      value: 0,
      unit: 'px',
    },
    display: gridDisplay[0],
    justifyItems: gridJustifyItems[0],
    alignItems: gridAlignItems[0],
    justifyContent: gridJustifyContent[0],
    alignContent: gridAlignContent[0],
    gridAutoColumns: [{
      value: 30,
      unit: 'px'
    }],
    gridAutoRows: [{
      value: 30,
      unit: 'px'
    }],
    gridAutoFlow: gridAutoFlow[0],
  },
  items: [],
  selectedItem: null,
  activeTab: 'container',
  showCodeContainer: false,
};

export const gridReducer = createReducer(
  initialState,
  on(GridActions.setGridContainer, (state, action) => {
    let itemsToKeep: GridItem[] = [];
    let itemsToReposition: GridItem[] = [];
    state.items.map((item, index) => {
        if (
            Math.abs(item.properties.gridColumnEnd - item.properties.gridColumnStart) > 1 ||
            Math.abs(item.properties.gridRowEnd - item.properties.gridRowStart) > 1
        ) {
          itemsToKeep.push(item);
        } else {
          itemsToReposition.push(item);
        }
    });

    for (let item of itemsToReposition) {
      const [newColumnStart, newRowStart, newColumnEnd, newRowEnd] = computeItemPosition(
          itemsToKeep,
          action.value.gridTemplateRows.length,
          action.value.gridTemplateColumns.length,
          action.value.gridAutoFlow,
      );

      itemsToKeep.push({
        ...item,
        properties: {
          ...item.properties,
          gridColumnStart: newColumnStart,
          gridRowStart: newRowStart,
          gridColumnEnd: newColumnEnd,
          gridRowEnd: newRowEnd,
        }
      })
    }

    return {
      ...state,
      container: {
        ...state.container,
        ...action.value
      },
      items: action.value.gridTemplateColumns.length == 0 || action.value.gridTemplateRows.length == 0 ? [] : itemsToKeep
    }
  }),
  on(GridActions.addGridItem, (state, action) => {
    const [newColumnStart, newRowStart, newColumnEnd, newRowEnd] = computeItemPosition(
        [...state.items],
        state.container.gridTemplateRows.length,
        state.container.gridTemplateColumns.length,
        state.container.gridAutoFlow,
    );

    return {
      ...state,
      items: [
          ...state.items, {
          id: uuidv4(),
          properties: {
            gridColumnStart: newColumnStart,
            gridRowStart: newRowStart,
            gridColumnEnd: newColumnEnd,
            gridRowEnd: newRowEnd,
            gridArea: '.',
            justifySelf: gridJustifySelf[0],
            alignSelf: gridAlignSelf[0],
          }
        }
      ]
    }
  }),
  on(GridActions.removeGridItem, (state, action) => {
    return {
      ...state,
      selectedItem: state.selectedItem?.id == action.id ? null : state.selectedItem as GridItem,
      items: state.items.filter(item => item.id != action.id)
    }
  }),
  on(GridActions.setActiveTab, (state, action) => {
    return {
      ...state,
      activeTab: action.value,
    }
  }),
  on(GridActions.setSelectedItem, (state, action) => {
    const selectedItem = state.selectedItem;
    const foundItem = state.items.find(item => item.id === action.id) as GridItem;

    return {
      ...state,
      selectedItem: selectedItem?.id === action.id ? null : {
        ...foundItem,
        properties: {
          ...foundItem.properties
        }
      },
    };
  }),
  on(GridActions.updateSelectedItem, (state, action) => {
    const updatedProperties = {
      gridColumnStart: action.value.gridColumnStart,
      gridRowStart: action.value.gridRowStart,
      gridColumnEnd:  action.value.gridColumnEnd,
      gridRowEnd: action.value.gridRowEnd,
      gridArea: action.value.gridArea,
      justifySelf:  action.value.justifySelf,
      alignSelf:  action.value.alignSelf,
    };

    return {
      ...state,
      items: [...state.items.map(item =>
          item.id === state.selectedItem?.id ? { ...item, properties: updatedProperties } : item
      )],
      selectedItem: {
        ...state.selectedItem,
        properties: {
          ...state.selectedItem?.properties,
          ...updatedProperties
        } as GridItemProperties,
      } as GridItem
    };
  }),
  on(GridActions.toggleCodeContainer, (state) => {
    return {
      ...state,
      showCodeContainer: !state.showCodeContainer,
    }
  }),
  on(GridActions.resetDefaults, () => initialState),
);
