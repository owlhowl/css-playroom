import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridSidebarNavComponent } from './grid-sidebar-nav.component';

describe('GridSidebarNavComponent', () => {
  let component: GridSidebarNavComponent;
  let fixture: ComponentFixture<GridSidebarNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GridSidebarNavComponent]
    });
    fixture = TestBed.createComponent(GridSidebarNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
