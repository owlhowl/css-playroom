import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {ActiveTab} from "../store/grid.reducer";
import {Observable} from "rxjs";
import * as GridSelectors from "../../grid/store/grid.selectors";
import * as GridActions from "../../grid/store/grid.actions";
import {GridSidebarNavTabComponent} from "../grid-sidebar-nav-tab/grid-sidebar-nav-tab.component";

@Component({
  selector: 'app-grid-sidebar-nav',
  standalone: true,
  imports: [CommonModule, GridSidebarNavTabComponent],
  templateUrl: './grid-sidebar-nav.component.html',
  styleUrls: ['./grid-sidebar-nav.component.scss']
})
export class GridSidebarNavComponent implements  OnInit {
  private store = inject(Store);

  tabs: {
    name: string,
    key: ActiveTab
  }[] = [
    { name: 'Container', key: 'container', },
    { name: 'Items', key: 'items' },
  ];
  activeTab$!: Observable<string>;
  showCodeContainer$!: Observable<boolean>;

  ngOnInit() {
    this.activeTab$ = this.store.pipe(select(GridSelectors.selectActiveTab));
    this.showCodeContainer$ = this.store.pipe(select(GridSelectors.selectShowCodeContainer));
  }

  setActiveTab(key: ActiveTab) {
    this.store.dispatch(GridActions.setActiveTab({value: key}));
  }

  resetDefaults() {
    this.store.dispatch(GridActions.resetDefaults());
  }

  addGridItem() {
    this.store.dispatch(GridActions.addGridItem());
  }

  toggleCodeContainer() {
    this.store.dispatch(GridActions.toggleCodeContainer());
  }
}
