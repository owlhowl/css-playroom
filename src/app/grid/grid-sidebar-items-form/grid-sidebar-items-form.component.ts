import {Component, inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';
import {GridItem, gridItemProperties} from "../store/grid.reducer";
import {FormBuilder, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Store} from "@ngrx/store";
import {Subject, takeUntil, tap} from "rxjs";
import * as GridActions from "../../grid/store/grid.actions";

@Component({
  selector: 'app-grid-sidebar-items-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './grid-sidebar-items-form.component.html',
  styleUrls: ['./grid-sidebar-items-form.component.scss']
})
export class GridSidebarItemsFormComponent implements OnChanges, OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() selectedItem!: GridItem;
  selectedItemForm!: FormGroup;
  justifySelfOptions = gridItemProperties.justifySelf;
  alignSelfOptions = gridItemProperties.alignSelf;
  destroy$: Subject<void> = new Subject();

  ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedItem'] && !changes['selectedItem'].firstChange) {
      this.selectedItemForm.patchValue(changes['selectedItem']?.currentValue.properties, {
        onlySelf: true, emitEvent: false
      });
    }
  }

  ngOnInit() {
    this.selectedItemForm = this.fb.group({
      gridArea: [this.selectedItem.properties.gridArea],
      gridColumnStart: [this.selectedItem.properties.gridColumnStart],
      gridRowStart: [this.selectedItem.properties.gridRowStart],
      gridColumnEnd: [this.selectedItem.properties.gridColumnEnd],
      gridRowEnd: [this.selectedItem.properties.gridRowEnd],
      justifySelf: [this.selectedItem.properties.justifySelf],
      alignSelf: [this.selectedItem.properties.alignSelf],
    });

    this.selectedItemForm.valueChanges.pipe(
        takeUntil(this.destroy$),
        tap((formValue) => {
          this.store.dispatch(GridActions.updateSelectedItem({value: formValue}));
        }),
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
