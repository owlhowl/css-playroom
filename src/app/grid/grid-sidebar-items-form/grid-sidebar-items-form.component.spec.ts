import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridSidebarItemsFormComponent } from './grid-sidebar-items-form.component';

describe('GridSidebarItemsFormComponent', () => {
  let component: GridSidebarItemsFormComponent;
  let fixture: ComponentFixture<GridSidebarItemsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GridSidebarItemsFormComponent]
    });
    fixture = TestBed.createComponent(GridSidebarItemsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
