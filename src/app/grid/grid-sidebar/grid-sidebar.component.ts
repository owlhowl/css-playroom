import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import * as GridSelectors from "../../grid/store/grid.selectors";
import {GridSidebarNavComponent} from "../grid-sidebar-nav/grid-sidebar-nav.component";
import {GridSidebarContainerComponent} from "../grid-sidebar-container/grid-sidebar-container.component";
import {GridSidebarItemsComponent} from "../grid-sidebar-items/grid-sidebar-items.component";

@Component({
  selector: 'app-grid-sidebar',
  standalone: true,
  imports: [CommonModule, GridSidebarNavComponent, GridSidebarContainerComponent, GridSidebarItemsComponent],
  templateUrl: './grid-sidebar.component.html',
  styleUrls: ['./grid-sidebar.component.scss']
})
export class GridSidebarComponent {
  private store = inject(Store);

  activeTab$!: Observable<string>;

  ngOnInit() {
    this.activeTab$ = this.store.select(GridSelectors.selectActiveTab);
  }
}
