import {Component, inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {CommonModule, KeyValue} from '@angular/common';
import {GridAutoFlow, GridContainer, gridSelectProperties} from "../store/grid.reducer";
import {FormArray, FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Subject, takeUntil, tap} from "rxjs";
import {Store} from "@ngrx/store";
import * as GridActions from "../../grid/store/grid.actions";
import {UnitPicker} from "../../shared/models/unit-picker";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";
import {faPlus, faTrashCan} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {
  FlexboxSidebarContainerSelectComponent
} from "../../flexbox/flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {FormControlPipe} from "../../shared/pipes/form-control.pipe";
import {kebabize} from "../../shared/utils/kebabize";

type PartialGridContainer = Pick<GridContainer, 'display' | 'justifyItems' | 'alignItems' | 'justifyContent' | 'alignContent'>;

@Component({
  selector: 'app-grid-sidebar-container-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, UnitPickerComponent, FontAwesomeModule, FlexboxSidebarContainerSelectComponent, FormControlPipe],
  templateUrl: './grid-sidebar-container-form.component.html',
  styleUrls: ['./grid-sidebar-container-form.component.scss']
})
export class GridSidebarContainerFormComponent implements OnChanges, OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() gridContainer!: GridContainer;
  gridPropertiesForm!: FormGroup;
  gridContainerSelects!: PartialGridContainer;
  gridProperties = gridSelectProperties;
  destroy$: Subject<void> = new Subject();

  protected readonly faTrashCan = faTrashCan;
  protected readonly faPlus = faPlus;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['gridContainer'] && !changes['gridContainer'].firstChange) {
      this.gridPropertiesForm.patchValue(changes['gridContainer']['currentValue'], {
        onlySelf: true, emitEvent: false
      });
    }

  }

  ngOnInit() {
    let gridTemplateColumns = this.fb.array<FormGroup>([]);
    for (let column of this.gridContainer.gridTemplateColumns) {
      const columnGroup = this.fb.group({
        value: [column.value],
        unit: [column.unit],
      });
      gridTemplateColumns.push(columnGroup);
    }

    let gridTemplateRows = this.fb.array<FormGroup>([]);
    for (let row of this.gridContainer.gridTemplateRows) {
      const rowGroup = this.fb.group({
        value: [row.value],
        unit: [row.unit],
      });
      gridTemplateRows.push(rowGroup);
    }

    let gridAutoColumns = this.fb.array<FormGroup>([]);
    for (let column of this.gridContainer.gridAutoColumns) {
      const autoColumnGroup = this.fb.group({
        value: [column.value],
        unit: [column.unit],
      });
      gridAutoColumns.push(autoColumnGroup);
    }

    let gridAutoRows = this.fb.array<FormGroup>([]);
    for (let row of this.gridContainer.gridAutoRows) {
      const autoRowGroup = this.fb.group({
        value: [row.value],
        unit: [row.unit],
      });
      gridAutoRows.push(autoRowGroup);
    }

    this.gridPropertiesForm = this.fb.group({
      gridTemplateColumns: gridTemplateColumns,
      columnLines: this.fb.array([]),
      gridTemplateRows: gridTemplateRows,
      rowLines: this.fb.array([]),
      rowGap: this.fb.group({
        value: [this.gridContainer.rowGap.value],
        unit: [this.gridContainer.rowGap.unit],
      }),
      columnGap: this.fb.group({
        value: [this.gridContainer.columnGap.value],
        unit: [this.gridContainer.columnGap.unit],
      }),
      display: [this.gridContainer.display],
      justifyItems: [this.gridContainer.justifyItems],
      alignItems: [this.gridContainer.alignItems],
      justifyContent: [this.gridContainer.justifyContent],
      alignContent: [this.gridContainer.alignContent],
      gridAutoColumns: gridAutoColumns,
      gridAutoRows: gridAutoRows,
      gridAutoFlow: [this.gridContainer.gridAutoFlow],
    });

    this.gridContainerSelects = {
      display: this.gridContainer.display,
      justifyItems: this.gridContainer.justifyItems,
      alignItems: this.gridContainer.alignItems,
      justifyContent: this.gridContainer.justifyContent,
      alignContent: this.gridContainer.alignContent,
    }

    this.gridPropertiesForm.valueChanges.pipe(
        takeUntil(this.destroy$),
        tap((formValue) => {
          this.store.dispatch(GridActions.setGridContainer({value: formValue}));
        })
    ).subscribe();
  }

  ngOnDestroy() {
  }

  get gridTemplateColumns() {
    return (<FormArray<FormGroup>>this.gridPropertiesForm.get('gridTemplateColumns')).controls;
  }

  get gridTemplateRows() {
    return (<FormArray<FormGroup>>this.gridPropertiesForm.get('gridTemplateRows')).controls;
  }

  get gridAutoColumns() {
    return (<FormArray<FormGroup>>this.gridPropertiesForm.get('gridAutoColumns')).controls;
  }

  get gridAutoRows() {
    return (<FormArray<FormGroup>>this.gridPropertiesForm.get('gridAutoRows')).controls;
  }

  addGridTemplateItem(name: string) {
    (<FormArray>this.gridPropertiesForm.get(name)).push(
      this.fb.group({
        value: [1],
        unit: ['fr'],
      })
    );
  }

  getFormGroup(name: string) {
    return this.gridPropertiesForm.get(name) as FormGroup;
  }

  getFormControl(name: string) {
    return this.gridPropertiesForm.get(name) as FormControl;
  }

  onDeleteColumn(index: number) {
    (<FormArray>this.gridPropertiesForm.get('gridTemplateColumns')).removeAt(index);
  }

  onDeleteRow(index: number) {
    (<FormArray>this.gridPropertiesForm.get('gridTemplateRows')).removeAt(index);
  }

  onDeleteAutoColumn(index: number) {
    (<FormArray>this.gridPropertiesForm.get('gridAutoColumns')).removeAt(index);
  }

  onDeleteAutoRow(index: number) {
    (<FormArray>this.gridPropertiesForm.get('gridAutoRows')).removeAt(index);
  }

  kebabize = kebabize;

  originalOrder = (a: KeyValue<keyof PartialGridContainer, string>, b: KeyValue<keyof PartialGridContainer, string>): number => {
    return 0;
  }
}
