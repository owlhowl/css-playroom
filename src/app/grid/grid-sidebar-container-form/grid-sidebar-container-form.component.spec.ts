import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridSidebarContainerFormComponent } from './grid-sidebar-container-form.component';

describe('GridSidebarContainerFormComponent', () => {
  let component: GridSidebarContainerFormComponent;
  let fixture: ComponentFixture<GridSidebarContainerFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GridSidebarContainerFormComponent]
    });
    fixture = TestBed.createComponent(GridSidebarContainerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
