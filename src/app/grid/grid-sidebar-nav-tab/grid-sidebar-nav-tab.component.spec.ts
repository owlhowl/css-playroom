import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridSidebarNavTabComponent } from './grid-sidebar-nav-tab.component';

describe('GridSidebarNavTabComponent', () => {
  let component: GridSidebarNavTabComponent;
  let fixture: ComponentFixture<GridSidebarNavTabComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GridSidebarNavTabComponent]
    });
    fixture = TestBed.createComponent(GridSidebarNavTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
