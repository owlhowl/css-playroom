import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-grid-sidebar-nav-tab',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './grid-sidebar-nav-tab.component.html',
  styleUrls: ['./grid-sidebar-nav-tab.component.scss']
})
export class GridSidebarNavTabComponent {
  @Input() name!: string;
  @Input() isActive!: boolean;
}
