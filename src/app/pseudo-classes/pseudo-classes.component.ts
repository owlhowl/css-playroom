import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Store} from "@ngrx/store";
import {CodeService} from "../shared/services/code.service";
import {Observable} from "rxjs";
import * as PseudoClassesSelectors from "../pseudo-classes/store/pseudo-classes.selectors";
import * as PseudoClassesActions from "../pseudo-classes/store/pseudo-classes.actions";
import {CodeContainerComponent} from "../shared/ui/code-container/code-container.component";
import {PseudoClassesSidebarComponent} from "./pseudo-classes-sidebar/pseudo-classes-sidebar.component";
import {PseudoClassesContainerComponent} from "./pseudo-classes-container/pseudo-classes-container.component";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-pseudo-classes',
  standalone: true,
  imports: [CommonModule, CodeContainerComponent, PseudoClassesSidebarComponent, PseudoClassesContainerComponent],
  templateUrl: './pseudo-classes.component.html',
  styleUrls: ['./pseudo-classes.component.scss']
})
export class PseudoClassesComponent implements OnInit {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);
  private store = inject(Store);
  private codeService = inject(CodeService);

  showCodeContainer$!: Observable<boolean>;
  pseudoClassesHtml$ = this.codeService.pseudoClassesHtml$;
  pseudoClassesCss$ = this.codeService.pseudoClassesCss$;

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Pseudo Classes');
    this.metaTagService.addTags([
      {name: 'description', content: 'cssplayroom.com is your first and best source for the css information you’re looking for. Select a pseudo class and visualise its effect in different circumstances'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Pseudo Classes, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
    this.showCodeContainer$ = this.store.select(PseudoClassesSelectors.selectShowCodeContainer);
  }

  ngOnDestroy() {
    this.store.dispatch(PseudoClassesActions.resetDefaults());
  }
}
