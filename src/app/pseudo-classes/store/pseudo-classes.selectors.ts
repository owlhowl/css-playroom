import {createFeatureSelector, createSelector} from "@ngrx/store";
import * as fromPseudoClasses from "../../pseudo-classes/store/pseudo-classes.reducer";

export const selectPseudoClassesState = createFeatureSelector<fromPseudoClasses.State>(
  fromPseudoClasses.pseudoClassesFeatureKey
);

export const selectPseudoClassType = createSelector(
  selectPseudoClassesState,
  (state: fromPseudoClasses.State) => state.pseudoClassType,
);

export const selectPseudoClass = createSelector(
  selectPseudoClassesState,
  (state: fromPseudoClasses.State) => state.pseudoClass,
);

export const selectPseudoClassOptions = createSelector(
  selectPseudoClassesState,
  (state: fromPseudoClasses.State) => state.pseudoClassOptions,
);

export const selectCssOptions = createSelector(
  selectPseudoClassesState,
  (state: fromPseudoClasses.State) => state.cssOptions,
);

export const selectShowCodeContainer = createSelector(
  selectPseudoClassesState,
  (state: fromPseudoClasses.State) => state.showCodeContainer,
);
