import {createReducer, on} from '@ngrx/store';
import {Border, BorderStyle} from '../../shared/models/border';
import * as PseudoClassesActions from '../../pseudo-classes/store/pseudo-classes.actions';
import {UnitPicker} from "../../shared/models/unit-picker";

export const elementDisplayStatePseudoClasses = ['fullscreen', 'modal', 'picture-in-picture'] as const;
export const inputPseudoClasses = [
  'autofill', 'enabled', 'disabled', 'read-only', 'read-write', 'placeholder-shown', 'default', 'checked',
  'indeterminate', 'valid', 'invalid', 'in-range', 'out-of-range', 'required', 'optional',
] as const;
export const locationPseudoClasses = [
  'any-link', 'link', 'visited', 'scope'
] as const;
export const treeStructuralPseudoClasses = [
  'empty', 'nth-child', 'nth-last-child', 'first-child', 'last-child', 'only-child', 'nth-of-type',
  'nth-last-of-type', 'first-of-type', 'last-of-type', 'only-of-type'
] as const;
export const userActionPseudoClasses = [
  'hover', 'active', 'focus', 'focus-visible', 'focus-within'
] as const;
export const functionalPseudoClasses = ['is', 'not', 'where', 'has'] as const;
export const pseudoClassTypes = [
  'elementDisplayState', 'input', 'location', 'treeStructural', 'userAction'
] as const;

export type ElementDisplayStatePseudoClass = typeof elementDisplayStatePseudoClasses[number];
export type InputPseudoClass = typeof inputPseudoClasses[number];
export type LocationPseudoClass = typeof locationPseudoClasses[number];
export type TreeStructuralPseudoClass = typeof treeStructuralPseudoClasses[number];
export type UserActionPseudoClass = typeof userActionPseudoClasses[number];
export type FunctionalPseudoClass = typeof functionalPseudoClasses[number];

export type PseudoClassType = typeof pseudoClassTypes[number];
export type PseudoClass = ElementDisplayStatePseudoClass | InputPseudoClass | LocationPseudoClass | TreeStructuralPseudoClass
  | UserActionPseudoClass | FunctionalPseudoClass;

export const pseudoClassMapping: Record<PseudoClassType, readonly PseudoClass[]> = {
  elementDisplayState: elementDisplayStatePseudoClasses,
  input: inputPseudoClasses,
  location: locationPseudoClasses,
  treeStructural: treeStructuralPseudoClasses,
  userAction: userActionPseudoClasses,
};

export type CssOptions = {
  backgroundColor: string,
  color: string,
  border: Border,
}
export interface State {
  pseudoClassType: PseudoClassType,
  pseudoClassOptions: PseudoClass[],
  pseudoClass: PseudoClass,
  cssOptions: CssOptions,
  showCodeContainer: boolean,
}

export const pseudoClassesFeatureKey = 'pseudoClasses';

const initialState: State = {
  pseudoClassType: [...pseudoClassTypes][0],
  pseudoClassOptions: [...elementDisplayStatePseudoClasses],
  pseudoClass: elementDisplayStatePseudoClasses[0],
  cssOptions: {
    backgroundColor: '#fed7aa',
    color: '#6b7280',
    border: {
      size: {
        value: 2,
        unit: 'px',
      },
      style: 'solid',
      color: '#c2410c',
    }
  },
  showCodeContainer: false,
};

export const pseudoClassesReducer = createReducer(
  initialState,
  on(PseudoClassesActions.setPseudoClassType, (state, action) => {
    return {
      ...state,
      pseudoClassType: action.value,
      pseudoClass: pseudoClassMapping[action.value][0],
      pseudoClassOptions: [...pseudoClassMapping[action.value]],
    }
  }),
  on(PseudoClassesActions.setPseudoClass, (state, action) => {
    return {
      ...state,
      pseudoClass: action.value
    }
  }),
  on(PseudoClassesActions.setCssOptions, (state, action) => {
    return {
      ...state,
      cssOptions: action.value
    }
  }),
  on(PseudoClassesActions.toggleCodeContainer, (state) => {
    return {
      ...state,
      showCodeContainer: !state.showCodeContainer,
    }
  }),
  on(PseudoClassesActions.resetDefaults, () => initialState),
);
