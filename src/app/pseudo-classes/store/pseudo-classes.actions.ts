import {createAction, props} from "@ngrx/store";
import {CssOptions, PseudoClass, PseudoClassType} from "./pseudo-classes.reducer";

export const setPseudoClassType = createAction(
  '[Pseudo Classes] Set pseudo class type',
  props<{value: PseudoClassType}>()
);

export const setPseudoClass = createAction(
  '[Pseudo Classes] Set pseudo class',
  props<{value: PseudoClass}>()
);

export const setCssOptions = createAction(
  '[Pseudo Classes] Set css options',
  props<{value: CssOptions}>()
)

export const resetDefaults = createAction(
  '[Pseudo Classes] Reset defaults'
);

export const toggleCodeContainer = createAction(
  '[Pseudo Classes] Toggle Code Container',
);

