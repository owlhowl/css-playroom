import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import {CssOptions, PseudoClass, PseudoClassType} from "../store/pseudo-classes.reducer";
import * as PseudoClassesSelectors from "../store/pseudo-classes.selectors";
import {
  TransformsSidebarItemFormComponent
} from "../../transforms/transforms-sidebar-item-form/transforms-sidebar-item-form.component";
import {PseudoClassesItemFormComponent} from "../pseudo-classes-item-form/pseudo-classes-item-form.component";

@Component({
  selector: 'app-pseudo-classes-item',
  standalone: true,
  imports: [CommonModule, TransformsSidebarItemFormComponent, PseudoClassesItemFormComponent],
  templateUrl: './pseudo-classes-item.component.html',
  styleUrls: ['./pseudo-classes-item.component.scss']
})
export class PseudoClassesItemComponent implements OnInit {
  private store = inject(Store);

  pseudoClassTypeState$!: Observable<PseudoClassType>;
  pseudoClassState$!: Observable<PseudoClass>;
  pseudoClassOptionsState$!: Observable<PseudoClass[]>;
  pseudoCssOptionsState$!: Observable<CssOptions>;

  ngOnInit() {
    this.pseudoClassTypeState$ = this.store.pipe(select(PseudoClassesSelectors.selectPseudoClassType));
    this.pseudoClassState$ = this.store.pipe(select(PseudoClassesSelectors.selectPseudoClass));
    this.pseudoClassOptionsState$ = this.store.pipe(select(PseudoClassesSelectors.selectPseudoClassOptions));
    this.pseudoCssOptionsState$ = this.store.pipe(select(PseudoClassesSelectors.selectCssOptions));
  }
}
