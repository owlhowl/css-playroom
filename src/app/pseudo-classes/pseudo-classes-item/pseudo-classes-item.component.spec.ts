import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PseudoClassesItemComponent } from './pseudo-classes-item.component';

describe('PseudoClassesItemComponent', () => {
  let component: PseudoClassesItemComponent;
  let fixture: ComponentFixture<PseudoClassesItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PseudoClassesItemComponent]
    });
    fixture = TestBed.createComponent(PseudoClassesItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
