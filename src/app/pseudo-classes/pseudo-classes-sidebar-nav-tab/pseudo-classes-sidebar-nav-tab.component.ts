import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-pseudo-classes-sidebar-nav-tab',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './pseudo-classes-sidebar-nav-tab.component.html',
  styleUrls: ['./pseudo-classes-sidebar-nav-tab.component.scss']
})
export class PseudoClassesSidebarNavTabComponent {
  @Input() name!: string;
  @Input() isActive!: boolean;
}
