import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PseudoClassesSidebarNavTabComponent } from './pseudo-classes-sidebar-nav-tab.component';

describe('PseudoClassesSidebarNavTabComponent', () => {
  let component: PseudoClassesSidebarNavTabComponent;
  let fixture: ComponentFixture<PseudoClassesSidebarNavTabComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PseudoClassesSidebarNavTabComponent]
    });
    fixture = TestBed.createComponent(PseudoClassesSidebarNavTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
