import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PseudoClassesSidebarComponent } from './pseudo-classes-sidebar.component';

describe('PseudoClassesSidebarComponent', () => {
  let component: PseudoClassesSidebarComponent;
  let fixture: ComponentFixture<PseudoClassesSidebarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PseudoClassesSidebarComponent]
    });
    fixture = TestBed.createComponent(PseudoClassesSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
