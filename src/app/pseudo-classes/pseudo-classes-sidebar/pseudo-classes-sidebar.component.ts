import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PseudoClassesSidebarNavComponent} from "../pseudo-classes-sidebar-nav/pseudo-classes-sidebar-nav.component";
import {PseudoClassesItemComponent} from "../pseudo-classes-item/pseudo-classes-item.component";

@Component({
  selector: 'app-pseudo-classes-sidebar',
  standalone: true,
  imports: [CommonModule, PseudoClassesSidebarNavComponent, PseudoClassesItemComponent],
  templateUrl: './pseudo-classes-sidebar.component.html',
  styleUrls: ['./pseudo-classes-sidebar.component.scss']
})
export class PseudoClassesSidebarComponent {

}
