import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PseudoClassesSidebarNavComponent } from './pseudo-classes-sidebar-nav.component';

describe('PseudoClassesSidebarNavComponent', () => {
  let component: PseudoClassesSidebarNavComponent;
  let fixture: ComponentFixture<PseudoClassesSidebarNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PseudoClassesSidebarNavComponent]
    });
    fixture = TestBed.createComponent(PseudoClassesSidebarNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
