import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs";
import * as PseudoClassesSelectors from "../../pseudo-classes/store/pseudo-classes.selectors";
import * as PseudoClassesActions from "../../pseudo-classes/store/pseudo-classes.actions";
import {
  PseudoClassesSidebarNavTabComponent
} from "../pseudo-classes-sidebar-nav-tab/pseudo-classes-sidebar-nav-tab.component";

@Component({
  selector: 'app-pseudo-classes-sidebar-nav',
  standalone: true,
  imports: [CommonModule, PseudoClassesSidebarNavTabComponent],
  templateUrl: './pseudo-classes-sidebar-nav.component.html',
  styleUrls: ['./pseudo-classes-sidebar-nav.component.scss']
})
export class PseudoClassesSidebarNavComponent {
  private store = inject(Store);

  showCodeContainer$!: Observable<boolean>;

  ngOnInit() {
    this.showCodeContainer$ = this.store.pipe(select(PseudoClassesSelectors.selectShowCodeContainer));
  }

  resetDefaults() {
    this.store.dispatch(PseudoClassesActions.resetDefaults());
  }

  toggleCodeContainer() {
    this.store.dispatch(PseudoClassesActions.toggleCodeContainer());
  }
}
