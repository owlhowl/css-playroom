import {Component, ElementRef, HostBinding, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import {CssOptions, PseudoClass, PseudoClassType} from "../../store/pseudo-classes.reducer";

@Component({
  selector: 'app-fullscreen',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './fullscreen.component.html',
  styleUrls: ['./fullscreen.component.scss']
})
export class FullscreenComponent implements OnChanges {
  @Input() cssOptions!: any;
  @ViewChild('fullscreenContainer') fullScreenContainer!: ElementRef<HTMLElement>;
  @HostBinding('style.--background-color') backgroundColor!: string;
  @HostBinding('style.--color') color!: string;
  @HostBinding('style.--border') border!: string;

  ngOnChanges(changes:SimpleChanges) {
    this.backgroundColor = this.cssOptions.backgroundColor;
    this.color = this.cssOptions.color;
    this.border = `${this.cssOptions.border.size.value}${this.cssOptions.border.size.unit} ${this.cssOptions.border.style} ${this.cssOptions.border.color}`;
  }

  toggleFullScreen() {
    if (document.fullscreenElement) {
      document.exitFullscreen();
      return;
    }

    this.fullScreenContainer.nativeElement.requestFullscreen();
  }
}
