import {Component, ElementRef, HostBinding, inject, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {CssOptions, PseudoClass, PseudoClassType} from "../store/pseudo-classes.reducer";
import * as PseudoClassesSelectors from "../store/pseudo-classes.selectors";
import {PseudoClassesItemFormComponent} from "../pseudo-classes-item-form/pseudo-classes-item-form.component";
import {FullscreenComponent} from "./fullscreen/fullscreen.component";
import {PictureInPictureComponent} from "./picture-in-picture/picture-in-picture.component";
import {ModalComponent} from "./modal/modal.component";
import {InputComponent} from "./input/input.component";
import {LocationComponent} from "./location/location.component";
import {TreeStructuralComponent} from "./tree-structural/tree-structural.component";
import {UserActionComponent} from "./user-action/user-action.component";

@Component({
  selector: 'app-pseudo-classes-container',
  standalone: true,
    imports: [CommonModule, PseudoClassesItemFormComponent, ModalComponent, FullscreenComponent, PictureInPictureComponent, InputComponent, LocationComponent, TreeStructuralComponent, UserActionComponent],
  templateUrl: './pseudo-classes-container.component.html',
  styleUrls: ['./pseudo-classes-container.component.scss']
})
export class PseudoClassesContainerComponent {
  private store = inject(Store);

  pseudoClassTypeState$!: Observable<PseudoClassType>;
  pseudoClassState$!: Observable<PseudoClass>;
  pseudoCssOptionsState$!: Observable<CssOptions>;

  ngOnInit() {
    this.pseudoClassTypeState$ = this.store.pipe(select(PseudoClassesSelectors.selectPseudoClassType));
    this.pseudoClassState$ = this.store.pipe(select(PseudoClassesSelectors.selectPseudoClass));
    this.pseudoCssOptionsState$ = this.store.pipe(select(PseudoClassesSelectors.selectCssOptions));
  }
}
