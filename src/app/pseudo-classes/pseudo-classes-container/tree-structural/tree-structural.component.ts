import {Component, HostBinding, Input, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-tree-structural',
  standalone: true,
  imports: [CommonModule, FontAwesomeModule],
  templateUrl: './tree-structural.component.html',
  styleUrls: ['./tree-structural.component.scss']
})
export class TreeStructuralComponent {
  @Input() cssOptions!: any;
  @Input() pseudoClass!: string;

  @HostBinding('style.--background-color') backgroundColor!: string;
  @HostBinding('style.--color') color!: string;
  @HostBinding('style.--border') border!: string;

  numbers = Array(16).fill(0).map((x,i)=>i+1);

  ngOnChanges(changes:SimpleChanges) {
    this.backgroundColor = this.cssOptions.backgroundColor;
    this.color = this.cssOptions.color;
    this.border = `${this.cssOptions.border.size.value}${this.cssOptions.border.size.unit} ${this.cssOptions.border.style} ${this.cssOptions.border.color}`;
  }
}
