import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeStructuralComponent } from './tree-structural.component';

describe('TreeStructuralComponent', () => {
  let component: TreeStructuralComponent;
  let fixture: ComponentFixture<TreeStructuralComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TreeStructuralComponent]
    });
    fixture = TestBed.createComponent(TreeStructuralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
