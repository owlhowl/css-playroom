import {Component, ElementRef, HostBinding, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-modal',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnChanges {
  @Input() cssOptions!: any;
  @ViewChild('dialog') dialog!: ElementRef<HTMLDialogElement>;
  @HostBinding('style.--background-color') backgroundColor!: string;
  @HostBinding('style.--color') color!: string;
  @HostBinding('style.--border') border!: string;

  ngOnChanges(changes:SimpleChanges) {
    this.backgroundColor = this.cssOptions.backgroundColor;
    this.color = this.cssOptions.color;
    this.border = `${this.cssOptions.border.size.value}${this.cssOptions.border.size.unit} ${this.cssOptions.border.style} ${this.cssOptions.border.color}`;
  }

  openModal() {
    this.dialog.nativeElement.showModal();
  }
}
