import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-input',
  standalone: true,
    imports: [CommonModule, FormsModule],
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnChanges {
  @Input() cssOptions!: any;
  @Input() pseudoClass!: string;

  @HostBinding('style.--background-color') backgroundColor!: string;
  @HostBinding('style.--color') color!: string;
  @HostBinding('style.--border') border!: string;

  ngOnChanges(changes:SimpleChanges) {
    this.backgroundColor = this.cssOptions.backgroundColor;
    this.color = this.cssOptions.color;
    this.border = `${this.cssOptions.border.size.value}${this.cssOptions.border.size.unit} ${this.cssOptions.border.style} ${this.cssOptions.border.color}`;
  }
}
