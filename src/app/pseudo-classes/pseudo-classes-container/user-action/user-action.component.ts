import {Component, HostBinding, Input, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-user-action',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './user-action.component.html',
  styleUrls: ['./user-action.component.scss']
})
export class UserActionComponent {
  @Input() cssOptions!: any;
  @Input() pseudoClass!: string;

  @HostBinding('style.--background-color') backgroundColor!: string;
  @HostBinding('style.--color') color!: string;
  @HostBinding('style.--border') border!: string;

  ngOnChanges(changes:SimpleChanges) {
    this.backgroundColor = this.cssOptions.backgroundColor;
    this.color = this.cssOptions.color;
    this.border = `${this.cssOptions.border.size.value}${this.cssOptions.border.size.unit} ${this.cssOptions.border.style} ${this.cssOptions.border.color}`;
  }
}
