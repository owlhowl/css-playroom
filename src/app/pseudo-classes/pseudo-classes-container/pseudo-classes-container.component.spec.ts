import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PseudoClassesContainerComponent } from './pseudo-classes-container.component';

describe('PseudoClassesContainerComponent', () => {
  let component: PseudoClassesContainerComponent;
  let fixture: ComponentFixture<PseudoClassesContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PseudoClassesContainerComponent]
    });
    fixture = TestBed.createComponent(PseudoClassesContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
