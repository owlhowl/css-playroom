import {Component, ElementRef, HostBinding, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-picture-in-picture',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './picture-in-picture.component.html',
  styleUrls: ['./picture-in-picture.component.scss']
})
export class PictureInPictureComponent implements OnChanges {
  @Input() cssOptions!: any;
  @ViewChild('video') video!: ElementRef<HTMLVideoElement>;
  @HostBinding('style.--background-color') backgroundColor!: string;
  @HostBinding('style.--color') color!: string;
  @HostBinding('style.--border') border!: string;

  ngOnChanges(changes:SimpleChanges) {
    this.backgroundColor = this.cssOptions.backgroundColor;
    this.color = this.cssOptions.color;
    this.border = `${this.cssOptions.border.size.value}${this.cssOptions.border.size.unit} ${this.cssOptions.border.style} ${this.cssOptions.border.color}`;
  }

  async togglePictureMode() {
    if (this.video.nativeElement !== document.pictureInPictureElement) {
      await this.video.nativeElement.requestPictureInPicture();
    } else {
      await document.exitPictureInPicture();
    }
  }
}
