import {Component, ElementRef, HostBinding, inject, Input, SimpleChanges, ViewChild} from '@angular/core';
import {CommonModule, ViewportScroller} from '@angular/common';
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {first} from "rxjs";

@Component({
  selector: 'app-location',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent {
  private viewportScroller = inject(ViewportScroller);
  private route = inject(ActivatedRoute);
  @Input() cssOptions!: any;
  @Input() pseudoClass!: string;

  @HostBinding('style.--background-color') backgroundColor!: string;
  @HostBinding('style.--color') color!: string;
  @HostBinding('style.--border') border!: string;

  ngOnChanges(changes:SimpleChanges) {
    this.backgroundColor = this.cssOptions.backgroundColor;
    this.color = this.cssOptions.color;
    this.border = `${this.cssOptions.border.size.value}${this.cssOptions.border.size.unit} ${this.cssOptions.border.style} ${this.cssOptions.border.color}`;
  }

  ngAfterViewInit(): void {
    this.route.fragment.pipe(
    ).subscribe(fragment => {
      console.log(fragment);
      if(fragment) {
        this.viewportScroller.scrollToAnchor(fragment)
      }
    });
  }
}
