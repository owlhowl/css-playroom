import {Component, inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  CssOptions,
  PseudoClass,
  pseudoClassMapping,
  PseudoClassType,
  pseudoClassTypes
} from "../store/pseudo-classes.reducer";
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Store} from "@ngrx/store";
import {Subject, takeUntil, tap} from "rxjs";
import {kebabize} from "../../shared/utils/kebabize";
import * as PseudoClassesActions from "../../pseudo-classes/store/pseudo-classes.actions";
import {
  FlexboxSidebarContainerSelectComponent
} from "../../flexbox/flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";
import {borderStyles} from "../../shared/models/border";

@Component({
  selector: 'app-pseudo-classes-item-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, FlexboxSidebarContainerSelectComponent, UnitPickerComponent],
  templateUrl: './pseudo-classes-item-form.component.html',
  styleUrls: ['./pseudo-classes-item-form.component.scss']
})
export class PseudoClassesItemFormComponent implements OnChanges, OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() pseudoClassTypeState!: PseudoClassType;
  @Input() pseudoClassState!: PseudoClass;
  @Input() pseudoClassOptionsState!: PseudoClass[];
  @Input() pseudoCssOptionsState!: CssOptions;

  typeFormControl!: FormControl;
  classFormControl!: FormControl;
  cssOptionsFormGroup!: FormGroup;
  pseudoClassTypes = [...pseudoClassTypes];
  borderStyles = [...borderStyles];
  destroy$: Subject<void> = new Subject();
  kebabize = kebabize;

  ngOnChanges(changes: SimpleChanges) {
    if (
        changes['pseudoClassTypeState'] && !changes['pseudoClassTypeState'].firstChange
    ) {
      this.typeFormControl.patchValue(changes['pseudoClassTypeState'].currentValue, {emitEvent: false});
    }

    if (
        changes['pseudoClassState'] && !changes['pseudoClassState'].firstChange
    ) {
      this.classFormControl.patchValue(changes['pseudoClassState'].currentValue, {emitEvent: false});
    }
  }

  ngOnInit() {
    this.typeFormControl = new FormControl<any>(this.pseudoClassTypeState);
    this.classFormControl = new FormControl<any>(this.pseudoClassState);
    this.cssOptionsFormGroup = this.fb.group({
      backgroundColor: this.fb.control(this.pseudoCssOptionsState.backgroundColor),
      color: this.fb.control(this.pseudoCssOptionsState.color),
      border: this.fb.group({
        size: this.fb.group({
          value: this.fb.control(this.pseudoCssOptionsState.border.size.value),
          unit: this.fb.control(this.pseudoCssOptionsState.border.size.unit),
        }),
        style: this.fb.control(this.pseudoCssOptionsState.border.style),
        color: this.fb.control(this.pseudoCssOptionsState.border.color),
      }),
    });

    this.typeFormControl.valueChanges.pipe(
      takeUntil(this.destroy$),
      tap((formValue) => {
        this.store.dispatch(PseudoClassesActions.setPseudoClassType({value: formValue}));
      }),
    ).subscribe();

    this.classFormControl.valueChanges.pipe(
      takeUntil(this.destroy$),
      tap((formValue) => {
        this.store.dispatch(PseudoClassesActions.setPseudoClass({value: formValue}));
      }),
    ).subscribe();

    this.cssOptionsFormGroup.valueChanges.pipe(
      takeUntil(this.destroy$),
      tap((formValue) => {
        this.store.dispatch(PseudoClassesActions.setCssOptions({value: formValue}));
      }),
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getFormGroup(name: string) {
    return this.cssOptionsFormGroup.get(name) as FormGroup;
  }

  getFormControl(name: string) {
    return this.cssOptionsFormGroup.get(name) as FormControl;
  }
}
