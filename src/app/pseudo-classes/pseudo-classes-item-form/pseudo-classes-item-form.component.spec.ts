import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PseudoClassesItemFormComponent } from './pseudo-classes-item-form.component';

describe('PseudoClassesItemFormComponent', () => {
  let component: PseudoClassesItemFormComponent;
  let fixture: ComponentFixture<PseudoClassesItemFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PseudoClassesItemFormComponent]
    });
    fixture = TestBed.createComponent(PseudoClassesItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
