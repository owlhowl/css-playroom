import {createReducer, on} from "@ngrx/store";
import * as AnimationActions from "../store/animations.actions";
import {v4 as uuidv4} from 'uuid';
import {act} from "@ngrx/effects";
import * as FlexboxAction from "../../flexbox/store/flexbox.actions";

const timeUnits = ['s', 'ms'] as const;
export const animationTimingFunctions = ['ease', 'ease-in', 'ease-out', 'ease-in-out', 'linear', 'step-start', 'step-end'] as const;
export const animationDirections = ['normal', 'reverse', 'alternate', 'alternate-reverse'] as const;
export const animationFillModes = ['none', 'forwards', 'backwards', 'both'] as const;
export const animationPlayStates = ['running', 'paused'] as const;
export const infinite = 'infinite' as const;

export type AnimationIterationCount = number | typeof infinite;
export type TimeUnits = typeof timeUnits[number];
export type AnimationTimingFunctions = typeof animationTimingFunctions[number];
export type AnimationDirection = typeof animationDirections[number];
export type AnimationFillMode = typeof animationFillModes[number];
export type AnimationPlayState = typeof animationPlayStates[number];

export const animationTypes = [
  'translateX', 'translateY', 'opacity', 'background', 'scale', 'resize', 'rotate'
] as const;
export type AnimationType = typeof animationTypes[number];

type AnimationValueMappings = {
  translateX: { value: number; unit: string };
  translateY: { value: number; unit: string };
  opacity: number;
  background: string;
  scale: number;
  resize: { width: { value: number; unit: string }; height: { value: number; unit: string } };
  rotate: { value: number; unit: string };
};

export type AnimationValues = Partial<Record<AnimationType, AnimationValueMappings[AnimationType]>>;
interface FilteredProperties {
  trueValues: Partial<Record<AnimationType, boolean>>;
  falseValues: Partial<Record<AnimationType, boolean>>;
}
export type AnimationKeyFrame = {
  id: string,
  frame: number;
  value: AnimationValues;
};

export const animationProperties = {
  animation: {
    animationDuration: {
      value: 1,
      units: timeUnits,
    },
    animationDelay: {
      value: 0,
      units: timeUnits
    },
    animationTimingFunction: animationTimingFunctions,
    animationFillMode: animationFillModes,
    animationDirection: animationDirections,
    animationPlayState: animationPlayStates,
    animationIterationCount: 1,
    animationName: 'animation-playground',
    animationTempName: uuidv4(),
  },
  animationTypes: animationTypes,
  animationKeyframes: {
    translateX: {
      value: 0,
      unit: 'px',
    },
    translateY: {
      value: 0,
      unit: 'px',
    },
    opacity: 1,
    background: '#fb923c',
    scale: 1.0,
    resize: {
      width: {
        value: 100,
        unit: 'px'
      },
      height: {
        value: 50,
        unit: 'px'
      }
    },
    rotate: {
      value: 0,
      unit: 'deg'
    },
  },
}

export interface AnimationState {
  animationDuration: {
    value: number,
    unit: TimeUnits,
  },
  animationTimingFunction: AnimationTimingFunctions,
  animationDelay: {
    value: number,
    unit: TimeUnits,
  },
  animationIterationCount: AnimationIterationCount,
  animationDirection: AnimationDirection,
  animationFillMode: AnimationFillMode,
  animationPlayState: AnimationPlayState,
  animationName: string,
  animationTempName: string,
}

export type ActiveTab = 'animation' | 'keyframes';

export const animationsFeatureKey = 'animations';

export interface State {
  animation: AnimationState,
  animationTypes: Partial<Record<AnimationType, boolean>>,
  keyframes: AnimationKeyFrame[],
  activeTab: ActiveTab,
  showCodeContainer: boolean,
}

const initialState: State = {
  animation: {
    animationDuration: {
      value: animationProperties.animation.animationDuration.value,
      unit: animationProperties.animation.animationDuration.units[0],
    },
    animationDelay: {
      value: animationProperties.animation.animationDelay.value,
      unit: animationProperties.animation.animationDelay.units[0],
    },
    animationTimingFunction: animationProperties.animation.animationTimingFunction[0],
    animationDirection: animationProperties.animation.animationDirection[0],
    animationFillMode: animationProperties.animation.animationFillMode[0],
    animationPlayState: animationProperties.animation.animationPlayState[0],
    animationIterationCount: animationProperties.animation.animationIterationCount,
    animationName: animationProperties.animation.animationName,
    animationTempName: uuidv4(),
  },
  animationTypes: {
    'translateX': true,
    'translateY': true,
    'opacity': false,
    'background': false,
    'scale': false,
    'resize': false,
    'rotate': false,
  },
  keyframes: [
    {
      id: uuidv4(),
      frame: 0,
      value: {
        translateX: {
          value: -100,
          unit: 'px',
        },
        translateY: {
          value: -300,
          unit: 'px',
        }
      }
    },
    {
      id: uuidv4(),
      frame: 100,
      value: {
        translateX: {
          value: 50,
          unit: 'px',
        },
        translateY: {
          value: 50,
          unit: 'px',
        },
      }
    }
  ],
  activeTab: 'animation' as const,
  showCodeContainer: false,
}

export const animationsReducer = createReducer(
  initialState,
  on(AnimationActions.setActiveTab, (state, action) => {
    return {
      ...state,
      activeTab: action.value,
    }
  }),
  on(AnimationActions.setAnimation, (state, action) => {
    return {
      ...state,
      animation: {
        ...state.animation,
        animationDuration: {
          ...state.animation.animationDuration,
          value: action.value.animationDuration.value,
          unit: action.value.animationDuration.unit
        },
        animationDelay: {
          ...state.animation.animationDelay,
          value: action.value.animationDelay.value,
          unit: action.value.animationDelay.unit
        },
        animationTimingFunction: action.value.animationTimingFunction,
        animationDirection: action.value.animationDirection,
        animationFillMode: action.value.animationFillMode,
        animationPlayState: action.value.animationPlayState,
        animationIterationCount: action.value.animationIterationCount == 'infinite' ? 'infinite' as const : +action.value.animationIterationCount,
        animationName: action.value.animationName,
        animationTempName: uuidv4(),
      }
    }
  }),
  on(AnimationActions.setAnimationTypes, (state, action) => {
    const { trueValues, falseValues }: FilteredProperties = Object.entries(action.value).reduce(
      (result, [key, value]) => {
        const animationType = key as AnimationType;
        result[value ? 'trueValues' : 'falseValues'][animationType] = value as boolean;
        return result;
      },
      { trueValues: {}, falseValues: {} } as FilteredProperties
    );

    return {
      ...state,
      animationTypes: action.value,
      animation: {
        ...state.animation,
        animationTempName: uuidv4(),
      },
      keyframes: [
        ...state.keyframes.map((keyframe) => {
          keyframe = JSON.parse(JSON.stringify(keyframe));
          for (const key of Object.keys(falseValues) as AnimationType[]) {
            if(keyframe.value[key] != null || keyframe.value[key] != undefined) {
              delete keyframe.value[key];
            }
           }

          for (const key of Object.keys(trueValues) as AnimationType[]) {
            if(!keyframe.value[key]) {
              keyframe.value[key] = animationProperties.animationKeyframes[key];
            }
          }

          return keyframe;
        }).sort(function(a, b) {
          return a.frame - b.frame;
        })
      ]
    }
  }),
  on(AnimationActions.setKeyframes, (state, action) => {
    return {
      ...state,
      keyframes: action.value
    };
  }),
  on(AnimationActions.addKeyframe, (state) => {
    const { trueValues, falseValues }: FilteredProperties = Object.entries(state.animationTypes).reduce(
      (result, [key, value]) => {
        const animationType = key as AnimationType;
        result[value ? 'trueValues' : 'falseValues'][animationType] = value as boolean;
        return result;
      },
      { trueValues: {}, falseValues: {} } as FilteredProperties
    );

    let newKeyframe: AnimationKeyFrame = {
      id: uuidv4(),
      frame: Math.floor((state.keyframes[1].frame - state.keyframes[0].frame ) / 2),
      value: {},
    }

    for (const key of Object.keys(trueValues) as AnimationType[]) {
      newKeyframe.value[key] = animationProperties.animationKeyframes[key];
    }

    return {
      ...state,
      keyframes: [
        ...state.keyframes,
        newKeyframe
      ].sort(function(a, b) {
        return a.frame - b.frame;
      })
    };
  }),
  on(AnimationActions.deleteKeyframe, (state, action) => {
    return {
      ...state,
      keyframes: state.keyframes.filter(item => item.id != action.id),
    }
  }),
  on(AnimationActions.toggleCodeContainer, (state) => {
    return {
      ...state,
      showCodeContainer: !state.showCodeContainer,
    }
  }),
  on(AnimationActions.resetDefaults, () => initialState),
);
