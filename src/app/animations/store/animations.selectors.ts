import {createFeatureSelector, createSelector} from "@ngrx/store";
import * as fromAnimations from "../../animations/store/animations.reducer";

export const selectAnimationsState = createFeatureSelector<fromAnimations.State>(
  fromAnimations.animationsFeatureKey
);

export const selectAnimation = createSelector(
  selectAnimationsState,
  (state: fromAnimations.State) => state.animation,
);

export const selectKeyframes = createSelector(
  selectAnimationsState,
  (state: fromAnimations.State) => state.keyframes,
);

export const selectAnimationsTypes = createSelector(
  selectAnimationsState,
  (state: fromAnimations.State) => state.animationTypes,
);

export const selectActiveTab = createSelector(
  selectAnimationsState,
  (state: fromAnimations.State) => state.activeTab,
);

export const selectShowCodeContainer = createSelector(
  selectAnimationsState,
  (state: fromAnimations.State) => state.showCodeContainer,
);
