import {createAction, props} from "@ngrx/store";
import {ActiveTab, AnimationKeyFrame, AnimationState, AnimationType} from "./animations.reducer";

export const setAnimation = createAction(
  '[Animations] Set animation',
  props<{value: AnimationState}>()
);

export const getAnimation = createAction(
  '[Animations] Get animation',
);

export const setActiveTab = createAction(
  '[Animations] Set active tab',
  props<{value: ActiveTab}>()
);

export const getActiveTab = createAction(
  '[Animations] Get active tab',
);

export const setAnimationTypes = createAction(
  '[Animations] Set animation types',
  props<{value: Partial<Record<AnimationType, boolean>>}>()
);

export const setKeyframes = createAction(
  '[Animations] Set keyframes',
  props<{value: AnimationKeyFrame[]}>()
);

export const addKeyframe = createAction(
  '[Animations] Add keyframe',
);

export const deleteKeyframe = createAction(
  '[Animations] Delete keyframe',
  props<{id: string}>(),
);

export const getAnimationTypes = createAction(
  '[Animations] Get animation types',
);

export const resetDefaults = createAction(
  '[Animations] Reset defaults'
);

export const toggleCodeContainer = createAction(
  '[Animations] Toggle Code Container',
);
