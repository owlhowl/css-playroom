import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-animations-sidebar-animation-form-select',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './animations-sidebar-animation-form-select.component.html',
  styleUrls: ['./animations-sidebar-animation-form-select.component.scss']
})
export class AnimationsSidebarAnimationFormSelectComponent {

}
