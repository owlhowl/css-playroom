import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsSidebarAnimationFormSelectComponent } from './animations-sidebar-animation-form-select.component';

describe('SidebarAnimationFormSelectComponent', () => {
  let component: AnimationsSidebarAnimationFormSelectComponent;
  let fixture: ComponentFixture<AnimationsSidebarAnimationFormSelectComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AnimationsSidebarAnimationFormSelectComponent]
    });
    fixture = TestBed.createComponent(AnimationsSidebarAnimationFormSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
