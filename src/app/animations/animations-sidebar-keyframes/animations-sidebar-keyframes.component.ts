import {Component, inject, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Observable, Subject} from "rxjs";
import {AnimationKeyFrame, AnimationState, AnimationType, animationTypes} from "../store/animations.reducer";
import {select, Store} from "@ngrx/store";
import * as AnimationSelectors from "../store/animations.selectors";
import {
  AnimationsSidebarKeyframesFormComponent
} from "../animations-sidebar-keyframes-form/animations-sidebar-keyframes-form.component";

@Component({
  selector: 'app-animations-sidebar-keyframes',
  standalone: true,
  imports: [CommonModule, AnimationsSidebarKeyframesFormComponent],
  templateUrl: './animations-sidebar-keyframes.component.html',
  styleUrls: ['./animations-sidebar-keyframes.component.scss']
})
export class AnimationsSidebarKeyframesComponent implements OnInit, OnDestroy {
  private store = inject(Store);
  animationTypes$!: Observable<Partial<Record<AnimationType, boolean>>>;
  allAnimationTypes = [...animationTypes];
  keyframes$!: Observable<AnimationKeyFrame[]>;
  destroy$: Subject<void> = new Subject();

  ngOnInit() {
    this.animationTypes$ = this.store.pipe(select(AnimationSelectors.selectAnimationsTypes));
    this.keyframes$ = this.store.pipe(select(AnimationSelectors.selectKeyframes));
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
