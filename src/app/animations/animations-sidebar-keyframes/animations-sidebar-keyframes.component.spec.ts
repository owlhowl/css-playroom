import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsSidebarKeyframesComponent } from './animations-sidebar-keyframes.component';

describe('SidebarKeyframesComponent', () => {
  let component: AnimationsSidebarKeyframesComponent;
  let fixture: ComponentFixture<AnimationsSidebarKeyframesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AnimationsSidebarKeyframesComponent]
    });
    fixture = TestBed.createComponent(AnimationsSidebarKeyframesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
