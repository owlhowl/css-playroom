import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ReplayAnimationService {
  replayAnimation$: Subject<void> = new Subject();

  constructor() { }
}
