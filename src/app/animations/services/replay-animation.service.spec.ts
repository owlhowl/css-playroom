import { TestBed } from '@angular/core/testing';

import { ReplayAnimationService } from './replay-animation.service';

describe('ReplayAnimationService', () => {
  let service: ReplayAnimationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReplayAnimationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
