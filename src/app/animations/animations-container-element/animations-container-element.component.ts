import {Component, ElementRef, inject, Input, OnChanges, Renderer2, SimpleChanges, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AnimationKeyFrame, animationProperties, AnimationState, AnimationValues} from "../store/animations.reducer";
import {animate, keyframes, state, style, transition, trigger} from "@angular/animations";
import {ReplayAnimationService} from "../services/replay-animation.service";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";
import {delay, tap} from "rxjs";

@Component({
  selector: 'app-animations-container-element',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './animations-container-element.component.html',
  styleUrls: ['./animations-container-element.component.scss'],
  styles: [],
})
export class AnimationsContainerElementComponent implements OnChanges {
  private renderer = inject(Renderer2);
  private el = inject(ElementRef);
  private replayService = inject(ReplayAnimationService);
  @Input() animations!: AnimationState;
  @Input() keyframes!: AnimationKeyFrame[];
  @ViewChild('animatedContainer') animatedContainer!: ElementRef<HTMLHtmlElement>;

  constructor() {
    this.replayService.replayAnimation$.pipe(
      takeUntilDestroyed(),
      tap(() => {
        this.createStyles();
        this.replayAnimation();
      })
    ).subscribe();
  }

  ngOnChanges(changes: SimpleChanges) {

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.createStyles();
    this.replayAnimation();
  }

  private createStyles() {
    const styleElements = this.el.nativeElement.querySelectorAll('style');

    // Check if there are existing style elements
    if (styleElements.length > 0) {
      // Get the last style element
      const lastStyleElement = styleElements[styleElements.length - 1];

      // Remove the last style element from its parent node
      this.renderer.removeChild(this.el.nativeElement, lastStyleElement);
    }
    const styleElement = this.renderer.createElement('style');
    this.renderer.appendChild(this.el.nativeElement, styleElement);

    let animationClass = `.animated {\n`;
    animationClass+= `\tanimation-name: ${this.animations.animationName};\n`;
    animationClass+= `\tanimation-duration: ${this.animations.animationDuration.value.toString()}${this.animations.animationDuration.unit};\n`;
    animationClass+= `\tanimation-delay: ${this.animations.animationDelay.value.toString()}${this.animations.animationDelay.unit};\n`;
    animationClass+= `\tanimation-timing-function: ${this.animations.animationTimingFunction};\n`;
    animationClass+= `\tanimation-iteration-count: ${this.animations.animationIterationCount};\n`;
    animationClass+= `\tanimation-direction: ${this.animations.animationDirection};\n`;
    animationClass+= `\tanimation-fill-mode: ${this.animations.animationFillMode};\n`;
    animationClass+= `\tanimation-play-state: ${this.animations.animationPlayState};\n`;
    animationClass += `}`;

    let keyframesCssString = `@keyframes ${this.animations.animationName} {\n`;
    for (let keyframe of this.keyframes) {
      keyframesCssString+= `\t${keyframe.frame}% { \n`;
      let translateY;
      let translateX;
      let includeTranslate = false;
      for (const [animationType, animationValue] of Object.entries(keyframe.value)) {
        switch (animationType) {
          case 'translateX':
            translateX = animationValue as {value: number, unit: string};
            includeTranslate = true;
            break;
          case 'translateY':
            translateY = animationValue as {value: number, unit: string};
            includeTranslate = true;
            break;
          case 'resize':
            let resize = animationValue as {width: {value: number, unit: string}, height: {value: number, unit: string}};
            keyframesCssString+= `\t\twidth: ${resize.width.value}${resize.width.unit};\n\t\theight: ${resize.height.value}${resize.height.unit};\n`;
            break;
          case 'rotate':
            let rotate = animationValue as {value: number, unit: string};
            keyframesCssString+= `\t\trotate: ${rotate.value}${rotate.unit};\n`;
            break;
          case 'scale':
          case 'opacity':
          case 'background':
            keyframesCssString+= `\t\t${animationType}: ${animationValue};\n`;
            break;
        }
      }
      const translateXString = translateX ? `${translateX.value}${translateX.unit}`: '0';
      const translateYString = translateY ? `${translateY.value}${translateY.unit}`: '0';

      if (includeTranslate) {
        keyframesCssString+= `\t\ttransform: translate(${translateXString}, ${translateYString});\n`;
      }
      keyframesCssString+= '\t}\n'
    }
    keyframesCssString+= '}'
    this.renderer.setProperty(styleElement, 'textContent', animationClass + keyframesCssString);
  }

  replayAnimation() {
    this.animatedContainer?.nativeElement.classList.remove('animated');
    // because fuck you, that's why
    setTimeout(() => {
      this.animatedContainer?.nativeElement.classList.add('animated');
    }, 10);
  }
}
