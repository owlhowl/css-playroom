import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsContainerElementComponent } from './animations-container-element.component';

describe('AnimationsContainerElementComponent', () => {
  let component: AnimationsContainerElementComponent;
  let fixture: ComponentFixture<AnimationsContainerElementComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AnimationsContainerElementComponent]
    });
    fixture = TestBed.createComponent(AnimationsContainerElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
