import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AnimationsSidebarNavComponent} from "../animations-sidebar-nav/animations-sidebar-nav.component";
import {AnimationsSidebarKeyframesComponent} from "../animations-sidebar-keyframes/animations-sidebar-keyframes.component";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import * as AnimationsSelectors from "../../animations/store/animations.selectors";
import {AnimationsSidebarAnimationComponent} from "../animations-sidebar-animation/animations-sidebar-animation.component";

@Component({
  selector: 'app-animations-sidebar',
  standalone: true,
  imports: [CommonModule, AnimationsSidebarNavComponent, AnimationsSidebarKeyframesComponent, AnimationsSidebarAnimationComponent],
  templateUrl: './animations-sidebar.component.html',
  styleUrls: ['./animations-sidebar.component.scss']
})
export class AnimationsSidebarComponent implements OnInit {
  private store = inject(Store);

  activeTab$!: Observable<string>;

  ngOnInit() {
    this.activeTab$ = this.store.select(AnimationsSelectors.selectActiveTab);
  }
}
