import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsSidebarComponent } from './animations-sidebar.component';

describe('AnimationsSidebarComponent', () => {
  let component: AnimationsSidebarComponent;
  let fixture: ComponentFixture<AnimationsSidebarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AnimationsSidebarComponent]
    });
    fixture = TestBed.createComponent(AnimationsSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
