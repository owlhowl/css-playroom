import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AnimationsSidebarAnimationFormComponent} from "../animations-sidebar-animation-form/animations-sidebar-animation-form.component";
import {Observable} from "rxjs";
import {AnimationState} from "../store/animations.reducer";
import {select, Store} from "@ngrx/store";
import * as AnimationSelectors from "../store/animations.selectors";

@Component({
  selector: 'app-animations-sidebar-animation',
  standalone: true,
  imports: [CommonModule, AnimationsSidebarAnimationFormComponent],
  templateUrl: './animations-sidebar-animation.component.html',
  styleUrls: ['./animations-sidebar-animation.component.scss']
})
export class AnimationsSidebarAnimationComponent implements OnInit {
  animationState$!: Observable<AnimationState>;
  private store = inject(Store);

  ngOnInit() {
    this.animationState$ = this.store.pipe(select(AnimationSelectors.selectAnimation));
  }
}
