import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsSidebarAnimationComponent } from './animations-sidebar-animation.component';

describe('SidebarAnimationsComponent', () => {
  let component: AnimationsSidebarAnimationComponent;
  let fixture: ComponentFixture<AnimationsSidebarAnimationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AnimationsSidebarAnimationComponent]
    });
    fixture = TestBed.createComponent(AnimationsSidebarAnimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
