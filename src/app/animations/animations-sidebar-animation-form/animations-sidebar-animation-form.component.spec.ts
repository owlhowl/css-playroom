import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsSidebarAnimationFormComponent } from './animations-sidebar-animation-form.component';

describe('SidebarAnimationFormComponent', () => {
  let component: AnimationsSidebarAnimationFormComponent;
  let fixture: ComponentFixture<AnimationsSidebarAnimationFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AnimationsSidebarAnimationFormComponent]
    });
    fixture = TestBed.createComponent(AnimationsSidebarAnimationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
