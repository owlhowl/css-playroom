import {Component, inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {CommonModule, KeyValue} from '@angular/common';
import {
  animationProperties,
  AnimationState,
} from "../store/animations.reducer";
import {FormBuilder, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {debounceTime, Subject, takeUntil, tap} from "rxjs";
import {Store} from "@ngrx/store";
import * as AnimationActions from "../../animations/store/animations.actions";
import {FormControlPipe} from "../../shared/pipes/form-control.pipe";
import {
  FlexboxSidebarContainerSelectComponent
} from "../../flexbox/flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";
import {TimeUnitPickerComponent} from "../../shared/ui/time-unit-picker/time-unit-picker.component";
import {
  AnimationsSidebarAnimationFormSelectComponent
} from "../animations-sidebar-animation-form-select/animations-sidebar-animation-form-select.component";
import {kebabize} from "../../shared/utils/kebabize";

type PartialAnimationState = Omit<AnimationState, 'animationDuration' | 'animationDelay' | 'animationIterationCount' | 'animationName' | 'animationTempName'>

@Component({
  selector: 'app-animations-sidebar-animation-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, FormControlPipe, FlexboxSidebarContainerSelectComponent, UnitPickerComponent, TimeUnitPickerComponent, AnimationsSidebarAnimationFormSelectComponent],
  templateUrl: './animations--sidebar-animation-form.component.html',
  styleUrls: ['./animations-sidebar-animation-form.component.scss']
})
export class AnimationsSidebarAnimationFormComponent implements OnChanges, OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() animationState!: AnimationState;
  animationSelects!: PartialAnimationState;
  animationForm!: FormGroup;
  animationProperties = {
    ...animationProperties.animation,
    animationTimingFunction: [...animationProperties.animation.animationTimingFunction],
    animationDirection: [...animationProperties.animation.animationDirection],
    animationFillMode: [...animationProperties.animation.animationFillMode],
    animationPlayState: [...animationProperties.animation.animationPlayState],
  };
  destroy$: Subject<void> = new Subject();
  kebabize = kebabize;

  get animationDuration() {
    return (<FormGroup>this.animationForm.get('animationDuration'));
  }

  get animationDelay() {
    return (<FormGroup>this.animationForm.get('animationDelay'));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['animationState'] && !changes['animationState'].firstChange) {
      this.animationForm.patchValue(changes['animationState']['currentValue'], {
        onlySelf: true, emitEvent: false
      });
    }
  }

  ngOnInit() {
    this.animationForm = this.fb.group({
      animationDuration: this.fb.group({
        value: [this.animationState.animationDuration.value],
        unit: [this.animationState.animationDuration.unit],
      }),
      animationDelay: this.fb.group({
        value: [this.animationState.animationDelay.value],
        unit: [this.animationState.animationDelay.unit],
      }),
      animationTimingFunction: [this.animationState.animationTimingFunction],
      animationDirection: [this.animationState.animationDirection],
      animationFillMode: [this.animationState.animationFillMode],
      animationPlayState: [this.animationState.animationPlayState],
      animationIterationCount: [this.animationState.animationIterationCount],
      animationName: [this.animationState.animationName],
    });

    this.animationSelects = {
      animationTimingFunction: this.animationState.animationTimingFunction,
      animationDirection: this.animationState.animationDirection,
      animationFillMode: this.animationState.animationFillMode,
      animationPlayState: this.animationState.animationPlayState,
    }

    this.animationForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(500),
      tap((formValue) => {
        this.store.dispatch(AnimationActions.setAnimation({value: formValue}));
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  originalOrder = (a: KeyValue<keyof PartialAnimationState, string>, b: KeyValue<keyof PartialAnimationState, string>): number => {
    return 0;
  }
}
