import {Component, inject, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AnimationsSidebarComponent} from "./animations-sidebar/animations-sidebar.component";
import {AnimationsContainerComponent} from "./animations-container/animations-container.component";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import * as AnimationsSelectors from "../animations/store/animations.selectors";
import * as AnimationsActions from "../animations/store/animations.actions";
import {CodeContainerComponent} from "../shared/ui/code-container/code-container.component";
import {CodeService} from "../shared/services/code.service";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-animations',
  standalone: true,
  imports: [CommonModule, AnimationsSidebarComponent, AnimationsContainerComponent, CodeContainerComponent, CodeContainerComponent],
  templateUrl: './animations.component.html',
  styleUrls: ['./animations.component.scss']
})
export class AnimationsComponent implements  OnInit, OnDestroy {
  private store = inject(Store);
  private codeService = inject(CodeService);
  private titleService = inject(Title);
  private metaTagService = inject(Meta);

  activeTab$!: Observable<string>;
  showCodeContainer$!: Observable<boolean>;
  animationsHtml$ = this.codeService.animationsHtml$;
  animationsCss$ = this.codeService.animationsCss$;

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Animations');
    this.metaTagService.addTags([
      {name: 'description', content: 'cssplayroom.com is your first and best source for the css information you’re looking for. Add keyframes, select animation visuals and visualize the animation.'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Animations, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
    this.activeTab$ = this.store.select(AnimationsSelectors.selectActiveTab);
    this.showCodeContainer$ = this.store.select(AnimationsSelectors.selectShowCodeContainer);
  }

  ngOnDestroy() {
    this.store.dispatch(AnimationsActions.resetDefaults());
  }
}
