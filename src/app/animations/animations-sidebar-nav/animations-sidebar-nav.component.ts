import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AnimationsSidebarNavTabComponent} from "../animations-sidebar-nav-tab/animations-sidebar-nav-tab.component";
import {ActiveTab} from "../store/animations.reducer";
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import * as AnimationsSelectors from "../../animations/store/animations.selectors";
import * as AnimationsActions from "../../animations/store/animations.actions";
import {ReplayAnimationService} from "../services/replay-animation.service";

@Component({
  selector: 'app-animations-sidebar-nav',
  standalone: true,
  imports: [CommonModule, AnimationsSidebarNavTabComponent],
  templateUrl: './animations-sidebar-nav.component.html',
  styleUrls: ['./animations-sidebar-nav.component.scss']
})
export class AnimationsSidebarNavComponent implements OnInit {
  private store = inject(Store);
  private replayService = inject(ReplayAnimationService);

  tabs: {
    name: string,
    key: ActiveTab
  }[] = [
    { name: 'Animation', key: 'animation' },
    { name: 'Keyframes', key: 'keyframes' },
  ];
  activeTab$!: Observable<string>;
  showCodeContainer$!: Observable<boolean>;

  ngOnInit() {
    this.activeTab$ = this.store.pipe(select(AnimationsSelectors.selectActiveTab));
    this.showCodeContainer$ = this.store.pipe(select(AnimationsSelectors.selectShowCodeContainer));
  }

  setActiveTab(key: ActiveTab) {
    this.store.dispatch(AnimationsActions.setActiveTab({value: key}));
  }

  resetDefaults() {
    this.store.dispatch(AnimationsActions.resetDefaults());
    this.replayService.replayAnimation$.next();
  }

  toggleCodeContainer() {
    this.store.dispatch(AnimationsActions.toggleCodeContainer());
  }

  replayAnimation() {
    this.replayService.replayAnimation$.next();
  }
}
