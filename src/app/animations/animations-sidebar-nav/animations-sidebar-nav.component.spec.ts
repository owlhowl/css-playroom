import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsSidebarNavComponent } from './animations-sidebar-nav.component';

describe('SidebarNavComponent', () => {
  let component: AnimationsSidebarNavComponent;
  let fixture: ComponentFixture<AnimationsSidebarNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AnimationsSidebarNavComponent]
    });
    fixture = TestBed.createComponent(AnimationsSidebarNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
