import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsSidebarKeyframesFormComponent } from './animations-sidebar-keyframes-form.component';

describe('AnimationsSidebarKeyframesFormComponent', () => {
  let component: AnimationsSidebarKeyframesFormComponent;
  let fixture: ComponentFixture<AnimationsSidebarKeyframesFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AnimationsSidebarKeyframesFormComponent]
    });
    fixture = TestBed.createComponent(AnimationsSidebarKeyframesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
