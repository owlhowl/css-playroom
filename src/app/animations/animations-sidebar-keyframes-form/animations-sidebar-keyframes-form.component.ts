import {Component, inject, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {CommonModule, KeyValue} from '@angular/common';
import {AnimationKeyFrame, AnimationType, animationTypes} from "../store/animations.reducer";
import {FormArray, FormBuilder, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {debounceTime, Subject, takeUntil, tap} from "rxjs";
import * as AnimationActions from "../store/animations.actions";
import {Store} from "@ngrx/store";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";
import {faTrashCan, faXmark} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {DegreeUnitPickerComponent} from "../../shared/ui/degree-unit-picker/degree-unit-picker.component";

@Component({
  selector: 'app-animations-sidebar-keyframes-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, UnitPickerComponent, FontAwesomeModule, DegreeUnitPickerComponent],
  templateUrl: './animations-sidebar-keyframes-form.component.html',
  styleUrls: ['./animations-sidebar-keyframes-form.component.scss']
})
export class AnimationsSidebarKeyframesFormComponent implements OnChanges, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() keyframes!: AnimationKeyFrame[];
  @Input() animationTypes!: Partial<Record<AnimationType, boolean>>;
  @Input() allAnimationTypes!: AnimationType[];

  faTrashCan = faTrashCan;
  animationTypesForm: FormGroup = this.fb.group({});
  keyframesForm: FormGroup = this.fb.group({
    keyframes: this.fb.array([])
  });
  destroy$: Subject<void> = new Subject();

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['animationTypes'] && !changes['animationTypes'].firstChange
      || changes['keyframes'] && !changes['keyframes'].firstChange && changes['keyframes'].previousValue.length != changes['keyframes'].currentValue.length
    ) {
      this.animationTypesForm.patchValue(this.allAnimationTypes, {
        onlySelf: true, emitEvent: false
      });
      this.keyframesForm.patchValue(this.keyframes, {
        onlySelf: true, emitEvent: false
      });

      this.buildKeyframesForm(false);
    }
  }

  ngOnInit() {
    for (const key of this.allAnimationTypes) {
      this.animationTypesForm.addControl(key, this.fb.control(this.animationTypes[key]));
    }
    this.buildKeyframesForm();

    this.animationTypesForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(500),
      tap((formValue) => {
        this.store.dispatch(AnimationActions.setAnimationTypes({value: formValue}));
      })
    ).subscribe();

    this.keyframesForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(1000),
      tap((formValue) => {
        this.store.dispatch(AnimationActions.setKeyframes({value: this.keyframesForm.getRawValue()['keyframes']}));
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  buildKeyframesForm(emitEvent = true) {
    const keyframesArray = this.keyframesForm.get('keyframes') as FormArray;
    keyframesArray.clear({ emitEvent: emitEvent });

    for (let keyframe of this.keyframes) {
      const valuesFormGroup = this.fb.group({});
      for (const [animationType, animationValue] of Object.entries(keyframe.value)) {
        switch (animationType) {
          case 'translateX':
            let translateX = animationValue as {value: number, unit: string};
            valuesFormGroup.addControl('translateX', this.fb.group({
              unit: translateX.unit,
              value: translateX.value,
            }), { emitEvent: emitEvent });
            break;
          case 'translateY':
            let translateY = animationValue as {value: number, unit: string};
            valuesFormGroup.addControl('translateY', this.fb.group({
              unit: translateY.unit,
              value: translateY.value,
            }), { emitEvent: emitEvent });
            break;
          case 'resize':
            let resize = animationValue as {width: {value: number, unit: string}, height: {value: number, unit: string}};
            valuesFormGroup.addControl('resize', this.fb.group({
              width: this.fb.group({
                unit: resize.width.unit,
                value: resize.width.value,
              }),
              height: this.fb.group({
                unit: resize.height.unit,
                value: resize.height.value,
              }),
            }), { emitEvent: emitEvent });
            break;
          case 'rotate':
            let rotate = animationValue as {value: number, unit: string};
            valuesFormGroup.addControl('rotate', this.fb.group({
              unit: rotate.unit,
              value: rotate.value,
            }), { emitEvent: emitEvent });
            break;
          default:
            valuesFormGroup.addControl(animationType, this.fb.control(animationValue), { emitEvent: emitEvent });
            break;
        }
      }

      const keyFrameGroup = this.fb.group({
        id: [keyframe.id],
        frame: [{ value: keyframe.frame, disabled:  keyframe.frame == 0 ||  keyframe.frame == 100 }],
        value: valuesFormGroup
      })
      keyframesArray.push(keyFrameGroup, { emitEvent: emitEvent });
    }
  }

  getFormGroup(i: number, name: string) {
    return this.keyframesForm.get('keyframes.' + i + '.value.' + name) as FormGroup;
  }

  addKeyframe() {
    this.store.dispatch(AnimationActions.addKeyframe());
  }

  removeKeyframe(id: string) {
    this.store.dispatch(AnimationActions.deleteKeyframe({id: id}));
  }

  trackByFn(item: any, index: any) {
    return index.id;
  }
}
