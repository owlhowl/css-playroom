import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-animations-sidebar-nav-tab',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './animations-sidebar-nav-tab.component.html',
  styleUrls: ['./animations-sidebar-nav-tab.component.scss']
})
export class AnimationsSidebarNavTabComponent {
  @Input() name!: string;
  @Input() isActive!: boolean;
}
