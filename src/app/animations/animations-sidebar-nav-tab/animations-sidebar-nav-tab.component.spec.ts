import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsSidebarNavTabComponent } from './animations-sidebar-nav-tab.component';

describe('SidebarNavTabComponent', () => {
  let component: AnimationsSidebarNavTabComponent;
  let fixture: ComponentFixture<AnimationsSidebarNavTabComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AnimationsSidebarNavTabComponent]
    });
    fixture = TestBed.createComponent(AnimationsSidebarNavTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
