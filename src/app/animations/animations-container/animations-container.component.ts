import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AnimationsSidebarAnimationFormComponent} from "../animations-sidebar-animation-form/animations-sidebar-animation-form.component";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {AnimationKeyFrame, AnimationState} from "../store/animations.reducer";
import * as AnimationsSelectors from "../store/animations.selectors";
import {
  AnimationsContainerElementComponent
} from "../animations-container-element/animations-container-element.component";
@Component({
  selector: 'app-animations-container',
  standalone: true,
  imports: [CommonModule, AnimationsSidebarAnimationFormComponent, AnimationsContainerElementComponent],
  templateUrl: './animations-container.component.html',
  styleUrls: ['./animations-container.component.scss']
})
export class AnimationsContainerComponent implements OnInit {
  private store = inject(Store);

  animation$!: Observable<AnimationState>;
  keyframes$!: Observable<AnimationKeyFrame[]>;

  ngOnInit() {
    this.animation$ = this.store.select(AnimationsSelectors.selectAnimation);
    this.keyframes$ = this.store.select(AnimationsSelectors.selectKeyframes);
  }
}
