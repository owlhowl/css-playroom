import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs";
import * as FiltersSelectors from "../../filters/store/filters.selectors";
import * as FiltersActions from "../../filters/store/filters.actions";
import {FiltersSidebarNavTabComponent} from "../filters-sidebar-nav-tab/filters-sidebar-nav-tab.component";

@Component({
  selector: 'app-filters-sidebar-nav',
  standalone: true,
  imports: [CommonModule, FiltersSidebarNavTabComponent],
  templateUrl: './filters-sidebar-nav.component.html',
  styleUrls: ['./filters-sidebar-nav.component.scss']
})
export class FiltersSidebarNavComponent implements OnInit {
  private store = inject(Store);
  showCodeContainer$!: Observable<boolean>;

  ngOnInit() {
    this.showCodeContainer$ = this.store.pipe(select(FiltersSelectors.selectShowCodeContainer));
  }

  resetDefaults() {
    this.store.dispatch(FiltersActions.resetDefaults());
  }

  toggleCodeContainer() {
    this.store.dispatch(FiltersActions.toggleCodeContainer());
  }
}
