import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltersSidebarNavComponent } from './filters-sidebar-nav.component';

describe('FiltersSidebarNavComponent', () => {
  let component: FiltersSidebarNavComponent;
  let fixture: ComponentFixture<FiltersSidebarNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FiltersSidebarNavComponent]
    });
    fixture = TestBed.createComponent(FiltersSidebarNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
