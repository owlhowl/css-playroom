import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-filters-sidebar-nav-tab',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './filters-sidebar-nav-tab.component.html',
  styleUrls: ['./filters-sidebar-nav-tab.component.scss']
})
export class FiltersSidebarNavTabComponent {
  @Input() name!: string;
  @Input() isActive!: boolean;
}
