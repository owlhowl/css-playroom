import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltersSidebarNavTabComponent } from './filters-sidebar-nav-tab.component';

describe('FiltersSidebarNavTabComponent', () => {
  let component: FiltersSidebarNavTabComponent;
  let fixture: ComponentFixture<FiltersSidebarNavTabComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FiltersSidebarNavTabComponent]
    });
    fixture = TestBed.createComponent(FiltersSidebarNavTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
