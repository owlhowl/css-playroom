import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Store} from "@ngrx/store";
import {UnitPicker} from "../../shared/models/unit-picker";
import {Observable} from "rxjs";
import {DropShadowFilter, FiltersValues} from "../store/filters.reducer";
import * as FiltersSelectors from "../../filters/store/filters.selectors";
import {AsPipe} from "../../shared/pipes/as.pipe";

@Component({
  selector: 'app-filters-container',
  standalone: true,
  imports: [CommonModule, AsPipe],
  templateUrl: './filters-container.component.html',
  styleUrls: ['./filters-container.component.scss']
})
export class FiltersContainerComponent implements OnInit {
  private store = inject(Store);

  filters$!: Observable<FiltersValues|null>;
  UnitPicker!: UnitPicker;
  DropShadow!: DropShadowFilter;

  ngOnInit() {
    this.filters$ = this.store.select(FiltersSelectors.selectFilters);
  }
}
