import {Component, inject, Input, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {DropShadowFilter, FiltersValues, filterTypes} from "../store/filters.reducer";
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Store} from "@ngrx/store";
import {debounceTime, Subject, takeUntil, tap} from "rxjs";
import {Transform3DValues} from "../../transforms/store/transforms.reducer";
import * as FilterActions from "../../filters/store/filters.actions";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";
import {DegreeUnitPickerComponent} from "../../shared/ui/degree-unit-picker/degree-unit-picker.component";
import {kebabize} from "../../shared/utils/kebabize";

@Component({
  selector: 'app-filters-sidebar-items-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, UnitPickerComponent, DegreeUnitPickerComponent],
  templateUrl: './filters-sidebar-items-form.component.html',
  styleUrls: ['./filters-sidebar-items-form.component.scss']
})
export class FiltersSidebarItemsFormComponent implements OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);
  @Input() filtersState!: FiltersValues;
  filtersForm = this.fb.group({});
  filterTypes = [...filterTypes];
  destroy$: Subject<void> = new Subject();
  kebabize = kebabize;

  ngOnInit() {
    for (let filter of this.filterTypes) {
      this.filtersForm.addControl(
        `${filter}Checkbox`,
        this.fb.control(this.filtersForm && filter in this.filtersState),
        {emitEvent: false}
      );

      switch (filter) {
        case 'blur':
        case 'hueRotate':
          let valUnit;
          if (this.filterTypes && filter in this.filterTypes) {
            // @ts-ignore
            valUnit = this.filterTypes[filter] as { value: number, unit: string };
          }
          this.filtersForm.addControl(filter, this.fb.group({
            value: this.fb.control<number | null>(valUnit?.value ?? null),
            unit: this.fb.control<string | null>(valUnit?.unit ?? null),
          }), {emitEvent: false});
          break;
        case 'dropShadow':
          let dropShadowValue;
          if (this.filterTypes && filter in this.filterTypes) {
            dropShadowValue = this.filterTypes[filter] as DropShadowFilter;
          }
          this.filtersForm.addControl(filter, this.fb.group({
            length1: this.fb.group({
              value: this.fb.control<number | null>(dropShadowValue?.length1?.value ?? null),
              unit: this.fb.control<string | null>(dropShadowValue?.length1?.unit ?? null),
            }),
            length2: this.fb.group({
              value: this.fb.control<number | null>(dropShadowValue?.length2?.value ?? null),
              unit: this.fb.control<string | null>(dropShadowValue?.length2?.unit ?? null),
            }),
            length3: this.fb.group({
              value: this.fb.control<number | null>(dropShadowValue?.length3?.value ?? null),
              unit: this.fb.control<string | null>(dropShadowValue?.length3?.unit ?? null),
            }),
            color: this.fb.control<string | null>(dropShadowValue?.color ?? null)
          }), {emitEvent: false});
          break;
        case 'brightness':
        case 'contrast':
        case 'grayscale':
        case 'invert':
        case 'opacity':
        case 'saturate':
        case 'sepia':
          let singleInput;
          if (this.filtersForm && filter in this.filterTypes) {
            // @ts-ignore
            singleInput = this.filterTypes[filter] as number;
          }
          this.filtersForm.addControl(
            filter, this.fb.control<number | null>(singleInput ?? null),
            {emitEvent: false}
          );
          break;
      }
    }

    this.filtersForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(500),
      tap(() => {
        const extractedValues: FiltersValues = {};
        const formValue = this.filtersForm.value as Partial<any>;
        for (const key of this.filterTypes) {
          let checkboxKey = `${key}Checkbox`;
          if (formValue.hasOwnProperty(checkboxKey) && formValue[checkboxKey] === true) {
            extractedValues[key] = formValue[key];
          }
        }
        this.store.dispatch(FilterActions.setFilters({value: extractedValues}));
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getFormControl(name: string) {
    return this.filtersForm.get(name) as FormControl;
  }

  getFormGroup(name: string) {
    return this.filtersForm.get(name) as FormGroup;
  }
}
