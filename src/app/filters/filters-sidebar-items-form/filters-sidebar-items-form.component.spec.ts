import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltersSidebarItemsFormComponent } from './filters-sidebar-items-form.component';

describe('FiltersSidebarItemsFormComponent', () => {
  let component: FiltersSidebarItemsFormComponent;
  let fixture: ComponentFixture<FiltersSidebarItemsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FiltersSidebarItemsFormComponent]
    });
    fixture = TestBed.createComponent(FiltersSidebarItemsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
