import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FiltersSidebarNavComponent} from "../filters-sidebar-nav/filters-sidebar-nav.component";
import {FiltersSidebarItemsComponent} from "../filters-sidebar-items/filters-sidebar-items.component";

@Component({
  selector: 'app-filters-sidebar',
  standalone: true,
  imports: [CommonModule, FiltersSidebarNavComponent, FiltersSidebarItemsComponent],
  templateUrl: './filters-sidebar.component.html',
  styleUrls: ['./filters-sidebar.component.scss']
})
export class FiltersSidebarComponent {

}
