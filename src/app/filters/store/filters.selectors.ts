import {createFeatureSelector, createSelector} from "@ngrx/store";
import * as fromFilters from './filters.reducer';

export const selectFiltersState = createFeatureSelector<fromFilters.State>(
  fromFilters.filtersFeatureKey
);

export const selectFilters = createSelector(
  selectFiltersState,
  (state: fromFilters.State) => state.filters
);

export const selectShowCodeContainer = createSelector(
  selectFiltersState,
  (state: fromFilters.State) => state.showCodeContainer
);
