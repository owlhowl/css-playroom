import {createAction, props} from "@ngrx/store";
import {FiltersValues} from "./filters.reducer";


export const resetDefaults = createAction(
  '[Filters] Reset defaults',
);

export const toggleCodeContainer = createAction(
  '[Filters] Toggle Code Container',
);

export const setFilters = createAction(
  '[Filters] Set filters',
  props<{value: FiltersValues | null}>()
);
