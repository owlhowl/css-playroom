import {createReducer, on} from "@ngrx/store";
import {UnitPicker} from "../../shared/models/unit-picker";
import * as FiltersActions from "../../filters/store/filters.actions";
import * as TransformsActions from "../../transforms/store/transforms.actions";


export const filterTypes = [
  'blur', 'brightness', 'contrast', 'dropShadow', 'grayscale', 'hueRotate', 'invert', 'opacity', 'saturate', 'sepia'
] as const;

export type FiltersType = typeof filterTypes[number];
export type DropShadowFilter = {
  length1: UnitPicker,
  length2: UnitPicker,
  length3: UnitPicker,
  color: string,
}

type FiltersValueMappings = {
  blur: UnitPicker;
  brightness: number;
  contrast: number;
  dropShadow: DropShadowFilter;
  grayscale: number;
  hueRotate: UnitPicker;
  invert: number;
  opacity: number;
  saturate: number;
  sepia: number;
};
export type FiltersValues = Partial<Record<FiltersType, FiltersValueMappings[FiltersType]>>;

export const filtersFeatureKey = 'filters';

export interface State {
  filters: FiltersValues,
  showCodeContainer: boolean,
}

const initialState: State = {
  filters: {},
  showCodeContainer: false,
};

export const filtersReducer = createReducer(
  initialState,
  on(FiltersActions.setFilters, (state, action) => {
    return {
      ...state,
      filters: {
        ...(action.value?.blur && { blur: action.value.blur }),
        ...(action.value?.brightness && { brightness: action.value.brightness }),
        ...(action.value?.contrast && { contrast: action.value.contrast }),
        ...(action.value?.dropShadow && { dropShadow: action.value.dropShadow }),
        ...(action.value?.grayscale && { grayscale: action.value.grayscale }),
        ...(action.value?.hueRotate && { hueRotate: action.value.hueRotate }),
        ...(action.value?.invert && { invert: action.value.invert }),
        ...(action.value?.opacity && { opacity: action.value.opacity }),
        ...(action.value?.saturate && { saturate: action.value.saturate }),
        ...(action.value?.sepia && { sepia: action.value.sepia }),
      }
    }
  }),
  on(FiltersActions.toggleCodeContainer, (state) => {
    return {
      ...state,
      showCodeContainer: !state.showCodeContainer,
    }
  }),
  on(FiltersActions.resetDefaults, () => initialState),
);
