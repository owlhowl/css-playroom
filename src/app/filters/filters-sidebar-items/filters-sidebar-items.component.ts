import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {FiltersValues} from "../store/filters.reducer";
import * as FiltersSelectors from "../store/filters.selectors";
import {FiltersSidebarItemsFormComponent} from "../filters-sidebar-items-form/filters-sidebar-items-form.component";

@Component({
  selector: 'app-filters-sidebar-items',
  standalone: true,
  imports: [CommonModule, FiltersSidebarItemsFormComponent],
  templateUrl: './filters-sidebar-items.component.html',
  styleUrls: ['./filters-sidebar-items.component.scss']
})
export class FiltersSidebarItemsComponent implements OnInit {
  private store = inject(Store);

  filtersState$! :Observable<FiltersValues>;

  ngOnInit() {
    this.filtersState$ = this.store.pipe(select(FiltersSelectors.selectFilters));
  }
}
