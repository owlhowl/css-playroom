import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltersSidebarItemsComponent } from './filters-sidebar-items.component';

describe('FiltersSidebarItemsComponent', () => {
  let component: FiltersSidebarItemsComponent;
  let fixture: ComponentFixture<FiltersSidebarItemsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FiltersSidebarItemsComponent]
    });
    fixture = TestBed.createComponent(FiltersSidebarItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
