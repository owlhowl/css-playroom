import {Component, inject, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Meta, Title} from "@angular/platform-browser";
import {Store} from "@ngrx/store";
import {CodeService} from "../shared/services/code.service";
import {Observable} from "rxjs";
import * as FiltersSelectors from "../filters/store/filters.selectors";
import * as FiltersActions from "../filters/store/filters.actions";
import {FiltersSidebarComponent} from "./filters-sidebar/filters-sidebar.component";
import {FiltersContainerComponent} from "./filters-container/filters-container.component";
import {CodeContainerComponent} from "../shared/ui/code-container/code-container.component";

@Component({
  selector: 'app-filters',
  standalone: true,
  imports: [CommonModule, FiltersSidebarComponent, FiltersContainerComponent, CodeContainerComponent],
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit, OnDestroy {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);
  private store = inject(Store);
  private codeService = inject(CodeService);

  showCodeContainer$!: Observable<boolean>;
  filtersHtml$ = this.codeService.filtersHtml$;
  filtersCss$ = this.codeService.filtersCss$;

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Filters');
    this.metaTagService.addTags([
      {name: 'description', content: 'cssplayroom.com is your first and best source for the css information you’re looking for. Set different filters and values for them and see their effects on an image.'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Filters, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
    this.showCodeContainer$ = this.store.select(FiltersSelectors.selectShowCodeContainer);
  }

  ngOnDestroy() {
    this.store.dispatch(FiltersActions.resetDefaults());
  }
}
