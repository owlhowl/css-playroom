import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositioningSidebarNavTabComponent } from './positioning-sidebar-nav-tab.component';

describe('PositioningSidebarNavTabComponent', () => {
  let component: PositioningSidebarNavTabComponent;
  let fixture: ComponentFixture<PositioningSidebarNavTabComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PositioningSidebarNavTabComponent]
    });
    fixture = TestBed.createComponent(PositioningSidebarNavTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
