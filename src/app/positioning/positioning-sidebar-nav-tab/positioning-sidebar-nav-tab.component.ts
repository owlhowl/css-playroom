import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-positioning-sidebar-nav-tab',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './positioning-sidebar-nav-tab.component.html',
  styleUrls: ['./positioning-sidebar-nav-tab.component.scss']
})
export class PositioningSidebarNavTabComponent {
  @Input() name!: string;
  @Input() isActive!: boolean;
}
