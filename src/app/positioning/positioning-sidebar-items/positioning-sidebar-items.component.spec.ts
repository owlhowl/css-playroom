import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositioningSidebarItemsComponent } from './positioning-sidebar-items.component';

describe('PositioningSidebarItemsComponent', () => {
  let component: PositioningSidebarItemsComponent;
  let fixture: ComponentFixture<PositioningSidebarItemsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PositioningSidebarItemsComponent]
    });
    fixture = TestBed.createComponent(PositioningSidebarItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
