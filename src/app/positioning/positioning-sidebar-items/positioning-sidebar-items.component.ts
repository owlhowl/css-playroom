import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {PositionItem} from "../store/positioning.reducer";
import * as PositioningSelectors from "../store/positioning.selectors";
import {
  FlexboxSidebarItemsFormComponent
} from "../../flexbox/flexbox-sidebar-items-form/flexbox-sidebar-items-form.component";
import {
  PositioningSidebarItemsFormComponent
} from "../positioning-sidebar-items-form/positioning-sidebar-items-form.component";

@Component({
  selector: 'app-positioning-sidebar-items',
  standalone: true,
  imports: [CommonModule, FlexboxSidebarItemsFormComponent, PositioningSidebarItemsFormComponent],
  templateUrl: './positioning-sidebar-items.component.html',
  styleUrls: ['./positioning-sidebar-items.component.scss']
})
export class PositioningSidebarItemsComponent implements OnInit {
  private store = inject(Store);

  selectedItem$!: Observable<PositionItem | null>;

  ngOnInit() {
    this.selectedItem$ = this.store.pipe(select(PositioningSelectors.selectSelectedItem));
  }
 }
