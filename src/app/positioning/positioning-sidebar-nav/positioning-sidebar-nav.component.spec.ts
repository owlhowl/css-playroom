import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositioningSidebarNavComponent } from './positioning-sidebar-nav.component';

describe('PositioningSidebarNavComponent', () => {
  let component: PositioningSidebarNavComponent;
  let fixture: ComponentFixture<PositioningSidebarNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PositioningSidebarNavComponent]
    });
    fixture = TestBed.createComponent(PositioningSidebarNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
