import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs";
import * as PositioningSelectors from './../store/positioning.selectors';
import * as PositioningActions from './../store/positioning.actions';
import {PositioningSidebarNavTabComponent} from "../positioning-sidebar-nav-tab/positioning-sidebar-nav-tab.component";

@Component({
  selector: 'app-positioning-sidebar-nav',
  standalone: true,
  imports: [CommonModule, PositioningSidebarNavTabComponent],
  templateUrl: './positioning-sidebar-nav.component.html',
  styleUrls: ['./positioning-sidebar-nav.component.scss']
})
export class PositioningSidebarNavComponent implements OnInit {
  private store = inject(Store);
  showCodeContainer$!: Observable<boolean>;

  ngOnInit() {
    this.showCodeContainer$ = this.store.pipe(select(PositioningSelectors.selectShowCodeContainer));
  }

  resetDefaults() {
    this.store.dispatch(PositioningActions.resetDefaults());
  }

  addItem() {
    this.store.dispatch(PositioningActions.addPositioningItem());
  }

  toggleCodeContainer() {
    this.store.dispatch(PositioningActions.toggleCodeContainer());
  }
}
