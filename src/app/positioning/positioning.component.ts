import {Component, inject, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Store} from "@ngrx/store";
import {CodeService} from "../shared/services/code.service";
import {Observable} from "rxjs";
import * as PositioningSelectors from './store/positioning.selectors';
import * as PositioningActions from './store/positioning.actions';
import {PositioningSidebarComponent} from "./positioning-sidebar/positioning-sidebar.component";
import {PositioningContainerComponent} from "./positioning-container/positioning-container.component";
import {CodeContainerComponent} from "../shared/ui/code-container/code-container.component";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-positioning',
  standalone: true,
  imports: [CommonModule, PositioningSidebarComponent, PositioningContainerComponent, CodeContainerComponent],
  templateUrl: './positioning.component.html',
  styleUrls: ['./positioning.component.scss']
})
export class PositioningComponent implements  OnInit, OnDestroy {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);
  private store = inject(Store);
  private codeService = inject(CodeService);

  showCodeContainer$!: Observable<boolean>;
  positioningHtml$ = this.codeService.positioningHtml$;
  positioningCss$ = this.codeService.positioningCss$;

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Positioning');
    this.metaTagService.addTags([
      {name: 'description', content: 'cssplayroom.com is your first and best source for the css information you’re looking for. Add div items, set their position and see the changes in container'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Positioning, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
    this.showCodeContainer$ = this.store.select(PositioningSelectors.selectShowCodeContainer);
  }

  ngOnDestroy() {
    this.store.dispatch(PositioningActions.resetDefaults());
  }
}
