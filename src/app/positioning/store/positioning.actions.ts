import {createAction, props} from "@ngrx/store";
import {PositionItem} from "./positioning.reducer";

export const addPositioningItem = createAction(
  '[Positioning] Add positioning item',
);

export const removePositioningItem = createAction(
  '[Positioning] Remove positioning item',
  props<{id: string}>(),
)

export const setSelectedItem = createAction(
  '[Positioning] Set selected item',
  props<{id: string}>(),
);

export const updateSelectedItem = createAction(
  '[Positioning] Update selected item',
  props<{value: PositionItem}>(),
);

export const resetDefaults = createAction(
  '[Positioning] Reset defaults',
);

export const toggleCodeContainer = createAction(
  '[Positioning] Toggle Code Container',
);
