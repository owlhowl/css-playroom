import {UnitPicker} from "../../shared/models/unit-picker";
import {createReducer, on} from "@ngrx/store";
import * as PositioningActions from "../../positioning/store/positioning.actions";
import { v4 as uuidv4 } from 'uuid';


export const positions = ['static', 'relative', 'absolute', 'fixed', 'sticky'] as const;
export type Position = typeof positions[number];
export interface PositionItem {
  id: string,
  position: Position,
  top: UnitPicker,
  right: UnitPicker,
  bottom: UnitPicker,
  left: UnitPicker,
}

export interface State {
  items: PositionItem[],
  selectedItem: PositionItem | null,
  showCodeContainer: boolean,
}

export const positioningFeatureKey = 'positioning';

const initialState: State = {
  items: [],
  selectedItem: null,
  showCodeContainer: false,
}

export const positioningReducer = createReducer(
  initialState,
  on(PositioningActions.addPositioningItem, (state) => {
    return {
      ...state,
      items: [
        ...state.items, {
          id: uuidv4(),
          position: 'static',
          top: {
            value: 0,
            unit: 'px',
          },
          right: {
            value: 0,
            unit: 'px',
          },
          bottom: {
            value: 0,
            unit: 'px',
          },
          left: {
            value: 0,
            unit: 'px',
          },
        } as PositionItem
      ]
    }
  }),
  on(PositioningActions.setSelectedItem, (state, action) => {
    const selectedItem = state.selectedItem;
    const foundItem = state.items.find(item => item.id == action.id) as PositionItem;

    return {
      ...state,
      selectedItem: selectedItem?.id === action.id ? null : {
        ...foundItem,
        top: {
          ...foundItem.top,
        },
        right: {
          ...foundItem.right,
        },
        bottom: {
          ...foundItem.bottom,
        },
        left: {
          ...foundItem.left,
        }
      }
    }
  }),
  on(PositioningActions.updateSelectedItem, (state, action) => {
    const updatedItem = {
      position: action.value.position,
      top: {
        value: action.value.top.value,
        unit: action.value.top.unit,
      },
      right: {
        value: action.value.right.value,
        unit: action.value.right.unit,
      },
      bottom: {
        value: action.value.bottom.value,
        unit: action.value.bottom.unit,
      },
      left: {
        value: action.value.left.value,
        unit: action.value.left.unit,
      },
    };

    return {
      ...state,
      items: [
        ...state.items.map(item => {
          return item.id === state.selectedItem?.id ? {
            ...item,
            position: updatedItem.position,
            top: {
              ...state.selectedItem?.top,
              ...updatedItem.top
            },
            right: {
              ...state.selectedItem?.right,
              ...updatedItem.right
            },
            bottom: {
              ...state.selectedItem?.bottom,
              ...updatedItem.bottom
            },
            left: {
              ...state.selectedItem?.left,
              ...updatedItem.left
            }
          } : item
        })
      ],
      selectedItem: {
        ...state.selectedItem,
        position: updatedItem.position,
        top: {
          ...state.selectedItem?.top,
          ...updatedItem.top
        },
        right: {
          ...state.selectedItem?.right,
          ...updatedItem.right
        },
        bottom: {
          ...state.selectedItem?.bottom,
          ...updatedItem.bottom
        },
        left: {
          ...state.selectedItem?.left,
          ...updatedItem.left
        }
      } as PositionItem
    }
  }),
  on(PositioningActions.removePositioningItem, (state, action) => {
    return {
      ...state,
      selectedItem: state.selectedItem?.id == action.id ? null : state.selectedItem,
      items: state.items.filter(item => item.id != action.id),
    }
  }),
  on(PositioningActions.toggleCodeContainer, (state) => {
    return {
      ...state,
      showCodeContainer: !state.showCodeContainer,
    }
  }),
  on(PositioningActions.resetDefaults, () => initialState),
)
