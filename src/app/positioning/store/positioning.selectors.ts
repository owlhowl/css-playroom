import {createFeatureSelector, createSelector} from "@ngrx/store";
import * as fromPositioning from "../../positioning/store/positioning.reducer";


export const selectPositioningState = createFeatureSelector<fromPositioning.State>(
  fromPositioning.positioningFeatureKey
);

export const selectPositioningItems = createSelector(
  selectPositioningState,
  (state: fromPositioning.State) => state.items,
);

export const selectSelectedItem = createSelector(
  selectPositioningState,
  (state: fromPositioning.State) => state.selectedItem,
);

export const selectShowCodeContainer = createSelector(
  selectPositioningState,
  (state: fromPositioning.State) => state.showCodeContainer,
);
