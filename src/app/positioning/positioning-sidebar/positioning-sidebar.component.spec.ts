import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositioningSidebarComponent } from './positioning-sidebar.component';

describe('PositioningSidebarComponent', () => {
  let component: PositioningSidebarComponent;
  let fixture: ComponentFixture<PositioningSidebarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PositioningSidebarComponent]
    });
    fixture = TestBed.createComponent(PositioningSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
