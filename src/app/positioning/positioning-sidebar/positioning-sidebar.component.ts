import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {PositioningSidebarNavComponent} from "../positioning-sidebar-nav/positioning-sidebar-nav.component";
import {PositioningSidebarItemsComponent} from "../positioning-sidebar-items/positioning-sidebar-items.component";

@Component({
  selector: 'app-positioning-sidebar',
  standalone: true,
  imports: [CommonModule, PositioningSidebarNavComponent, PositioningSidebarItemsComponent],
  templateUrl: './positioning-sidebar.component.html',
  styleUrls: ['./positioning-sidebar.component.scss']
})
export class PositioningSidebarComponent {
}
