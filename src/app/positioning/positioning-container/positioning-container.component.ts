import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {PositionItem} from "../store/positioning.reducer";
import {faXmark} from "@fortawesome/free-solid-svg-icons";
import * as PositioningSelectors from "../store/positioning.selectors";
import * as PositioningActions from "../store/positioning.actions";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
@Component({
  selector: 'app-positioning-container',
  standalone: true,
  imports: [CommonModule, FontAwesomeModule],
  templateUrl: './positioning-container.component.html',
  styleUrls: ['./positioning-container.component.scss']
})
export class PositioningContainerComponent implements OnInit {
  private store = inject(Store);

  positioningItems$!: Observable<PositionItem[]>;
  selectedItem$!: Observable<PositionItem | null>;
  faDelete = faXmark;

  ngOnInit() {
    this.positioningItems$ = this.store.select(PositioningSelectors.selectPositioningItems);
    this.selectedItem$ = this.store.select(PositioningSelectors.selectSelectedItem);
  }

  setSelectedItem(index: string) {
    this.store.dispatch(PositioningActions.setSelectedItem({id: index}));
  }

  removePositioningItem(id: string) {
    this.store.dispatch(PositioningActions.removePositioningItem({id: id}));
  }
}
