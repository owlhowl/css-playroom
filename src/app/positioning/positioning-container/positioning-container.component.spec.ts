import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositioningContainerComponent } from './positioning-container.component';

describe('PositioningContainerComponent', () => {
  let component: PositioningContainerComponent;
  let fixture: ComponentFixture<PositioningContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PositioningContainerComponent]
    });
    fixture = TestBed.createComponent(PositioningContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
