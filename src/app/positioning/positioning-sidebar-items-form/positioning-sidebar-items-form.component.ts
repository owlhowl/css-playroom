import {Component, inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';
import {PositionItem, positions} from "../store/positioning.reducer";
import {FormBuilder, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Subject, takeUntil, tap} from "rxjs";
import {Store} from "@ngrx/store";
import * as PositioningActions from "../../positioning/store/positioning.actions";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";

@Component({
  selector: 'app-positioning-sidebar-items-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, UnitPickerComponent],
  templateUrl: './positioning-sidebar-items-form.component.html',
  styleUrls: ['./positioning-sidebar-items-form.component.scss']
})
export class PositioningSidebarItemsFormComponent implements OnChanges, OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() selectedItem!: PositionItem;
  selectedItemForm!: FormGroup;
  destroy$: Subject<void> = new Subject();
  positions = [...positions];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedItem'] && !changes['selectedItem'].firstChange) {

      if (changes['selectedItem'].currentValue) {
        this.selectedItemForm.patchValue(changes['selectedItem']?.currentValue, {
          onlySelf: true, emitEvent: false
        });
      }
    }
  }

  ngOnInit() {
    this.selectedItemForm = this.fb.group({
      position: [this.selectedItem?.position],
      top: this.fb.group({
        value: [this.selectedItem?.top.value],
        unit: [this.selectedItem?.top.unit],
      }),
      right: this.fb.group({
        value: [this.selectedItem?.right.value],
        unit: [this.selectedItem?.right.unit],
      }),
      bottom: this.fb.group({
        value: [this.selectedItem?.bottom.value],
        unit: [this.selectedItem?.bottom.unit],
      }),
      left: this.fb.group({
        value: [this.selectedItem?.left.value],
        unit: [this.selectedItem?.left.unit],
      }),
    });

    this.selectedItemForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      tap((formValue) => {
        this.store.dispatch(PositioningActions.updateSelectedItem({value: formValue}));
      }),
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getFormGroup(name: string) {
    return this.selectedItemForm.get(name) as FormGroup;
  }

}
