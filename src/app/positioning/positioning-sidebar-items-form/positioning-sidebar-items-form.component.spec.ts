import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositioningSidebarItemsFormComponent } from './positioning-sidebar-items-form.component';

describe('PositioningSidebarItemsFormComponent', () => {
  let component: PositioningSidebarItemsFormComponent;
  let fixture: ComponentFixture<PositioningSidebarItemsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PositioningSidebarItemsFormComponent]
    });
    fixture = TestBed.createComponent(PositioningSidebarItemsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
