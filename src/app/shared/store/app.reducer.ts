import { ActionReducerMap } from '@ngrx/store';
import * as animations from '../../animations/store/animations.reducer';
import * as flexbox from '../../flexbox/store/flexbox.reducer';
import * as grid from '../../grid/store/grid.reducer';
import * as boxModel from '../../box-model/store/box-model.reducer';
import * as positioning from '../../positioning/store/positioning.reducer';
import * as transforms from '../../transforms/store/transforms.reducer';
import * as pseudoClasses from '../../pseudo-classes/store/pseudo-classes.reducer';
import * as filters from '../../filters/store/filters.reducer';

export interface AppState {
  animations: animations.State,
  boxModel: boxModel.State;
  flexbox: flexbox.State;
  grid: grid.State;
  positioning: positioning.State;
  transforms: transforms.State;
  pseudoClasses: pseudoClasses.State,
  filters: filters.State,
}

export const appReducer: ActionReducerMap<AppState> = {
  animations: animations.animationsReducer,
  boxModel: boxModel.boxModelReducer,
  flexbox: flexbox.flexboxReducer,
  grid: grid.gridReducer,
  positioning: positioning.positioningReducer,
  transforms: transforms.transformsReducer,
  pseudoClasses: pseudoClasses.pseudoClassesReducer,
  filters: filters.filtersReducer,
};
