export interface UnitPicker {
  unit: string,
  value: number,
}
