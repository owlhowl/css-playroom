import {IconDefinition} from "@fortawesome/free-brands-svg-icons";

export interface Page {
  name: string,
  path: string,
  icon: IconDefinition,
  description: string,
  active?: boolean,
}
