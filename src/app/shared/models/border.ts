import {UnitPicker} from './unit-picker';

export const borderStyles = ['none', 'hidden', 'dotted', 'dashed', 'solid'
  ,'double', 'groove', 'ridge', 'inset', 'outset'] as const;

export type BorderStyle = typeof borderStyles[number];
export type Border = {
  size: UnitPicker,
  style: BorderStyle,
  color: string,
}
