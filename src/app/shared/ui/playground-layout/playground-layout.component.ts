import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterOutlet} from "@angular/router";
import {MenuSidebarComponent} from "../menu-sidebar/menu-sidebar.component";

@Component({
  selector: 'app-playground-layout',
  standalone: true,
  imports: [CommonModule, RouterOutlet, MenuSidebarComponent],
  templateUrl: './playground-layout.component.html',
  styleUrls: ['./playground-layout.component.scss']
})
export class PlaygroundLayoutComponent {

}
