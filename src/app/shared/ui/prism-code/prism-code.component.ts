import {AfterViewInit, Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import * as Prism from "prismjs";
import 'prismjs/components/prism-css';

@Component({
  selector: 'app-prism-code',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './prism-code.component.html',
  styleUrls: ['./prism-code.component.scss']
})
export class PrismCodeComponent implements OnChanges, AfterViewInit {
  @Input() code!: string;
  @Input() lang!: string;
  @ViewChild('codeContainer') codeContainer!: ElementRef;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['code']) {
      if (this.codeContainer?.nativeElement) {
        this.codeContainer.nativeElement.textContent = this.code;
        Prism.highlightElement(this.codeContainer.nativeElement);
      }
    }
  }

  ngAfterViewInit() {
    Prism.highlightElement(this.codeContainer.nativeElement);
  }
}
