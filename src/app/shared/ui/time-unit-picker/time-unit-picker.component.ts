import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Subject} from "rxjs";

@Component({
  selector: 'app-time-unit-picker',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './time-unit-picker.component.html',
  styleUrls: ['./time-unit-picker.component.scss']
})
export class TimeUnitPickerComponent {
  @Input() timeUnitFormGroup!: FormGroup;
  @Input() label!: string;

  defaultUnits = ['ms', 's'];
  destroy$: Subject<void> = new Subject();

  ngOnInit() {}

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
