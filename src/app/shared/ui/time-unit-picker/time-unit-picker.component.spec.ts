import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeUnitPickerComponent } from './time-unit-picker.component';

describe('TimeUnitPickerComponent', () => {
  let component: TimeUnitPickerComponent;
  let fixture: ComponentFixture<TimeUnitPickerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TimeUnitPickerComponent]
    });
    fixture = TestBed.createComponent(TimeUnitPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
