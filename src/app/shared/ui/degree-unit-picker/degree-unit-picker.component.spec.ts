import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DegreeUnitPickerComponent } from './degree-unit-picker.component';

describe('DegreeUnitPickerComponent', () => {
  let component: DegreeUnitPickerComponent;
  let fixture: ComponentFixture<DegreeUnitPickerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DegreeUnitPickerComponent]
    });
    fixture = TestBed.createComponent(DegreeUnitPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
