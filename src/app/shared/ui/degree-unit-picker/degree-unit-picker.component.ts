import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Subject} from "rxjs";

@Component({
  selector: 'app-degree-unit-picker',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './degree-unit-picker.component.html',
  styleUrls: ['./degree-unit-picker.component.scss']
})
export class DegreeUnitPickerComponent {
  @Input() unitFormGroup!: FormGroup;
  @Input() label!: string;
  @Input() extraOptions?: string[];

  defaultUnits = ['deg', 'turn', 'rad'];
  destroy$: Subject<void> = new Subject();

  ngOnInit() {}

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
