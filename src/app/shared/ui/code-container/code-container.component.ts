import {Component, inject, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {CodeService} from "../../services/code.service";
import {PrismCodeComponent} from "../prism-code/prism-code.component";

@Component({
  selector: 'app-code-container',
  standalone: true,
  imports: [CommonModule, PrismCodeComponent],
  templateUrl: './code-container.component.html',
  styleUrls: ['./code-container.component.scss']
})
export class CodeContainerComponent {
  @Input() codeHtml!: string;
  @Input() codeCss!: string;
}
