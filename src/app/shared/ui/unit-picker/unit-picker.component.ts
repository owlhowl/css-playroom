import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {Subject} from "rxjs";

@Component({
  selector: 'app-unit-picker',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './unit-picker.component.html',
  styleUrls: ['./unit-picker.component.scss']
})
export class UnitPickerComponent implements OnInit, OnDestroy {
  @Input() unitFormGroup!: FormGroup;
  @Input() label!: string;
  @Input() extraOptions?: string[];

  defaultUnits = ['px', '%', 'em', 'rem', 'fr', 'vh', 'vw'];
  destroy$: Subject<void> = new Subject();

  ngOnInit() {}

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
