import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Page} from "../../models/page.interface";
import {MenuSidebarItemComponent} from "./menu-sidebar-item/menu-sidebar-item.component";
import {RouterLink} from "@angular/router";
import {PagesService} from "../../services/pages.service";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {faGitlab, faYoutube} from "@fortawesome/free-brands-svg-icons";

@Component({
  selector: 'app-menu-sidebar',
  standalone: true,
  imports: [CommonModule, MenuSidebarItemComponent, RouterLink, FontAwesomeModule],
  templateUrl: './menu-sidebar.component.html',
  styleUrls: ['./menu-sidebar.component.scss']
})
export class MenuSidebarComponent implements OnInit {
  pagesService = inject(PagesService)
  pages: Page[] = [];
  comingSoonPages: Page[] = [];


  ngOnInit() {
    this.pages = this.pagesService.getPages();
    this.comingSoonPages = this.pagesService.getComingSoonPages();
  }

  protected readonly faGitlab = faGitlab;
  protected readonly faYoutube = faYoutube;
}
