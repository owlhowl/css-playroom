import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Page} from "../../../models/page.interface";
import {RouterLink, RouterLinkActive} from "@angular/router";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

@Component({
  selector: 'app-menu-sidebar-item',
  standalone: true,
  imports: [CommonModule, RouterLink, FontAwesomeModule, RouterLinkActive],
  templateUrl: './menu-sidebar-item.component.html',
  styleUrls: ['./menu-sidebar-item.component.scss']
})
export class MenuSidebarItemComponent {
  @Input() page!: Page;
}
