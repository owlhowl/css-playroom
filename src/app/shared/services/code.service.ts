import {inject, Injectable} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {combineLatest, map, Observable, of} from "rxjs";
import {FlexContainer, FlexItem} from '../../flexbox/store/flexbox.reducer';
import * as FlexboxSelectors from "../../flexbox/store/flexbox.selectors";
import * as AnimationsSelectors from "../../animations/store/animations.selectors";
import * as BoxModelSelectors from "../../box-model/store/box-model.selectors";
import * as PositioningSelectors from "../../positioning/store/positioning.selectors";
import * as TransformSelectors from "../../transforms/store/transforms.selectors";
import * as PseudoClassesSelectors from "../../pseudo-classes/store/pseudo-classes.selectors";
import * as GridSelectors from "../../grid/store/grid.selectors";
import * as FiltersSelectors from "../../filters/store/filters.selectors";
import {AnimationKeyFrame, AnimationState} from "../../animations/store/animations.reducer";
import {BoxModelState} from "../../box-model/store/box-model.reducer";
import {PositionItem} from "../../positioning/store/positioning.reducer";
import {
  Matrix, Matrix3d, Rotate3d, Scale3d,
  Transform2DValues, Transform3DValues,
  TransformedItem, TransformOptions, Translate3d
} from "../../transforms/store/transforms.reducer";
import {UnitPicker} from "../models/unit-picker";
import {CssOptions, PseudoClass, PseudoClassType} from "../../pseudo-classes/store/pseudo-classes.reducer";
import {GridContainer, GridItem} from "../../grid/store/grid.reducer";
import {DropShadowFilter, FiltersValues} from "../../filters/store/filters.reducer";

@Injectable({
  providedIn: 'root'
})
export class CodeService {
  private store = inject(Store);

  flexboxContainerState$: Observable<FlexContainer>;
  flexboxItemsState$: Observable<FlexItem[]>;
  animationState$: Observable<AnimationState>;
  keyframesState$: Observable<AnimationKeyFrame[]>;
  boxModelState$: Observable<BoxModelState>;
  positioningItemsState$: Observable<PositionItem[]>;
  transformedItemState$: Observable<TransformedItem>;
  transformOptionsState$: Observable<TransformOptions>;
  transformFunctions2dState$: Observable<Transform2DValues | null>;
  transformFunctions3dState$: Observable<Transform3DValues | null>;
  pseudoClassTypeState$: Observable<PseudoClassType>;
  pseudoClassState$: Observable<PseudoClass>;
  pseudoCssOptionsState$: Observable<CssOptions>;
  gridContainerState$: Observable<GridContainer>;
  gridItemsState$: Observable<GridItem[]>;
  filtersState$: Observable<FiltersValues | null>;

  flexboxHtml$: Observable<string>;
  flexboxCss$: Observable<string>;
  animationsHtml$: Observable<string>;
  animationsCss$: Observable<string>;
  boxModelHtml$: Observable<string>;
  boxModelCss$: Observable<string>;
  positioningHtml$: Observable<string>;
  positioningCss$: Observable<string>;
  transformsHtml$: Observable<string>;
  transformsCss$: Observable<string>;
  pseudoClassesHtml$: Observable<string>;
  pseudoClassesCss$: Observable<string>;
  gridHtml$: Observable<string>;
  gridCss$: Observable<string>;
  filtersHtml$: Observable<string>;
  filtersCss$: Observable<string>;

  constructor() {
    this.flexboxContainerState$ = this.store.pipe(select(FlexboxSelectors.selectFlexContainer));
    this.flexboxItemsState$ = this.store.pipe(select(FlexboxSelectors.selectFlexItems));
    this.animationState$ = this.store.pipe(select(AnimationsSelectors.selectAnimation));
    this.keyframesState$ = this.store.pipe(select(AnimationsSelectors.selectKeyframes));
    this.boxModelState$ = this.store.pipe(select(BoxModelSelectors.selectBoxModel));
    this.positioningItemsState$ = this.store.pipe(select(PositioningSelectors.selectPositioningItems));
    this.transformedItemState$ = this.store.pipe(select(TransformSelectors.selectTransformedItem));
    this.transformOptionsState$ = this.store.pipe(select(TransformSelectors.selectTransformOptions));
    this.transformFunctions2dState$ = this.store.pipe(select(TransformSelectors.selectTransformFunctions2D));
    this.transformFunctions3dState$ = this.store.pipe(select(TransformSelectors.selectTransformFunctions3D));
    this.pseudoClassTypeState$ = this.store.pipe(select(PseudoClassesSelectors.selectPseudoClassType));
    this.pseudoClassState$ = this.store.pipe(select(PseudoClassesSelectors.selectPseudoClass));
    this.pseudoCssOptionsState$ = this.store.pipe(select(PseudoClassesSelectors.selectCssOptions));
    this.gridContainerState$ = this.store.pipe(select(GridSelectors.selectGridContainer));
    this.gridItemsState$ = this.store.pipe(select(GridSelectors.selectGridItems));
    this.filtersState$ = this.store.pipe(select(FiltersSelectors.selectFilters));

    this.flexboxHtml$ = this.flexboxItemsState$.pipe(
      map((flexboxItems) => {
        const itemsCode = flexboxItems.length === 0 ? '' : flexboxItems.map((item, index) => {
          return `\t<div class="item-${index}">${index}</div>`
        }).join('\n');

        return `<div class="container">\n${itemsCode}\n</div>`;
      })
    );

    this.flexboxCss$ = combineLatest([this.flexboxContainerState$, this.flexboxItemsState$]).pipe(
      map(([flexboxContainer, flexboxItems]) => {
        let gap;
        if (flexboxContainer.rowGap.value == flexboxContainer.columnGap.value &&
            flexboxContainer.rowGap.unit == flexboxContainer.columnGap.unit
        ) {
          gap = `\n\t/* row-gap and column-gap can be combined in: \n\t 'gap: ${flexboxContainer.rowGap.value}${flexboxContainer.rowGap.unit}' */`;
        } else {
          gap = `\n\t/* row-gap and column-gap can be combined in: \n\t 'gap: ${flexboxContainer.rowGap.value}${flexboxContainer.rowGap.unit} ${flexboxContainer.columnGap.value}${flexboxContainer.columnGap.unit}' */`;
        }
        let flexDirection = `\n\tflex-direction: ${flexboxContainer.flexDirection}; ${flexboxContainer.flexDirection == 'row' ? '/* default */' : ''}`;
        let flexWrap = `\n\tflex-wrap: ${flexboxContainer.flexWrap}; ${flexboxContainer.flexWrap == 'nowrap' ? '/* default */' : ''}`;
        let flexFlow = `\n\t/* flex-direction and flex-wrap can be combined in \n\t 'flex-flow: ${flexboxContainer.flexDirection} ${flexboxContainer.flexWrap}' */`;
        let justifyContent = `\n\tjustify-content: ${flexboxContainer.justifyContent}; ${flexboxContainer.justifyContent == 'flex-start' ? '/* default */' : ''}`;
        let alignItems = `\n\talign-items: ${flexboxContainer.alignItems}; ${flexboxContainer.alignItems == 'flex-start' ? '/* default */' : ''}`;
        let alignContent = `\n\talign-content: ${flexboxContainer.alignContent}; ${flexboxContainer.alignContent == 'flex-start' ? '/* default */' : ''}`;
        let rowGap = `\n\trow-gap: ${flexboxContainer.rowGap.value}${flexboxContainer.rowGap.unit};`;
        let columnGap = `\n\tcolumn-gap: ${flexboxContainer.columnGap.value}${flexboxContainer.columnGap.unit};`;

        const containerCss = `.container {\n\tdisplay: flex; ${flexDirection}${flexWrap}${flexFlow}${justifyContent}\
        ${justifyContent}${alignItems}${alignContent}${rowGap}${columnGap}${gap}\n}`;

        const itemsCss = flexboxItems
          .map((it, index) => {
            let order = `\n\torder: ${it.styles.order}; ${it.styles.order == 0 ? '/* default */' : ''}`;
            let flexGrow = `\n\tflex-grow: ${it.styles.flexGrow}; ${it.styles.order == 0 ? '/* default */' : ''}`;
            let flexShrink = `\n\tflex-shrink: ${it.styles.flexShrink}; ${it.styles.flexShrink == 1 ? '/* default */' : ''}`;
            let flexBasis = `\n\tflex-basis: ${it.styles.flexBasis}; ${it.styles.flexBasis == 'auto' ? '/* default */' : ''}`;
            let alignSelf = `\n\talign-self: ${it.styles.alignSelf}; ${it.styles.alignSelf == 'auto' ? '/* default */' : ''}`;
            let flex = `\n\t/* flex-grow, flex-shrink and flex-basis can be combined in \n\t 'flex: ${it.styles.flexGrow} ${it.styles.flexShrink} ${it.styles.flexBasis}' */`;

            return `.item-${index} {${order}${flexGrow}${flexShrink}${flexBasis}${flex}${alignSelf}\n}`
          })
          .filter((it) => it !== null)
          .join('\n\n');

        return `${containerCss}${itemsCss?'\n\n'+itemsCss:''}`;
      })
    );

    this.gridHtml$ = this.gridItemsState$.pipe(
        map((gridItems) => {
            const itemsCode = gridItems.length === 0 ? '' : gridItems.map((item, index) => {
                return `\t<div class="item-${index}">${item.properties.gridArea}</div>`
            }).join('\n');

            return `<div class="container">\n${itemsCode}\n</div>`;
        })
    );

    this.gridCss$ = combineLatest([this.gridContainerState$, this.gridItemsState$]).pipe(
        map(([container, items]) => {
            let gridTemplateColumnsValue = container.gridTemplateColumns.map((item) => {
                return item.value + item.unit;
            }).join(' ');
            let gridTemplateRowsValue = container.gridTemplateRows.map((item) => {
                return item.value + item.unit;
            }).join(' ');
            let gridAutoColumnsValue = container.gridAutoColumns.map((item) => {
                return item.value + item.unit;
            }).join(' ');
            let gridAutoRowsValue = container.gridAutoRows.map((item) => {
                return item.value + item.unit;
            }).join(' ');

            let display = `\n\tdisplay: ${container.display};`;
            let gridTemplateColumns = `\n\tgrid-template-columns: ${gridTemplateColumnsValue};`;
            let gridTemplateRows = `\n\tgrid-template-rows: ${gridTemplateRowsValue};`;
            let columnGap = `\n\tcolumn-gap: ${container.columnGap.value}${container.columnGap.unit};`;
            let rowGap = `\n\trow-gap: ${container.rowGap.value}${container.rowGap.unit};`;
            let justifyItems = `\n\tjustify-items: ${container.justifyItems};`;
            let alignItems = `\n\talign-items: ${container.alignItems};`;
            let justifyContent = `\n\tjustify-content: ${container.justifyContent};`;
            let alignContent = `\n\talign-content: ${container.alignContent};`;
            let gridAutoColumns = `\n\tgrid-auto-columns: ${gridAutoColumnsValue};`;
            let gridAutoRows = `\n\tgrid-auto-rows: ${gridAutoRowsValue};`;
            let gridAutoFlow = `\n\tgrid-auto-flow: ${container.gridAutoFlow};`;

            const containerCss = `.container {${display}${gridTemplateColumns}${gridTemplateRows}\
                ${columnGap}${rowGap}${justifyItems}${alignItems}${justifyContent}${alignContent}${gridAutoColumns}\
                ${gridAutoRows}${gridAutoFlow}\n}`;

            const itemsCss = items
                .map((it, index) => {
                    let gridColumnStart = `\n\tgrid-column-start: ${it.properties.gridColumnStart};`;
                    let gridRowStart = `\n\tgrid-row-start: ${it.properties.gridRowStart};`;
                    let gridColumnEnd = `\n\tgrid-column-end: ${it.properties.gridColumnEnd};`;
                    let gridRowEnd = `\n\tgrid-row-end: ${it.properties.gridRowEnd};`;
                    let justifySelf = `\n\tjustify-self: ${it.properties.justifySelf};`;
                    let alignSelf = `\n\talign-self: ${it.properties.alignSelf};`;

                    return `.item-${index} {${gridColumnStart}${gridRowStart}${gridColumnEnd}${gridRowEnd}${justifySelf}${alignSelf}\n}`
                })
                .filter((it) => it !== null)
                .join('\n\n');
            return `${containerCss}${itemsCss?'\n\n'+itemsCss:''}`;
        }),
    );

    this.filtersHtml$ = of('<img class="image" src="/..path/to/image"></div>');

    this.filtersCss$ = this.filtersState$.pipe(
      map((filters) => {
        let filtersCss = '';
        filtersCss += '\n\tfilter: ' +
          (( filters?.blur && (filters?.blur as UnitPicker).value && (filters?.blur as UnitPicker).unit) ? '\n\t\tblur(' + (filters?.blur as UnitPicker).value + (filters?.blur  as UnitPicker).unit + ')' : '') +
          ((filters?.brightness) ? '\n\t\tbrightness(' + filters.brightness + ')' : '') +
          ((filters?.contrast) ? '\n\t\tcontrast(' + filters.contrast + ')' : '') +
          ((filters?.dropShadow) ? 'drop-shadow(' +
            ( (filters?.dropShadow as DropShadowFilter).length1.value && (filters?.dropShadow as DropShadowFilter).length1.unit ? ( (filters?.dropShadow as DropShadowFilter).length1.value + ((filters?.dropShadow as DropShadowFilter).length1.unit || '') + ' ' ) : '' ) +
          ( (filters?.dropShadow as DropShadowFilter).length2.value && (filters?.dropShadow as DropShadowFilter).length2.unit ? ( (filters?.dropShadow as DropShadowFilter).length2.value + ((filters?.dropShadow as DropShadowFilter).length2.unit || '') + ' ' ) : '' ) +
          ( (filters?.dropShadow as DropShadowFilter).length3.value && (filters?.dropShadow as DropShadowFilter).length3.unit ? ( (filters?.dropShadow as DropShadowFilter).length3.value + ((filters?.dropShadow as DropShadowFilter).length3.unit || '') + ' ' ) : '' ) +
          (  (filters?.dropShadow as DropShadowFilter).color ?  (filters?.dropShadow as DropShadowFilter).color : '' ) +
          ')' : '') +
          ((filters?.grayscale) ? '\n\t\tgrayscale(' + filters.grayscale + ')' : '') +
          (( filters?.hueRotate && (filters?.hueRotate as UnitPicker).value && (filters?.hueRotate as UnitPicker).unit) ? '\n\t\thue-rotate(' + (filters?.hueRotate as UnitPicker).value + (filters?.hueRotate  as UnitPicker).unit + ')' : '') +
          ((filters?.invert) ? '\n\t\tinvert(' + filters.invert + ')' : '') +
          ((filters?.opacity) ? '\n\t\topacity(' + filters.opacity + ')' : '') +
          ((filters?.saturate) ? '\n\t\tsaturate(' + filters.saturate + ')' : '') +
          ((filters?.saturate) ? '\n\t\tsaturate(' + filters.saturate + ')' : '')


        return  `.image {\
          \t${filtersCss}\
          \n}`;
      })
    );

    this.animationsHtml$ = of('<div class="animated"></div>');

    this.animationsCss$ = combineLatest([this.animationState$, this.keyframesState$]).pipe(
      map(([animation, keyframes]) => {
        let animationClass = `.animated {\n`;
        animationClass+= `\tanimation-name: ${animation.animationName};\n`;
        animationClass+= `\tanimation-duration: ${animation.animationDuration.value.toString()}${animation.animationDuration.unit};\n`;
        animationClass+= `\tanimation-delay: ${animation.animationDelay.value.toString()}${animation.animationDelay.unit};\n`;
        animationClass+= `\tanimation-timing-function: ${animation.animationTimingFunction};\n`;
        animationClass+= `\tanimation-iteration-count: ${animation.animationIterationCount};\n`;
        animationClass+= `\tanimation-direction: ${animation.animationDirection};\n`;
        animationClass+= `\tanimation-fill-mode: ${animation.animationFillMode};\n`;
        animationClass+= `\tanimation-play-state: ${animation.animationPlayState};\n`;
        animationClass += `}`;

        let keyframesCssString = `@keyframes ${animation.animationName} {\n`;
        for (let keyframe of keyframes) {
          keyframesCssString+= `\t${keyframe.frame}% { \n`;
          let translateY;
          let translateX;
          let includeTranslate = false;
          for (const [animationType, animationValue] of Object.entries(keyframe.value)) {
            switch (animationType) {
              case 'translateX':
                translateX = animationValue as {value: number, unit: string};
                includeTranslate = true;
                break;
              case 'translateY':
                translateY = animationValue as {value: number, unit: string};
                includeTranslate = true;
                break;
              case 'resize':
                let resize = animationValue as {width: {value: number, unit: string}, height: {value: number, unit: string}};
                keyframesCssString+= `\t\twidth: ${resize.width.value}${resize.width.unit};\n\t\theight: ${resize.height.value}${resize.height.unit};\n`;
                break;
              case 'rotate':
                let rotate = animationValue as {value: number, unit: string};
                keyframesCssString+= `\t\trotate: ${rotate.value}${rotate.unit};\n`;
                break;
              case 'scale':
              case 'opacity':
              case 'background':
                keyframesCssString+= `\t\t${animationType}: ${animationValue};\n`;
                break;
            }
          }
          const translateXString = translateX ? `${translateX.value}${translateX.unit}`: '0';
          const translateYString = translateY ? `${translateY.value}${translateY.unit}`: '0';

          if (includeTranslate){
            keyframesCssString+= `\t\ttransform: translate(${translateXString}, ${translateYString});\n`;
          }
          keyframesCssString+= '\t}\n'
        }
        keyframesCssString+= '}'

        return `${animationClass}\n${keyframesCssString}`;
      })
    );

    this.boxModelHtml$ = of('<div class="container"></div>');

    this.boxModelCss$ = this.boxModelState$.pipe(
      map((boxModel) => {
        let containerCss = `.container {\n\tbackground-color: #fb923c /* not customized through the form */\
        \n\tbox-sizing: ${boxModel.boxSizing};\
        \n\twidth: ${boxModel.content.width.value}${boxModel.content.width.unit};\
        \n\theight: ${boxModel.content.height.value}${boxModel.content.height.unit};`;

        const pt = boxModel.padding.top.value + boxModel.padding.top.unit;
        const pr = boxModel.padding.right.value + boxModel.padding.right.unit;
        const pb = boxModel.padding.bottom.value + boxModel.padding.bottom.unit;
        const pl = boxModel.padding.left.value + boxModel.padding.left.unit;

        const allPaddingsEqual = [pt, pr, pb, pl].every((value, index, array) => value === array[0]);

        if (allPaddingsEqual) {
          containerCss+=`\n\tpadding: ${pt};`;
        } else if (pt == pb && pr == pl) {
          containerCss+=`\n\tpadding: ${pt} ${pr};`;
        } else {
          containerCss+=`\n\tpadding-top: ${pt};\
          \n\tpadding-right: ${pr};\
          \n\tpadding-bottom: ${pb};\
          \n\tpadding-left: ${pl};\
          `;
        }

        const bt = boxModel.border.top.size.value + boxModel.border.top.size.unit + ' ' +
          boxModel.border.top.style + ' ' + boxModel.border.top.color;
        const br = boxModel.border.right.size.value + boxModel.border.right.size.unit + ' ' +
          boxModel.border.right.style + ' ' + boxModel.border.right.color;
        const bb = boxModel.border.bottom.size.value + boxModel.border.bottom.size.unit + ' ' +
          boxModel.border.bottom.style + ' ' + boxModel.border.bottom.color;
        const bl = boxModel.border.left.size.value + boxModel.border.left.size.unit + ' ' +
          boxModel.border.left.style + ' ' + boxModel.border.left.color;

        const allBordersEqual = [bt, br, bb, bl].every((value, index, array) => value === array[0]);

        if (allBordersEqual) {
          containerCss+=`\n\tborder: ${bt};`;
        } else if (bt == bb && br == bl) {
          containerCss+=`\n\tborder: ${bt} ${br};`;
        } else {
          containerCss+=`\n\tborder-top: ${bt};\
          \n\tborder-right: ${br};\
          \n\tborder-bottom: ${bb};\
          \n\tborder-left: ${bl};\
          `;
        }

        const mt = boxModel.margin.top.value + boxModel.margin.top.unit;
        const mr = boxModel.margin.right.value + boxModel.margin.right.unit;
        const mb = boxModel.margin.bottom.value + boxModel.margin.bottom.unit;
        const ml = boxModel.margin.left.value + boxModel.margin.left.unit;
        const allMarginsEqual = [mt, mr, mb, ml].every((value, index, array) => value === array[0]);

        if (allMarginsEqual) {
          containerCss += `\n\tmargin: ${mt};`
        } else {
          containerCss+=`\n\tmargin-top: ${mt};\
          \n\tmargin-right: ${mr};\
          \n\tmargin-bottom: ${mb};\
          \n\tmargin-left: ${ml};\
          `;
        }

        containerCss += "\n}"
        return containerCss;
      })
    );

    this.positioningHtml$ = this.positioningItemsState$.pipe(
      map((positioningItems) => {
        const itemsCode = positioningItems.length === 0 ? '' : positioningItems.map((item, index) => {
          return `\t<div class="item-${index}">${index}</div>`
        }).join('\n');

        return `<div class="container">\n${itemsCode}\n</div>`;
      })
    );

    this.positioningCss$ = this.positioningItemsState$.pipe(
      map((positioningItems) => {
        //  flex-wrap overflow-auto
        const containerCss = `.container {\
          \n\t\position: relative;\n\theight: 100%;\n\tdisplay: flex;\n\tflex-direction:row;\n\tflex-wrap: wrap;\
          \n\tflex-grow: 1;\n\talign-items: flex-start;\n\tjustify-items: start;\n\tgap: 0.5rem;\n\ttext-align: center;\
          \n\tpadding: 1rem;\n\toverflow: auto;\n}`

        const itemsCss = positioningItems
          .map((item, index) => {
            let position = `\n\torder: ${item.position};`;
            let top = `\n\ttop: ${item.top.value}${item.top.unit};`;
            let right = `\n\ttop: ${item.right.value}${item.right.unit};`;
            let bottom = `\n\tbottom: ${item.bottom.value}${item.bottom.unit};`;
            let left = `\n\tleft: ${item.left.value}${item.left.unit};`;

            return `.item-${index} {${position}${top}${right}${bottom}${left}\n}`
          })
          .filter((it) => it !== null)
          .join('\n\n');

        return `${containerCss}${itemsCss?'\n\n'+itemsCss:''}`;
      })
    );

    this.transformsHtml$ = this.transformedItemState$.pipe(
      map((item) => {
        let itemHtml = '';
        switch (item.name) {
          case 'rectangle':
            itemHtml = '<div class="rectangle"></div>';
            break;
          case 'circle':
            itemHtml = '<div class="circle"></div>';
            break;
          case 'image':
            itemHtml = '<img class="image" src="/..path/to/image"></div>';
            break;
          case 'cube':
            itemHtml = '<div class="scene">\n' +
              '\t<div class="cube">\n' +
                '\t\t<div class="cube__face cube__face--front">front</div>\n' +
                '\t\t<div class="cube__face cube__face--back">back</div>\n' +
                '\t\t<div class="cube__face cube__face--right">right</div>\n' +
                '\t\t<div class="cube__face cube__face--left">left</div>\n' +
                '\t\t<div class="cube__face cube__face--top">top</div>\n' +
                '\t\t<div class="cube__face cube__face--bottom">bottom</div>\n' +
                '\t</div>\n' + '</div>';
            break;
        }
        return itemHtml;
      })
    );

    this.transformsCss$ = combineLatest([
      this.transformedItemState$, this.transformFunctions2dState$, this.transformFunctions3dState$, this.transformOptionsState$
    ]).pipe(
      map(([item, functions2d, functions3d, transformOptions]) => {

        let transformCss = 'transform-box: ' + transformOptions.box + ';' +
          '\n\ttransform-origin: ' + transformOptions.origin.x.value + transformOptions.origin.x.unit +  ' ' +
          transformOptions.origin.y.value + transformOptions.origin.y.unit + ';' +
          '\n\ttransform-style: ' + transformOptions.style + ';';

        if (item.type == '2d') {
          transformCss += '\n\ttransform: ' +
          ((functions2d?.rotate) ? '\n\t\trotate(' + (functions2d.rotate as UnitPicker).value + (functions2d.rotate  as UnitPicker).unit + ')' : '') +
          ((functions2d?.rotateX) ? '\n\t\trotateX(' + (functions2d.rotateX as UnitPicker).value + (functions2d.rotateX as UnitPicker).unit + ')' : '') +
          ((functions2d?.rotateY) ? '\n\t\trotateY(' + (functions2d.rotateY as UnitPicker).value + (functions2d.rotateY as UnitPicker).unit + ')' : '') +
          ((functions2d?.matrix) ? '\n\t\tmatrix(' + (functions2d.matrix as Matrix).a + ',' +
            (functions2d.matrix as Matrix).b + ',' +
            (functions2d.matrix as Matrix).c + ',' +
            (functions2d.matrix as Matrix).d + ',' +
            (functions2d.matrix as Matrix).tx + ',' +
            (functions2d.matrix as Matrix).ty +
            ')' : '') +
          ((functions2d?.translate) ? '\n\t\ttranslate('
              + ((functions2d.translate as {x: UnitPicker, y: UnitPicker}).x ?
            ((functions2d.translate as {x: UnitPicker, y: UnitPicker}).x.value  + (functions2d.translate as {x: UnitPicker, y: UnitPicker}).x.unit) : 0) + ', '
          + ((functions2d.translate as {x: UnitPicker, y: UnitPicker}).y ?
            ((functions2d.translate as {x: UnitPicker, y: UnitPicker}).y.value  + (functions2d.translate as {x: UnitPicker, y: UnitPicker}).y.unit) : 0) +
            ')' : '') +
            ((functions2d?.translateX) ? '\n\t\ttranslateX(' + (functions2d.translateX as UnitPicker).value + (functions2d.translateX as UnitPicker).unit + ')' : '') +
            ((functions2d?.translateY) ? '\n\t\ttranslateY(' + (functions2d.translateY as UnitPicker).value + (functions2d.translateY as UnitPicker).unit + ')' : '') +
            ((functions2d?.skewX) ? '\n\t\tskewX(' + (functions2d.skewX as UnitPicker).value + (functions2d.skewX  as UnitPicker).unit + ')' : '') +
            ((functions2d?.skewY) ? '\n\t\tskewY(' + (functions2d.skewY as UnitPicker).value + (functions2d.skewY as UnitPicker).unit + ')' : '') +
            ((functions2d?.scale) ? '\n\t\tscale(' + functions2d.scale + ')' : '') +
            ((functions2d?.scaleX) ? '\n\t\tscaleX(' + functions2d.scaleX + ')' : '') +
            ((functions2d?.scaleY) ? '\n\t\tscaleY(' + functions2d.scaleY + ')' : '')
        } else {
          transformCss += 'transform:' +
          ((functions3d?.rotate3d) ? '\n\t\trotate3d(' +
            (functions3d.rotate3d as Rotate3d).x + ',' +
            (functions3d.rotate3d as Rotate3d).y + ',' +
            (functions3d.rotate3d as Rotate3d).z + ',' +
            (functions3d.rotate3d as Rotate3d).a.value + (functions3d.rotate3d as Rotate3d).a.unit + ')' : '') +
          ((functions3d?.rotateX) ? '\n\t\trotateX(' + (functions3d.rotateX as UnitPicker).value + (functions3d.rotateX as UnitPicker).unit + ')' : '') +
          ((functions3d?.rotateY) ? '\n\t\trotateY(' + (functions3d.rotateY as UnitPicker).value + (functions3d.rotateY as UnitPicker).unit + ')' : '') +
          ((functions3d?.rotateZ) ? '\n\t\trotateZ(' + (functions3d.rotateZ as UnitPicker).value + (functions3d.rotateZ as UnitPicker).unit + ')' : '') +
          ((functions3d?.matrix3d) ? '\n\t\tmatrix3d(' +
            (functions3d.matrix3d as Matrix3d).a1 + ',' +
            (functions3d.matrix3d as Matrix3d).b1 + ',' +
            (functions3d.matrix3d as Matrix3d).c1 + ',' +
            (functions3d.matrix3d as Matrix3d).d1 + ',' +
            (functions3d.matrix3d as Matrix3d).a2 + ',' +
            (functions3d.matrix3d as Matrix3d).b2 + ',' +
            (functions3d.matrix3d as Matrix3d).c2 + ',' +
            (functions3d.matrix3d as Matrix3d).d2 + ',' +
            (functions3d.matrix3d as Matrix3d).a3 + ',' +
            (functions3d.matrix3d as Matrix3d).b3 + ',' +
            (functions3d.matrix3d as Matrix3d).c3 + ',' +
            (functions3d.matrix3d as Matrix3d).d3 + ',' +
            (functions3d.matrix3d as Matrix3d).a4 + ',' +
            (functions3d.matrix3d as Matrix3d).b4 + ',' +
            (functions3d.matrix3d as Matrix3d).c4 + ',' +
            (functions3d.matrix3d as Matrix3d).d4 +
          ')' : '') +
          ((functions3d?.perspective) ? '\n\t\ttranslateX(' + (functions3d.perspective as UnitPicker).value + (functions3d.perspective as UnitPicker).unit + ')' : '') +
          ((functions3d?.translate3d) ? '\n\t\ttranslate3d('
            + (functions3d.translate3d as Translate3d).x.value + (functions3d.translate3d as Translate3d).x.unit + ',' +
            + (functions3d.translate3d as Translate3d).y.value + (functions3d.translate3d as Translate3d).y.unit + ',' +
            + (functions3d.translate3d as Translate3d).z.value + (functions3d.translate3d as Translate3d).y.unit +
          ')' : '') +
          ((functions3d?.translateX) ? '\n\t\ttranslateX(' + (functions3d.translateX as UnitPicker).value + (functions3d.translateX as UnitPicker).unit + ')' : '') +
          ((functions3d?.translateY) ? '\n\t\ttranslateY(' + (functions3d.translateY as UnitPicker).value + (functions3d.translateY as UnitPicker).unit + ')' : '') +
          ((functions3d?.translateZ) ? '\n\t\ttranslateZ(' + (functions3d.translateZ as UnitPicker).value + (functions3d.translateZ as UnitPicker).unit + ')' : '') +
          ((functions3d?.scale3d) ?
            '\n\t\tscale3d(' +
            (functions3d.scale3d as Scale3d).x + ', ' +
            (functions3d.scale3d as Scale3d).y + ', ' +
            (functions3d.scale3d as Scale3d).z + ')'
          : '') +
          ((functions3d?.scaleX) ? '\n\t\tscaleX(' + functions3d.scaleX + ')' : '') +
          ((functions3d?.scaleY) ? '\n\t\tscaleY(' + functions3d.scaleY + ')' : '') +
          ((functions3d?.scaleZ) ? '\n\t\tscaleZ(' + functions3d.scaleZ + ')' : '')
        }

        let figureCss = '';
        switch (item.name) {
          case 'rectangle':
            figureCss = `.rectangle {\
              \n\twidth: 12rem; /* 48px */\
              \n\theight: 12rem; /* 48px */\
              \n\tbackground-color: #f97316;\
              \n\tborder-radius: 0.375rem; /* 1.5px */\
              \n\topacity: 0.3;\
              \n\t${transformCss}\
              \n}`;
            break
          case 'circle':
            figureCss = `.circle {\
              \n\twidth: 12rem; /* 48px */\
              \n\theight: 12rem; /* 48px */\
              \n\tbackground-color: #f97316;\
              \n\tborder-radius: 50%;\
              \n\topacity: 0.3;\
              \n\t${transformCss}\
              \n}`;
            break
          case 'image':
            figureCss = `.image {\
              \n\twidth: 12rem; /* 48px */\
              \n\theight: 12rem; /* 48px */\
              \n\tbackground-color: #f97316;\
              \n\tborder-radius: 0.375rem; /* 1.5px */\
              \n\topacity: 0.3;\
              \n\t${transformCss}\
              \n}`;
            break
          case 'cube':
            figureCss = `
.scene {
  width: 200px;
  height: 200px;
  margin: 80px;
  perspective: 400px;
}

.cube {
  width: 200px;
  height: 200px;
  position: relative;
  transform-style: preserve-3d;
  transition: transform 1s;
  ${transformCss}
}

.cube__face {
  position: absolute;
  width: 200px;
  height: 200px;
  border: 2px solid black;
  line-height: 200px;
  font-size: 40px;
  font-weight: bold;
  color: white;
  text-align: center;
}

.cube__face--front {
  background: hsla(25, 95%, 53%, 0.7);
  transform: rotateY(  0deg) translateZ(100px);
}
.cube__face--right {
  background: hsla(45, 93%, 47%, 0.7);
  transform: rotateY( 90deg) translateZ(100px);
}
.cube__face--back {
  background: hsla(84, 81%, 44%, 0.7);
  transform: rotateY(180deg) translateZ(100px);
}
.cube__face--left {
  background: hsla(189, 94%, 43%, 0.7);
  transform: rotateY(-90deg) translateZ(100px)
}
.cube__face--top {
  background: hsla(271, 91%, 65%, 0.7);
  transform: rotateX( 90deg) translateZ(100px);
}
.cube__face--bottom {
  background: hsla(350, 89%, 60%, 0.7);
  transform: rotateX(-90deg) translateZ(100px);
}`;
            break
        }

        return figureCss;
      })
    );

    this.pseudoClassesHtml$ = combineLatest([this.pseudoClassTypeState$, this.pseudoClassState$]).pipe(
      map(([pseudoType, pseudoClass]) => {
        if (pseudoClass == 'fullscreen') {
          return `<div class="fullscreen-container">\
            \n\tContent displayed differently on element fullscreen\
            \n\tvia element.requestFullscreen() method. \
            \n</div>`;
        }

        if (pseudoClass == 'modal') {
          return `<!-- button to trigger modal opening using showModal() method-->\
            \n<button>Open modal</button>\
            \n\n<dialog>\
            \n\t<form method="dialog">\
            \n\t\t<div>\
            \n\t\t\t<p>Modal content.</p>\
            \n\t\t\t<p>Close by clicking the button below or pressing Esc.</p>\
            \n\t\t</div>\
            \n\t\t<button>Close dialog</button>\
            \n\t</form>\
            \n</dialog>`;
        }

        if (pseudoClass == 'picture-in-picture') {
          return `<div id="video-container">\
            \n\t<video controls autoplay id="video" src="../path/to/video.mp4"></video>\
            \n</div>
            \n<!-- button to trigger video picture in picture mode\
            \nvia requestPictureInPicture() method.-->\
            \n<button>Toggle Picture Mode</button>`;
        }

        if (pseudoType == 'input') {
            return `<!-- Input pseudo classes sample code is not related to the form-->\
              \n<label for="name">Name</label>\
              \n<input id="name" name="name" type="text" autocomplete="name" />\
              \n\n<label for="email">Email Address</label>\
              \n<input id="email" name="email" type="email" autocomplete="email" />\
            `;
        }

        if (pseudoType == 'location') {
            return `<div class="nasa-links">\
            \n\t<a\
            \n\t\thref="https://science.nasa.gov/astrophysics/focus-areas/black-holes/"\
            \n\t\ttarget="_blank"\
            \n\t>Black Holes</a>\
            \n\n\t<a\
            \n\t\thref="https://science.nasa.gov/mission/webb/""\
            \n\t\ttarget="_blank"\
            \n\t>James Webb Space Telescope</a>\
            \n\n\t<a\
            \n\t\thref="https://science.nasa.gov/astrophysics/focus-areas/what-are-galaxies/"\
            \n\t\ttarget="_blank"\
            \n\t>Galaxies</a>\
            \n</div>`;
        }

        if (pseudoType == 'treeStructural') {
            return `<div class="container">\
                \n\t<div class="child">1</div>\
                \n\t<div class="child">2</div>\
                \n\t<div class="child">3</div>\
                \n\t<div class="child"></div>\
                \n\t<div class="child">5</div>\
                \n\t<div class="child"></div>\
                \n\t<div class="child">7</div>\
                \n\t<div class="child">8</div>\
                \n\t<div class="child">9</div>\
                \n\t<div class="child">10</div>\
                \n</div>`;
        }

        if (pseudoType == 'userAction') {
            return `<div class="container">\
                \n\t<label for="magical-name">Magical Name</label>\
                \n\t<input type="text" id="magical-name" class="input">\
                \n</div>\
                \n\n<div class="container">\
                \n\t<label for="super-power">Super power</label>\
                \n\t<input type="text" id="super-power" class="input">\
                \n</div>\
                \n\n<div class="container">\
                \n\t<label for="super-power">Super power</label>\
                \n\t<select id="spirit-animal" class="input">\
                \n\t\t<option value="unicorn">Unicorn</option>\
                \n\t\t<option value="dragon">Dragon</option>\
                \n\t\t<option value="sloth">Sloth</option>\
                \n\t</select>\
                \n</div>\
                \n\n<div class="container">\
                \n\t<button class="button">Confirm</button>\
                \n</div>`
        }

        return '<div class="container"></div>';
      }),
    );

    this.pseudoClassesCss$ = combineLatest([this.pseudoClassTypeState$, this.pseudoClassState$, this.pseudoCssOptionsState$]).pipe((
      map(([pseudoType, pseudoClass, cssOptions]) => {
          const cssDefault = `\n\tbackground-color: ${cssOptions.backgroundColor};\
              \n\tborder: ${cssOptions.border.size.value}${cssOptions.border.size.unit} ${cssOptions.border.style} ${cssOptions.border.color};\
              \n\tcolor: ${cssOptions.color};`

          if (pseudoClass == 'fullscreen') {
          return `.fullscreen-container {\
          \n\t background-color: #fff;\
          \n\t color: #4b5563;\
          \n}\
          \n\n.fullscreen-container:fullscreen {\
          ${cssDefault}\
          \n}`;
        }

        if (pseudoClass == 'modal') {
          return `:modal {\
          ${cssDefault}\
          \n}`;
        }

        if (pseudoClass == 'picture-in-picture') {
          return `#video {\
            \n\twidth: 100%;\
            \n}\
            \n\n#video-container {\
              \n\tposition: relative;\
            \n}\
            \n\n#video-container:has(video:picture-in-picture)::after {\
              \n\tcontent: 'Video is now playing in a Picture-in-Picture window';\
              \n\tposition: absolute;\
              ${cssDefault}\
              \n\twidth: 100%;\
              \n\theight: 100%;\
              \n\tpadding: 40px;\
              \n}`;
        }

        if (pseudoType == 'input') {
            return `input:${pseudoClass} {\
                ${cssDefault}\
                \n}`
        }

        // if (['enabled', 'disabled'].includes(pseudoClass)) {
        //     return `*:${pseudoClass} {\
        //         ${cssDefault}\
        //         \n}`;
        // }

        if (pseudoType == 'location') {
            if (pseudoClass == 'scope') {
                return `@scope (.nasa-links) {\
                    \n\t:scope {\
                    \n\t\t/* some styles applied to scope div */\
                    \n\t}\
                    \n\ta {\
                    \n\t\tbackground-color: ${cssOptions.backgroundColor};\
                    \n\t\tborder: ${cssOptions.border.size.value}${cssOptions.border.size.unit} ${cssOptions.border.style} ${cssOptions.border.color};\
                    \n\t\tcolor: ${cssOptions.color};\
                    \n\t}\
                    \n}`
            } else {
                return `a:${pseudoClass} {\
                ${cssDefault}\
                \n}`;
            }
        }

        if (pseudoType == 'treeStructural') {
            return `.child:${pseudoClass} {\
                ${cssDefault}\
                \n}`;
        }

        if (pseudoType == 'userAction') {
            if (pseudoClass == 'focus-within') {
                return `.container:${pseudoClass} {\
                ${cssDefault}\
                \n}`;
            } else {
                return `.input:${pseudoClass}, .button:${pseudoClass} {\
                ${cssDefault}\
                \n}`;
            }
        }

        return '.container {}';
      })
    ));
  }
}
