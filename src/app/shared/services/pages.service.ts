import { Injectable } from '@angular/core';
import {Page} from "../models/page.interface";
import {
  faArrowsTurnToDots, faBox,
  faChartSimple, faCircle,
  faCircleDot, faComputerMouse, faFilter,
  faGrip, faRotateRight,
  faUpDownLeftRight
} from "@fortawesome/free-solid-svg-icons";
import {faCirclePlay} from "@fortawesome/free-regular-svg-icons";

@Injectable({
  providedIn: 'root'
})
export class PagesService {
  private pages: Page[] = [
    {
      name: 'Animations',
      path: 'animations',
      icon: faCirclePlay,
      description: `Create different animations on a rectangle, like moving it horizontally or vertically,
      changing the color or opacity. Control the animations properties and keyframes and see how it looks!`,
      active: true,
    },
    {
      name: 'Box Model',
      path: 'box-model',
      icon: faBox,
      description: `When laying out a document, the browser's rendering engine represents each element as a rectangular
        box according to the standard CSS basic box model. CSS determines the size, position, and properties
        (color, background, border size, etc.) of these boxes. Play around with these properties.`,
      active: true,
    },
    {
      name: 'Flexbox',
      path: 'flexbox',
      icon: faChartSimple,
      description: `Play with css flexbox by adding new items to a container and editing container or individual items
      properties.`,
      active: true,
    },
    {
      name: 'Grid',
      path: 'grid',
      icon: faGrip,
      description: `Play with making a grid of squares bigger or smaller while keeping them in neat rows and columns.`,
      active: true,
    },
    {
      name: 'Filters',
      path: 'filters',
      icon: faFilter,
      description: `Adjust different color filters for an image with pure css!`,
      active: true,
    },
    {
      name: 'Positioning',
      path: 'positioning',
      icon: faUpDownLeftRight,
      description: `Add a list of items to a page and try changing position for individual items and see where will they go.`,
      active: true,
    },
    {
      name: 'Pseudo classes',
      path:'pseudo-classes',
      icon: faCircle,
      description: `Experiment with a large list of pseudo classes divided into groups by their purpose.`,
      active: true,
    },
    {
      name: 'Pseudo elements',
      path:'pseudo-elements',
      icon: faCircleDot,
      description: ``,
      active: false,
    },
    {
      name: 'Scroll',
      path: 'scroll',
      icon: faComputerMouse,
      description: ``,
      active: false,
    },
    {
      name: 'Transforms',
      path: 'transforms',
      icon: faArrowsTurnToDots,
      description: `Visualize an rectangle, circle, image or cube. Try stretching it, rotating it, or moving it around like you're playing
        with a toy.`,
      active: true,
    },
    {
      name: 'Transitions',
      path: 'transitions',
      icon: faRotateRight,
      description: ``,
      active: false,
    },
  ]

  constructor() { }

  getPages() {
    return this.pages.filter(page => page.active);
  }

  getComingSoonPages() {
    return this.pages.filter(page => !page.active);
  }
}
