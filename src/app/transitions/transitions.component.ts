import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComingSoonComponent} from "../shared/ui/coming-soon/coming-soon.component";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-transitions',
  standalone: true,
    imports: [CommonModule, ComingSoonComponent],
  templateUrl: './transitions.component.html',
  styleUrls: ['./transitions.component.scss']
})
export class TransitionsComponent implements OnInit {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Flexbox');
    this.metaTagService.addTags([
      // {name: 'description', content: 'Add div items, set css flexbox properties, visualize the output and get the css code'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Transitions, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
  }

}
