import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransitionsComponent } from './transitions.component';

describe('TransitionsComponent', () => {
  let component: TransitionsComponent;
  let fixture: ComponentFixture<TransitionsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransitionsComponent]
    });
    fixture = TestBed.createComponent(TransitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
