import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComingSoonComponent} from "../shared/ui/coming-soon/coming-soon.component";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-scroll',
  standalone: true,
    imports: [CommonModule, ComingSoonComponent],
  templateUrl: './scroll.component.html',
  styleUrls: ['./scroll.component.scss']
})
export class ScrollComponent implements OnInit {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Scroll');
    this.metaTagService.addTags([
      // {name: 'description', content: 'Add div items, set css flexbox properties, visualize the output and get the css code'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Scroll, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
  }
}
