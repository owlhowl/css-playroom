import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Page} from "../../shared/models/page.interface";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-home-section',
  standalone: true,
  imports: [CommonModule, FontAwesomeModule, RouterLink],
  templateUrl: './home-section.component.html',
  styleUrls: ['./home-section.component.scss']
})
export class HomeSectionComponent {
  @Input() page!: Page;
}
