import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Page} from "../shared/models/page.interface";
import {PagesService} from "../shared/services/pages.service";
import {HomeSectionComponent} from "./home-section/home-section.component";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, HomeSectionComponent],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);
  pagesService = inject(PagesService)
  pages: Page[] = [];

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom');
    this.metaTagService.addTags([
      {name: 'description', content: 'cssplayroom.com is your first and best source for the css information you’re looking for. Visualise the css functionality by selecting and applying elements in a playroom. I hope you find what you are searching for!'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Flexbox, Grid, Animations, Box Model, Filters, Positioning,\s' +
          'Pseudo Classes, Transforms, Pseudo Elements, Scroll, Transitions, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
    this.pages = this.pagesService.getPages();
  }
}
