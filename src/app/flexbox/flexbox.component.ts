import {Component, OnInit, inject, OnDestroy} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as FlexboxSelectors from './store/flexbox.selectors';
import { FlexboxSidebarComponent } from './flexbox-sidebar/flexbox-sidebar.component';
import { FlexboxContainerComponent } from './flexbox-container/flexbox-container.component';
import * as FlexboxActions from "./store/flexbox.actions";
import {CodeContainerComponent} from "../shared/ui/code-container/code-container.component";
import {CodeService} from "../shared/services/code.service";
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-flexbox',
  standalone: true,
  imports: [CommonModule, FlexboxSidebarComponent, FlexboxContainerComponent, CodeContainerComponent],
  templateUrl: './flexbox.component.html',
  styleUrls: ['./flexbox.component.scss']
})
export class FlexboxComponent implements OnInit, OnDestroy {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);
  private store = inject(Store);
  private codeService = inject(CodeService);

  activeTab$!: Observable<string>;
  showCodeContainer$!: Observable<boolean>;
  flexboxHtml$ = this.codeService.flexboxHtml$;
  flexboxCss$ = this.codeService.flexboxCss$;

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Flexbox');
    this.metaTagService.addTags([
      {name: 'description', content: 'cssplayroom.com is your first and best source for the css information you’re looking for. Add div items, set css flexbox properties, visualize the output and get the css code'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Flexbox, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
    this.activeTab$ = this.store.select(FlexboxSelectors.selectActiveTab);
    this.showCodeContainer$ = this.store.select(FlexboxSelectors.selectShowCodeContainer);
  }

  ngOnDestroy() {
    this.store.dispatch(FlexboxActions.resetDefaults());
  }
}
