import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexboxSidebarNavTabComponent } from './flexbox-sidebar-nav-tab.component';

describe('SidebarNavTabComponent', () => {
  let component: FlexboxSidebarNavTabComponent;
  let fixture: ComponentFixture<FlexboxSidebarNavTabComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FlexboxSidebarNavTabComponent]
    });
    fixture = TestBed.createComponent(FlexboxSidebarNavTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
