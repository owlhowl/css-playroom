import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-flexbox-sidebar-nav-tab',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './flexbox-sidebar-nav-tab.component.html',
  styleUrls: ['./flexbox-sidebar-nav-tab.component.scss']
})
export class FlexboxSidebarNavTabComponent {
  @Input() name!: string;
  @Input() isActive!: boolean;
}
