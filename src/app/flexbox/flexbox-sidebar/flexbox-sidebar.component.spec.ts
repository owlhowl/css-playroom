import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexboxSidebarComponent } from './flexbox-sidebar.component';

describe('FlexboxSidebarComponent', () => {
  let component: FlexboxSidebarComponent;
  let fixture: ComponentFixture<FlexboxSidebarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FlexboxSidebarComponent]
    });
    fixture = TestBed.createComponent(FlexboxSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
