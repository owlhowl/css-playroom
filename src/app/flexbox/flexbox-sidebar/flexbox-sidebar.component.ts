import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import * as FlexboxSelectors from './../store/flexbox.selectors';
import { Observable } from 'rxjs';
import { FlexboxSidebarNavComponent } from '../flexbox-sidebar-nav/flexbox-sidebar-nav.component';
import { FlexboxSidebarContainerComponent } from '../flexbox-sidebar-container/flexbox-sidebar-container.component';
import { FlexboxSidebarItemsComponent } from '../flexbox-sidebar-items/flexbox-sidebar-items.component';

@Component({
  selector: 'app-flexbox-sidebar',
  standalone: true,
  imports: [CommonModule, FlexboxSidebarNavComponent, FlexboxSidebarContainerComponent, FlexboxSidebarItemsComponent, FlexboxSidebarItemsComponent],
  templateUrl: './flexbox-sidebar.component.html',
  styleUrls: ['./flexbox-sidebar.component.scss']
})
export class FlexboxSidebarComponent implements OnInit {
  private store = inject(Store);

  activeTab$!: Observable<string>;

  ngOnInit() {
    this.activeTab$ = this.store.select(FlexboxSelectors.selectActiveTab);
  }
}
