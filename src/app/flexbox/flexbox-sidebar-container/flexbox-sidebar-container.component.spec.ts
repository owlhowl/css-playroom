import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexboxSidebarContainerComponent } from './flexbox-sidebar-container.component';

describe('SidebarContainerComponent', () => {
  let component: FlexboxSidebarContainerComponent;
  let fixture: ComponentFixture<FlexboxSidebarContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FlexboxSidebarContainerComponent]
    });
    fixture = TestBed.createComponent(FlexboxSidebarContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
