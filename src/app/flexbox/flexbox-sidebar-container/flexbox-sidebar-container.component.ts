import {Component, inject, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexContainer} from "../store/flexbox.reducer";
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import * as FlexboxSelectors from "../store/flexbox.selectors";
import {FlexboxSidebarContainerSelectComponent} from "../flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {FlexboxSidebarContainerFormComponent} from "../flexbox-sidebar-container-form/flexbox-sidebar-container-form.component";

@Component({
  selector: 'app-flexbox-sidebar-container',
  standalone: true,
  imports: [CommonModule, FlexboxSidebarContainerSelectComponent, FlexboxSidebarContainerFormComponent],
  templateUrl: './flexbox-sidebar-container.component.html',
  styleUrls: ['./flexbox-sidebar-container.component.scss']
})
export class FlexboxSidebarContainerComponent implements OnInit {
  private store = inject(Store);
  flexboxContainer$!: Observable<FlexContainer>;

  ngOnInit() {
    this.flexboxContainer$ = this.store.pipe(select(FlexboxSelectors.selectFlexContainer));
  }
}
