import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromFlexbox from './flexbox.reducer';


export const selectFlexboxState = createFeatureSelector<fromFlexbox.State>(
    fromFlexbox.flexboxFeatureKey
);

export const selectActiveTab = createSelector(
  selectFlexboxState,
  (state: fromFlexbox.State) => state.activeTab,
);

export const selectFlexContainer = createSelector(
  selectFlexboxState,
  (state: fromFlexbox.State) => state.container,
);

export const selectFlexItems = createSelector(
  selectFlexboxState,
  (state: fromFlexbox.State) => state.flexItems,
);

export const selectSelectedItem = createSelector(
  selectFlexboxState,
  (state: fromFlexbox.State) => state.selectedItem,
);

export const selectShowCodeContainer = createSelector(
  selectFlexboxState,
  (state: fromFlexbox.State) => state.showCodeContainer,
);
