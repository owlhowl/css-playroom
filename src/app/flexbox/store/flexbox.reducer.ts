import { createReducer, on } from '@ngrx/store';
import * as FlexboxActions from './flexbox.actions';
import { v4 as uuidv4 } from 'uuid';
import {UnitPicker} from "../../shared/models/unit-picker";

export interface FlexItem {
  id: string,
  styles: FlexItemStyles,
}

export interface FlexItemStyles {
  order: number,
  flexGrow: number,
  flexShrink: number,
  flexBasis: string | number,
  alignSelf: string,
}

export interface FlexContainer {
  flexDirection: string,
  flexWrap: string,
  flexFlow: string,
  justifyContent:string,
  alignItems: string,
  alignContent: string,
  rowGap: UnitPicker,
  columnGap: UnitPicker,
}

export interface State {
  container: FlexContainer,
  flexItems: FlexItem[],
  selectedItem: FlexItem | null,
  activeTab: ActiveTab,
  showCodeContainer: boolean,
}

export type ActiveTab = 'container' | 'items';

export const flexProperties = {
  container: {
    flexDirection: ['row', 'row-reverse', 'column', 'column-reverse'],
    flexWrap: ['nowrap', 'wrap', 'wrap-reverse'],
    flexFlow: ['row nowrap'],
    justifyContent: [
      'flex-start',
      'flex-end',
      'center',
      'space-between',
      'space-around',
      'space-evenly',
    ],
    alignItems: [
      'flex-start',
      'flex-end',
      'center',
      'stretch',
      'baseline',
    ],
    alignContent: [
      'flex-start',
      'flex-end',
      'center',
      'stretch',
      'space-between',
      'space-around',
    ],
    rowGap: {
      unit: 'px',
      value: 0
    },
    columnGap: {
      unit: 'px',
      value: 0
    },
  },
  items: {
    alignSelf: [
      'auto',
      'flex-start',
      'flex-end',
      'center',
      'baseline',
      'stretch',
    ],
  }
}

export const flexboxFeatureKey = 'flexbox';

const initialState: State = {
  container: {
    flexDirection: flexProperties.container.flexDirection[0],
    flexWrap: flexProperties.container.flexWrap[0],
    flexFlow: flexProperties.container.flexFlow[0],
    justifyContent: flexProperties.container.justifyContent[0],
    alignItems: flexProperties.container.alignItems[0],
    alignContent: flexProperties.container.alignContent[0],
    rowGap: flexProperties.container.rowGap,
    columnGap: flexProperties.container.columnGap,
  },
  flexItems: [],
  selectedItem: null,
  activeTab: 'container',
  showCodeContainer: false,
};

export const flexboxReducer = createReducer(
  initialState,
  on(FlexboxActions.setFlexContainer, (state, action) => {
    return {
      ...state,
      container: {
        ...state.container,
        flexDirection: action.value.flexDirection,
        flexWrap: action.value.flexWrap,
        flexFlow: `${action.value.flexDirection} ${action.value.flexFlow}`,
        justifyContent: action.value.justifyContent,
        alignItems: action.value.alignItems,
        alignContent: action.value.alignContent,
        rowGap: {
          ...state.container.rowGap,
          unit: action.value.rowGap.unit,
          value: action.value.rowGap.value,
        },
        columnGap: {
          ...state.container.columnGap,
          unit: action.value.columnGap.unit,
          value: action.value.columnGap.value,
        },
      },
    }
  }),
  on(FlexboxActions.setActiveTab, (state, action) => {
    return {
      ...state,
      activeTab: action.value,
    }
  }),
  on(FlexboxActions.addFlexItem, (state) => {
    return {
      ...state,
      flexItems: [
        ...state.flexItems, {
        id: uuidv4(),
        styles: {
          order: 0,
          flexGrow: 0,
          flexShrink: 1,
          flexBasis: 'auto',
          alignSelf: 'auto',
        }
      } as FlexItem]
    }
  }),
  on(FlexboxActions.setSelectedItem, (state, action) => {
    const selectedItem = state.selectedItem;
    const foundItem = state.flexItems.find(item => item.id === action.id) as FlexItem;

    return {
      ...state,
      selectedItem: selectedItem?.id === action.id ? null : {
        ...foundItem,
        styles: {
          ...foundItem.styles
        }
      },
    };
  }),
  on(FlexboxActions.removeFlexItem, (state, action) => {
    return {
      ...state,
      selectedItem: state.selectedItem?.id == action.id ? null : state.selectedItem as FlexItem,
      flexItems: state.flexItems.filter(item => item.id != action.id),
    }
  }),
  on(FlexboxActions.updateSelectedItem, (state, action) => {
    const updatedStyles = {
      order: action.value.order,
      flexGrow: action.value.flexGrow,
      flexShrink: action.value.flexShrink,
      flexBasis: action.value.flexBasis,
      alignSelf: action.value.alignSelf,
    };

    return {
      ...state,
      flexItems: [...state.flexItems.map(item =>
        item.id === state.selectedItem?.id ? { ...item, styles: updatedStyles } : item
      )],
      selectedItem: {
        ...state.selectedItem,
        styles: {
          ...state.selectedItem?.styles,
          ...updatedStyles
        } as FlexItemStyles,
      } as FlexItem
    };
  }),
  on(FlexboxActions.toggleCodeContainer, (state) => {
    return {
      ...state,
      showCodeContainer: !state.showCodeContainer,
    }
  }),
  on(FlexboxActions.resetDefaults, () => initialState),
);
