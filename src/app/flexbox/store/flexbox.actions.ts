import { createAction, props } from "@ngrx/store";
import {FlexContainer, FlexItemStyles} from "./flexbox.reducer";

export const setFlexContainer = createAction(
  '[Flexbox] Set flex container',
  props<{value: FlexContainer}>(),
);

export const addFlexItem = createAction(
    '[Flexbox] Add flex item',
);

export const removeFlexItem = createAction(
    '[Flexbox] Remove flex item',
    props<{id: string}>(),
);

export const setSelectedItem = createAction(
    '[Flexbox] Set selected item',
    props<{id: string}>(),
);

export const updateSelectedItem = createAction(
    '[Flexbox] Update selected item',
    props<{value: FlexItemStyles}>(),
);

export const setActiveTab = createAction(
    '[Flexbox] Set active tab',
    props<{value: 'container' | 'items'}>(),
);

export const resetDefaults = createAction(
    '[Flexbox] Reset defaults',
);

export const toggleCodeContainer = createAction(
  '[Flexbox] Toggle Code Container',
);
