import {
  Component,
  inject,
  Input, OnChanges,
  OnDestroy,
  OnInit, SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FlexItem, flexProperties} from "../store/flexbox.reducer";
import {FormBuilder, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Subject, takeUntil, tap} from "rxjs";
import {FormControlPipe} from "../../shared/pipes/form-control.pipe";
import {FlexboxSidebarContainerSelectComponent} from "../flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {Store} from "@ngrx/store";
import * as FlexboxActions from "../store/flexbox.actions";

@Component({
  selector: 'app-flexbox-sidebar-items-form',
  standalone: true,
  imports: [CommonModule, FormControlPipe, ReactiveFormsModule, FlexboxSidebarContainerSelectComponent],
  templateUrl: './flexbox-sidebar-items-form.component.html',
  styleUrls: ['./flexbox-sidebar-items-form.component.scss'],
})
export class FlexboxSidebarItemsFormComponent implements OnChanges, OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() selectedItem!: FlexItem;
  selectedItemForm!: FormGroup;
  destroy$: Subject<void> = new Subject();
  alignOptions = flexProperties.items.alignSelf;
  flex = '';

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedItem'] && !changes['selectedItem'].firstChange) {
      this.selectedItemForm.patchValue(changes['selectedItem']?.currentValue.styles, {
        onlySelf: true, emitEvent: false
      });
      this.setFlex();
    }
  }

  ngOnInit(){
    this.selectedItemForm = this.fb.group({
      order: [this.selectedItem.styles.order],
      flexGrow: [this.selectedItem.styles.flexGrow],
      flexShrink: [this.selectedItem.styles.flexShrink],
      flexBasis: [this.selectedItem.styles.flexBasis],
      alignSelf: [this.selectedItem.styles.alignSelf],
    });

    this.setFlex();

    this.selectedItemForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      tap((formValue) => {
        this.store.dispatch(FlexboxActions.updateSelectedItem({value: formValue}));
      }),
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  setFlex() {
    this.flex = `${this.selectedItem.styles.flexGrow} ${this.selectedItem.styles.flexShrink} ${this.selectedItem.styles.flexBasis}`;
  }
}
