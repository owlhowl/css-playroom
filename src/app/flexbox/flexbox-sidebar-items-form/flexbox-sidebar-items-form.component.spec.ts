import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexboxSidebarItemsFormComponent } from './flexbox-sidebar-items-form.component';

describe('SidebarItemsFormComponent', () => {
  let component: FlexboxSidebarItemsFormComponent;
  let fixture: ComponentFixture<FlexboxSidebarItemsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FlexboxSidebarItemsFormComponent]
    });
    fixture = TestBed.createComponent(FlexboxSidebarItemsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
