import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexboxSidebarContainerSelectComponent } from './flexbox-sidebar-container-select.component';

describe('SidebarContainerSelectComponent', () => {
  let component: FlexboxSidebarContainerSelectComponent;
  let fixture: ComponentFixture<FlexboxSidebarContainerSelectComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FlexboxSidebarContainerSelectComponent]
    });
    fixture = TestBed.createComponent(FlexboxSidebarContainerSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
