import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormControl, ReactiveFormsModule} from "@angular/forms";

@Component({
  selector: 'app-flexbox-sidebar-container-select',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './flexbox-sidebar-container-select.component.html',
  styleUrls: ['./flexbox-sidebar-container-select.component.scss']
})
export class FlexboxSidebarContainerSelectComponent {
  @Input() selectFormControl!: FormControl;
  @Input() value!: string;
  @Input() label!: string;
  @Input() options!: string[];

  ngOnInit(){
  }
}
