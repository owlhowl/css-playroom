import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexboxSidebarContainerFormComponent } from './flexbox-sidebar-container-form.component';

describe('SidebarContainerFormComponent', () => {
  let component: FlexboxSidebarContainerFormComponent;
  let fixture: ComponentFixture<FlexboxSidebarContainerFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FlexboxSidebarContainerFormComponent]
    });
    fixture = TestBed.createComponent(FlexboxSidebarContainerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
