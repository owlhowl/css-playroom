import {Component, inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {CommonModule, KeyValue} from '@angular/common';
import {FormBuilder, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {FlexContainer, flexProperties} from "../store/flexbox.reducer";
import {FlexboxSidebarContainerSelectComponent} from "../flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {Subject, takeUntil, tap} from "rxjs";
import {Store} from "@ngrx/store";
import * as FlexboxActions from "../store/flexbox.actions";
import {FormControlPipe} from "../../shared/pipes/form-control.pipe";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";
import {kebabize} from "../../shared/utils/kebabize";

type PartialFlexContainer = Pick<FlexContainer, 'flexDirection' | 'flexWrap' | 'justifyContent' | 'alignItems' | 'alignContent'>;

@Component({
  selector: 'app-flexbox-sidebar-container-form',
  standalone: true,
  imports: [
    CommonModule,
    FlexboxSidebarContainerSelectComponent,
    ReactiveFormsModule,
    FormControlPipe,
    UnitPickerComponent
  ],
  templateUrl: './flexbox-sidebar-container-form.component.html',
  styleUrls: ['./flexbox-sidebar-container-form.component.scss']
})
export class FlexboxSidebarContainerFormComponent implements OnChanges, OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() flexContainer!: FlexContainer;
  flexContainerSelects!: PartialFlexContainer;
  flexPropertiesForm!: FormGroup;
  flexProperties = flexProperties.container;
  destroy$: Subject<void> = new Subject()
  gap = '';
  flexFlow = '';

  constructor() {}

  get rowGap() {
    return (<FormGroup>this.flexPropertiesForm.get('rowGap'));
  }

  get columnGap() {
    return (<FormGroup>this.flexPropertiesForm.get('columnGap'));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['flexContainer'] && !changes['flexContainer'].firstChange) {
      this.flexPropertiesForm.patchValue(changes['flexContainer']['currentValue'], {
        onlySelf: true, emitEvent: false
      });
      this.setGap();
      this.setFlexFlow();
    }
  }

  ngOnInit() {
    this.flexPropertiesForm = this.fb.group({
      flexDirection: [this.flexContainer.flexDirection],
      flexWrap: [this.flexContainer.flexWrap],
      flexFlow: [this.flexContainer.flexFlow],
      justifyContent: [this.flexContainer.justifyContent],
      alignItems: [this.flexContainer.alignItems],
      alignContent: [this.flexContainer.alignContent],
      rowGap: this.fb.group({
        value: [this.flexContainer.rowGap.value],
        unit: [this.flexContainer.rowGap.unit],
      }),
      columnGap: this.fb.group({
        value: [this.flexContainer.columnGap.value],
        unit: [this.flexContainer.columnGap.unit],
      }),
    });

    this.setGap();
    this.setFlexFlow();

    this.flexContainerSelects = {
      flexDirection: this.flexContainer.flexDirection,
      flexWrap: this.flexContainer.flexWrap,
      justifyContent: this.flexContainer.justifyContent,
      alignItems: this.flexContainer.alignItems,
      alignContent: this.flexContainer.alignContent,
    }

    this.flexPropertiesForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      tap((formValue) => {
        this.store.dispatch(FlexboxActions.setFlexContainer({value: formValue}));
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  setGap() {
    if (this.flexContainer.rowGap.value == this.flexContainer.columnGap.value &&
      this.flexContainer.rowGap.unit == this.flexContainer.columnGap.unit
    ) {
      this.gap = `${this.flexContainer.rowGap.value}${this.flexContainer.rowGap.unit}`;
    } else {
      this.gap = `${this.flexContainer.rowGap.value}${this.flexContainer.rowGap.unit} ${this.flexContainer.columnGap.value}${this.flexContainer.columnGap.unit}`;
    }
  }

  setFlexFlow() {
    this.flexFlow = `${this.flexContainer.flexDirection} ${this.flexContainer.flexWrap}`;
  }

  kebabize = kebabize;

  originalOrder = (a: KeyValue<keyof PartialFlexContainer, string>, b: KeyValue<keyof PartialFlexContainer, string>): number => {
    return 0;
  }
}
