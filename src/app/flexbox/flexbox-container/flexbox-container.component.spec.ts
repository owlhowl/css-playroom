import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexboxContainerComponent } from './flexbox-container.component';

describe('FlexboxContainerComponent', () => {
  let component: FlexboxContainerComponent;
  let fixture: ComponentFixture<FlexboxContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FlexboxContainerComponent]
    });
    fixture = TestBed.createComponent(FlexboxContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
