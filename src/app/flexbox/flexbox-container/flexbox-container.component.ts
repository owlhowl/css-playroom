import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Observable} from "rxjs";
import {FlexContainer, FlexItem} from "../store/flexbox.reducer";
import {Store} from "@ngrx/store";
import * as FlexboxSelectors from "../store/flexbox.selectors";
import * as FlexboxActions from "../store/flexbox.actions";
import {faXmark} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

@Component({
  selector: 'app-flexbox-container',
  standalone: true,
  imports: [CommonModule, FontAwesomeModule],
  templateUrl: './flexbox-container.component.html',
  styleUrls: ['./flexbox-container.component.scss']
})
export class FlexboxContainerComponent implements OnInit {
  private store = inject(Store);

  flexboxContainer$!: Observable<FlexContainer>;
  flexboxItems$!: Observable<FlexItem[]>;
  selectedItem$!: Observable<FlexItem | null>;
  faDelete = faXmark;

  ngOnInit() {
    this.flexboxContainer$ = this.store.select(FlexboxSelectors.selectFlexContainer);
    this.flexboxItems$ = this.store.select(FlexboxSelectors.selectFlexItems);
    this.selectedItem$ = this.store.select(FlexboxSelectors.selectSelectedItem);
  }

  setSelectedItem(index: string) {
    this.store.dispatch(FlexboxActions.setActiveTab({value: 'items'}));
    this.store.dispatch(FlexboxActions.setSelectedItem({id: index}));
  }

  removeFlexItem(id: string) {
    this.store.dispatch(FlexboxActions.removeFlexItem({id: id}));
  }
}
