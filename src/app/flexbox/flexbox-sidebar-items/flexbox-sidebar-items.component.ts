import { Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FlexboxSidebarContainerSelectComponent} from "../flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {Store, select} from "@ngrx/store";
import {Observable} from "rxjs";
import {FlexItem} from "../store/flexbox.reducer";
import * as FlexboxSelectors from "../store/flexbox.selectors";
import {FlexboxSidebarItemsFormComponent} from "../flexbox-sidebar-items-form/flexbox-sidebar-items-form.component";

@Component({
  selector: 'app-flexbox-sidebar-items',
  standalone: true,
  imports: [CommonModule, FlexboxSidebarContainerSelectComponent, FlexboxSidebarItemsFormComponent],
  templateUrl: './flexbox-sidebar-items.component.html',
  styleUrls: ['./flexbox-sidebar-items.component.scss'],
})
export class FlexboxSidebarItemsComponent implements OnInit {
  private store = inject(Store);

  selectedItem$!: Observable<FlexItem | null>;

  ngOnInit() {
    this.selectedItem$ = this.store.pipe(select(FlexboxSelectors.selectSelectedItem));
  }
}
