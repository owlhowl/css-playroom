import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexboxSidebarItemsComponent } from './flexbox-sidebar-items.component';

describe('SidebarItemsComponent', () => {
  let component: FlexboxSidebarItemsComponent;
  let fixture: ComponentFixture<FlexboxSidebarItemsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FlexboxSidebarItemsComponent]
    });
    fixture = TestBed.createComponent(FlexboxSidebarItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
