import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexboxSidebarNavComponent } from './flexbox-sidebar-nav.component';

describe('SidebarNavComponent', () => {
  let component: FlexboxSidebarNavComponent;
  let fixture: ComponentFixture<FlexboxSidebarNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FlexboxSidebarNavComponent]
    });
    fixture = TestBed.createComponent(FlexboxSidebarNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
