import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from '@ngrx/store';
import { Observable } from 'rxjs';
import * as FlexboxSelectors from './../store/flexbox.selectors';
import * as FlexboxActions from './../store/flexbox.actions';
import { FlexboxSidebarNavTabComponent } from '../flexbox-sidebar-nav-tab/flexbox-sidebar-nav-tab.component';
import { ActiveTab } from '../store/flexbox.reducer';

@Component({
  selector: 'app-flexbox-sidebar-nav',
  standalone: true,
  imports: [CommonModule, FlexboxSidebarNavTabComponent],
  templateUrl: './flexbox-sidebar-nav.component.html',
  styleUrls: ['./flexbox-sidebar-nav.component.scss']
})
export class FlexboxSidebarNavComponent implements  OnInit {
  private store = inject(Store);

  tabs: {
    name: string,
    key: ActiveTab
  }[] = [
    { name: 'Container', key: 'container', },
    { name: 'Items', key: 'items' },
  ];
  activeTab$!: Observable<string>;
  showCodeContainer$!: Observable<boolean>;

  ngOnInit() {
    this.activeTab$ = this.store.pipe(select(FlexboxSelectors.selectActiveTab));
    this.showCodeContainer$ = this.store.pipe(select(FlexboxSelectors.selectShowCodeContainer));
  }

  setActiveTab(key: ActiveTab) {
    this.store.dispatch(FlexboxActions.setActiveTab({value: key}));
  }

  resetDefaults() {
    this.store.dispatch(FlexboxActions.resetDefaults());
  }

  addFlexItem() {
    this.store.dispatch(FlexboxActions.addFlexItem());
  }

  toggleCodeContainer() {
    this.store.dispatch(FlexboxActions.toggleCodeContainer());
  }
}
