import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsSidebarComponent } from './transforms-sidebar.component';

describe('TransformsSidebarComponent', () => {
  let component: TransformsSidebarComponent;
  let fixture: ComponentFixture<TransformsSidebarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsSidebarComponent]
    });
    fixture = TestBed.createComponent(TransformsSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
