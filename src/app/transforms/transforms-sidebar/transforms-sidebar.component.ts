import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import * as TransformsSelectors from "../../transforms/store/transforms.selectors";
import {
  AnimationsSidebarAnimationComponent
} from "../../animations/animations-sidebar-animation/animations-sidebar-animation.component";
import {
  AnimationsSidebarKeyframesComponent
} from "../../animations/animations-sidebar-keyframes/animations-sidebar-keyframes.component";
import {AnimationsSidebarNavComponent} from "../../animations/animations-sidebar-nav/animations-sidebar-nav.component";
import {
  TransformsSidebarFunctionsComponent
} from "../transforms-sidebar-functions/transforms-sidebar-functions.component";
import {
  TransformsSidebarItemFormComponent
} from "../transforms-sidebar-item-form/transforms-sidebar-item-form.component";
import {TransformsSidebarNavComponent} from "../transforms-sidebar-nav/transforms-sidebar-nav.component";
import {TransformsSidebarItemComponent} from "../transforms-sidebar-item/transforms-sidebar-item.component";

@Component({
  selector: 'app-transforms-sidebar',
  standalone: true,
  imports: [CommonModule, AnimationsSidebarAnimationComponent, AnimationsSidebarKeyframesComponent, AnimationsSidebarNavComponent, TransformsSidebarFunctionsComponent, TransformsSidebarItemFormComponent, TransformsSidebarNavComponent, TransformsSidebarItemComponent],
  templateUrl: './transforms-sidebar.component.html',
  styleUrls: ['./transforms-sidebar.component.scss']
})
export class TransformsSidebarComponent {
  private store = inject(Store);

  activeTab$!: Observable<string>;

  ngOnInit() {
    this.activeTab$ = this.store.select(TransformsSelectors.selectActiveTab);
  }
}
