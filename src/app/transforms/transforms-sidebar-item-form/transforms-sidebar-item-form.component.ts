import {Component, inject, Input, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  transformBoxes,
  TransformedItem,
  transformedItems,
  TransformOptions,
  transformStyles
} from "../store/transforms.reducer";
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {debounceTime, Subject, take, takeUntil, tap} from "rxjs";
import * as TransformsActions from "../../transforms/store/transforms.actions";
import {Store} from "@ngrx/store";
import {kebabize} from "../../shared/utils/kebabize";
import {
  FlexboxSidebarContainerSelectComponent
} from "../../flexbox/flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {FormControlPipe} from "../../shared/pipes/form-control.pipe";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";

@Component({
  selector: 'app-transforms-sidebar-item-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, FlexboxSidebarContainerSelectComponent, FormControlPipe, UnitPickerComponent],
  templateUrl: './transforms-sidebar-item-form.component.html',
  styleUrls: ['./transforms-sidebar-item-form.component.scss']
})
export class TransformsSidebarItemFormComponent implements OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() transformOptionsState!: TransformOptions;
  @Input() transformedItemState!: TransformedItem;
  selectedItemFormControl!: FormControl;
  transformOptionsForm!: FormGroup;
  transformedItems = [...transformedItems];
  destroy$: Subject<void> = new Subject();
  transformBoxes = [...transformBoxes];
  transformStyles = [...transformStyles];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['transformOptionsState'] && !changes['transformOptionsState'].firstChange) {
      this.transformOptionsForm.patchValue(changes['transformOptionsState'].currentValue, { emitEvent: false });
    }
  }

  ngOnInit() {
    this.selectedItemFormControl = new FormControl<any>(this.transformedItemState.name);
    this.transformOptionsForm = this.fb.group({
      box: [this.transformOptionsState.box],
      origin: this.fb.group({
        x: this.fb.group({
          value: [this.transformOptionsState.origin.x.value],
          unit: [this.transformOptionsState.origin.x.unit],
        }),
        y: this.fb.group({
          value: [this.transformOptionsState.origin.y.value],
          unit: [this.transformOptionsState.origin.y.unit],
        }),
      }),
      style: [this.transformOptionsState.style]
    });

    this.selectedItemFormControl.valueChanges.pipe(
      takeUntil(this.destroy$),
      tap((formValue) => {
        this.store.dispatch(TransformsActions.setTransformedItem({value: formValue}));
      })
    ).subscribe();

    this.transformOptionsForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(500),
      tap((formValue) => {
        this.store.dispatch(TransformsActions.setTransformOptions({value: formValue}));
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  get originX() {
    return (<FormGroup>this.transformOptionsForm.get('origin.x'));
  }

  get originY() {
    return (<FormGroup>this.transformOptionsForm.get('origin.y'));
  }
}
