import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-transforms-sidebar-nav-tab',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './transforms-sidebar-nav-tab.component.html',
  styleUrls: ['./transforms-sidebar-nav-tab.component.scss']
})
export class TransformsSidebarNavTabComponent {
  @Input() name!: string;
  @Input() isActive!: boolean;
}
