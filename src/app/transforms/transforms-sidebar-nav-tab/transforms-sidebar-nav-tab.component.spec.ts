import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsSidebarNavTabComponent } from './transforms-sidebar-nav-tab.component';

describe('TransformsSidebarNavTabComponent', () => {
  let component: TransformsSidebarNavTabComponent;
  let fixture: ComponentFixture<TransformsSidebarNavTabComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsSidebarNavTabComponent]
    });
    fixture = TestBed.createComponent(TransformsSidebarNavTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
