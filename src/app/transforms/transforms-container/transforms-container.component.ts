import {Component, inject, OnInit} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {Store} from "@ngrx/store";
import * as TransformSelectors from "../../transforms/store/transforms.selectors";
import {Observable} from "rxjs";
import {
  Matrix,
  Matrix3d,
  Rotate3d, Scale3d,
  Transform2DValues,
  Transform3DValues,
  TransformedItem, TransformOptions, Translate3d
} from "../store/transforms.reducer";
import {
  AnimationsContainerElementComponent
} from "../../animations/animations-container-element/animations-container-element.component";
import {ToggleViewService} from "../services/toggle-view.service";
import {AsPipe} from "../../shared/pipes/as.pipe";
import {UnitPicker} from '../../shared/models/unit-picker';

@Component({
  selector: 'app-transforms-container',
  standalone: true,
  imports: [CommonModule, AnimationsContainerElementComponent, NgOptimizedImage, AsPipe],
  templateUrl: './transforms-container.component.html',
  styleUrls: ['./transforms-container.component.scss']
})
export class TransformsContainerComponent implements OnInit {
  private store = inject(Store);
  private toggleViewService = inject(ToggleViewService);
  
  UnitPicker!: UnitPicker;
  Matrix!: Matrix;
  Matrix3d!: Matrix3d;
  Rotate3d!: Rotate3d;
  Scale3d!: Scale3d;
  Translate3d!: Translate3d;
  viewOriginal$ = this.toggleViewService.viewOriginal$;
  transformedItem$!: Observable<TransformedItem>;
  transformOptions$!: Observable<TransformOptions>;
  animations2D$!: Observable<Transform2DValues|null>;
  animations3D$!: Observable<Transform3DValues|null>;

  ngOnInit() {
    this.transformedItem$ = this.store.select(TransformSelectors.selectTransformedItem);
    this.animations2D$ = this.store.select(TransformSelectors.selectTransformFunctions2D);
    this.animations3D$ = this.store.select(TransformSelectors.selectTransformFunctions3D);
    this.transformOptions$ = this.store.select(TransformSelectors.selectTransformOptions);
  }
}
