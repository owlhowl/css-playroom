import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsContainerComponent } from './transforms-container.component';

describe('TransformsContainerComponent', () => {
  let component: TransformsContainerComponent;
  let fixture: ComponentFixture<TransformsContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsContainerComponent]
    });
    fixture = TestBed.createComponent(TransformsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
