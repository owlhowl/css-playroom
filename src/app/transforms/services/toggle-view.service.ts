import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ToggleViewService {
  viewOriginal$ = new BehaviorSubject<boolean>(false);

  constructor() { }
}
