import {createReducer, on} from "@ngrx/store";
import * as TransformsActions from "../../transforms/store/transforms.actions";
import {act} from "@ngrx/effects";
import {UnitPicker} from "../../shared/models/unit-picker";


export const transformsFeatureKey = 'transforms';
export const transformBoxes = [
  'content-box', 'border-box', 'fill-box', 'stroke-box', 'view-box'
] as const;
export const transformStyles = ['flat', 'preserve-3d'] as const;

export const transformedItems = [
  {
    name: 'rectangle',
    type: '2d',
  },
  {
    name: 'circle',
    type: '2d',
  },
  {
    name: 'image',
    type: '2d',
  },
  {
    name: 'cube',
    type: '3d',
  }
]
export const transforms2D = [
  'matrix', 'rotate', 'rotateX', 'rotateY', 'translate', 'translateX', 'translateY', 'scale', 'scaleX',
  'scaleY', 'skewX', 'skewY',
] as const;
export const transforms3D = [
  'matrix3d', 'perspective', 'rotate3d', 'rotateX', 'rotateY', 'rotateZ', 'translate3d',
  'translateX', 'translateY', 'translateZ', 'scale3d', 'scaleX', 'scaleY', 'scaleZ'
] as const;

export const initialValuesTransform2D = {
  matrix: { a: 0, b: 0, c: 0, d: 0, tx: 0, ty: 0, },
  rotate: { value: 0, unit: 'deg', },
  rotateX: { value: 0, unit: 'deg', },
  rotateY: { value: 0, unit: 'deg', },
  translateX: { value: 0, unit: 'px', },
  translateY: { value: 0, unit: 'px', },
  translate: {
    x: { value: 0, unit: 'px'},
    y: { value: 0, unit: 'px'},
  },
  scale: 1,
  scaleX: 1,
  scaleY: 1,
  skewX: { value: 0, unit: 'deg'},
  skewY: { value: 0, unit: 'deg'},
}

export const initialValuesTransform3D = {
  matrix3d: {
    a1: 0, b1: 0, c1: 0, d1: 0,
    a2: 0, b2: 0, c2: 0, d2: 0,
    a3: 0, b3: 0, c3: 0, d3: 0,
    a4: 0, b4: 0, c4: 0, d4: 0
  },
  perspective: { value: 0, unit: 'px', },
  rotate3d: {
    x: {value: 0, unit: 'deg' },
    y: {value: 0, unit: 'deg' },
    z: {value: 0, unit: 'deg' },
    a: {value: 0, unit: 'deg' },
  },
  rotateX: { value: 0, unit: 'deg', },
  rotateY: { value: 0, unit: 'deg', },
  rotateZ: { value: 0, unit: 'deg', },
  translate3d: {
    x: { value: 0, unit: 'px' },
    y: { value: 0, unit: 'px' },
    z: { value: 0, unit: 'px' },
  },
  scale3d: { x: 1, y: 1, z: 1 },
  scaleX: 1,
  scaleY: 1,
  scaleZ: 1,
};

export interface Matrix {
  a: number,
  b: number,
  c: number,
  d: number,
  tx: number,
  ty: number,
}

export interface Matrix3d {
  a1: number, b1: number, c1: number, d1: number,
  a2: number, b2: number, c2: number, d2: number,
  a3: number, b3: number, c3: number, d3: number,
  a4: number, b4: number, c4: number, d4: number,
}

export interface Rotate3d {
  x: number,
  y: number,
  z: number,
  a: {value: number, unit: string },
}

export interface Scale3d {
  x: number, y: number, z: number
}

export interface Translate3d {
  x: { value: number, unit: string },
  y: { value: number, unit: string },
  z: { value: number, unit: string },
}

type Transform2DValueMappings = {
  matrix: Matrix,
  rotate: { value: number, unit: string, },
  rotateX: { value: number, unit: string, },
  rotateY: { value: number, unit: string, },
  translate: {
    x: { value: number, unit: string},
    y: { value: number, unit: string},
  },
  translateX: { value: number, unit: string },
  translateY: { value: number, unit: string },
  scale: number,
  scaleX: number,
  scaleY: number,
  skewX: { value: number, unit: string },
  skewY: { value: number, unit: string },
}

type Transform3DValueMappings = {
  matrix3d: Matrix3d,
  perspective: UnitPicker,
  rotate3d: Rotate3d,
  rotateX: { value: number, unit: string, },
  rotateY: { value: number, unit: string, },
  rotateZ: { value: number, unit: string, },
  translate3d: Translate3d,
  translateX: { value: number, unit: string },
  translateY: { value: number, unit: string },
  translateZ: { value: number, unit: string },
  scale3d: Scale3d,
  scaleX: number,
  scaleY: number,
  scaleZ: number,
}
export type TransformBox = typeof transformBoxes[number];
export type TransformStyle = typeof transformStyles[number];
export type TransformFunction2D = typeof transforms2D[number];
export type TransformFunction3D = typeof transforms3D[number];

export type Transform2DValues = Partial<Record<TransformFunction2D, Transform2DValueMappings[TransformFunction2D]>>;
export type Transform3DValues = Partial<Record<TransformFunction3D, Transform3DValueMappings[TransformFunction3D]>>;

export type TransformOptions = {
  box: TransformBox,
  origin: {
    x: {value: number, unit: string},
    y: {value: number, unit: string},
  },
  style: TransformStyle,
}

export type TransformedItem = typeof transformedItems[number];
export type ActiveTab = 'item' | 'transforms';

export interface State {
  transformedItem: TransformedItem,
  transformOptions: TransformOptions,
  transformFunctions2D: Transform2DValues | null,
  transformFunctions3D: Transform3DValues | null,
  activeTab: ActiveTab,
  showCodeContainer: boolean,
  reset: boolean,
}

const initialState: State = {
  transformedItem: {
    name: 'rectangle',
    type: '2d',
  },
  transformOptions: {
    box: 'view-box',
    origin: {
      x: {
        value: 50, unit: '%',
      },
      y: {
        value: 50, unit: '%',
      },
    },
    style: 'flat',
  },
  transformFunctions2D: {
    matrix: { a: 1, b: 0, c: 0, d: 1, tx: 0, ty: 0, },
    // rotate: { value: 0, unit: 'deg', },
    // rotateX: { value: 0, unit: 'deg', },
    // rotateY: { value: 0, unit: 'deg', },
    // translateX: { value: 0, unit: 'px', },
    // translateY: { value: 0, unit: 'px', },
    // translate: {
    //   x: { value: 0, unit: 'px'},
    //   y: { value: 0, unit: 'px'},
    // },
    // scale: 1,
    // scaleX: 1,
    // scaleY: 1,
    // skewX: { value: 0, unit: 'deg'},
    // skewY: { value: 0, unit: 'deg'},
  },
  transformFunctions3D: {
    matrix3d: {
      a1: 1, b1: 0, c1: 0, d1: 0,
      a2: 0, b2: 1, c2: 0, d2: 0,
      a3: 0, b3: 0, c3: 1, d3: 0,
      a4: 0, b4: 0, c4: 0, d4: 1
    },
    // perspective: { value: 0, unit: 'px', },
    // rotate3d: {
    //   x: 1,
    //   y: 1,
    //   z: 1,
    //   a: {value: 0, unit: 'deg' },
    // },
    // rotateX: { value: 0, unit: 'deg', },
    // rotateY: { value: 0, unit: 'deg', },
    // rotateZ: { value: 0, unit: 'deg', },
    // translate3d: {
    //   x: { value: 0, unit: 'px' },
    //   y: { value: 0, unit: 'px' },
    //   z: { value: 0, unit: 'px' },
    // },
    // scale3d: { x: 1, y: 1, z: 1 },
    // scaleX: 1,
    // scaleY: 1,
    // scaleZ: 1,
  },
  activeTab: 'item',
  showCodeContainer: false,
  reset: false,
}

export const transformsReducer = createReducer(
  initialState,
  on(TransformsActions.setTransformedItem, (state, action) => {
    return {
      ...state,
      transformedItem: transformedItems.find((item) => item.name == action.value) as TransformedItem,
      transformOptions: {
        ...state.transformOptions,
        style: action.value == 'cube' ? 'preserve-3d' as const : 'flat' as const,
      }
    };
  }),
  on(TransformsActions.setTransformFunctions2D, (state, action) => {
    return {
      ...state,
      transformFunctions2D: {
        ...(action.value?.matrix && { matrix: action.value.matrix }),
        ...(action.value?.rotate && { rotate: action.value.rotate }),
        ...(action.value?.rotateX && { rotateX: action.value.rotateX }),
        ...(action.value?.rotateY && { rotateY: action.value.rotateY }),
        ...(action.value?.translateX && { translateX: action.value.translateX }),
        ...(action.value?.translateY && { translateY: action.value.translateY }),
        ...(action.value?.translate && { translate: action.value.translate }),
        ...(action.value?.scale && { scale: action.value.scale }),
        ...(action.value?.scaleX && { scaleX: action.value.scaleX }),
        ...(action.value?.scaleY && { scaleY: action.value.scaleY }),
        ...(action.value?.skewX && { skewX: action.value.skewX }),
        ...(action.value?.skewY && { skewY: action.value.skewY }),
        }
      }
  }),
  on(TransformsActions.setTransformFunctions3D, (state, action) => {
    return {
      ...state,
      transformFunctions3D: {
        ...(action.value?.matrix3d && { matrix3d: action.value.matrix3d }),
        ...(action.value?.perspective && { perspective: action.value.perspective }),
        ...(action.value?.rotate3d && { rotate3d: action.value.rotate3d }),
        ...(action.value?.rotateX && { rotateX: action.value.rotateX }),
        ...(action.value?.rotateY && { rotateY: action.value.rotateY }),
        ...(action.value?.rotateZ && { rotateZ: action.value.rotateZ }),
        ...(action.value?.translate3d && { translate3d: action.value.translate3d }),
        ...(action.value?.translateX && { translateX: action.value.translateX }),
        ...(action.value?.translateY && { translateY: action.value.translateY }),
        ...(action.value?.translateZ && { translateZ: action.value.translateZ }),
        ...(action.value?.scale3d && { scale3d: action.value.scale3d }),
        ...(action.value?.scaleX && { scaleX: action.value.scaleX }),
        ...(action.value?.scaleY && { scaleY: action.value.scaleY }),
        ...(action.value?.scaleZ && { scaleZ: action.value.scaleZ }),
      }
    }
  }),
  on(TransformsActions.setTransformOptions, (state, action) => {
      return {
        ...state,
        transformOptions: {
          ...state.transformOptions,
          box: action.value.box,
          origin: {
            ...state.transformOptions.origin,
            x: {
              ...state.transformOptions.origin.x,
              value: action.value.origin.x.value,
              unit: action.value.origin.x.unit,
            },
            y: {
              ...state.transformOptions.origin.y,
              value: action.value.origin.y.value,
              unit: action.value.origin.y.unit,
            },
          },
          style: action.value.style,
        }
      };
  }),
  on(TransformsActions.setActiveTab, (state, action) => {
    return {
      ...state,
      activeTab: action.value,
    };
  }),
  on(TransformsActions.toggleCodeContainer, (state) => {
    return {
      ...state,
      showCodeContainer: !state.showCodeContainer,
    };
  }),
  on(TransformsActions.resetDefaults, () => initialState),
);
