import {createAction, props} from "@ngrx/store";
import {
  ActiveTab, Transform2DValues, Transform3DValues,
  TransformOptions
} from "./transforms.reducer";

export const setTransformedItem = createAction(
  '[Transforms] Set transformed item',
  props<{value: string}>()
);

export const getTransformedItem = createAction(
  '[Transforms] Get transformed item',
);

export const setTransformOptions = createAction(
  '[Transforms] Set transform options',
  props<{value: TransformOptions}>()
);

export const getTransformOptions = createAction(
  '[Transforms] Get transform options',
);

export const setTransformFunctions2D = createAction(
  '[Transforms] Set transform functions 2d',
  props<{value: Transform2DValues | null}>()
);

export const setTransformFunctions3D = createAction(
  '[Transforms] Set transform functions 3d',
  props<{value: Transform3DValues | null}>()
);

export const getTransformFunctions = createAction(
  '[Transforms] Get transform functions',
);

export const setActiveTab = createAction(
  '[Transforms] Set active tab',
  props<{value: ActiveTab}>()
)

export const getActiveTab = createAction(
  '[Transforms] Get active tab',
);

export const resetDefaults = createAction(
  '[Transforms] Reset defaults'
);

export const toggleCodeContainer = createAction(
  '[Transforms] Toggle Code Container',
);
