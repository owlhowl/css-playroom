import {createFeatureSelector, createSelector} from "@ngrx/store";
import * as fromTransforms from "../../transforms/store/transforms.reducer";

export const selectTransformsState = createFeatureSelector<fromTransforms.State>(
  fromTransforms.transformsFeatureKey
);

export const selectTransformedItem = createSelector(
  selectTransformsState,
  (state: fromTransforms.State) => state.transformedItem
);

export const selectTransformOptions = createSelector(
  selectTransformsState,
  (state: fromTransforms.State) => state.transformOptions
);

export const selectTransformFunctions2D = createSelector(
  selectTransformsState,
  (state: fromTransforms.State) => state.transformFunctions2D
);

export const selectTransformFunctions3D = createSelector(
  selectTransformsState,
  (state: fromTransforms.State) => state.transformFunctions3D
);

export const selectActiveTab = createSelector(
  selectTransformsState,
  (state: fromTransforms.State) => state.activeTab,
);

export const selectShowCodeContainer = createSelector(
  selectTransformsState,
  (state: fromTransforms.State) => state.showCodeContainer,
);
