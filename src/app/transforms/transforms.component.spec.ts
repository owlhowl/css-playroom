import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsComponent } from './transforms.component';

describe('TransformsComponent', () => {
  let component: TransformsComponent;
  let fixture: ComponentFixture<TransformsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsComponent]
    });
    fixture = TestBed.createComponent(TransformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
