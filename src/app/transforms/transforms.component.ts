import {Component, inject, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {CodeContainerComponent} from "../shared/ui/code-container/code-container.component";
import {TransformsSidebarComponent} from "./transforms-sidebar/transforms-sidebar.component";
import {TransformsContainerComponent} from "./transforms-container/transforms-container.component";
import {Store} from "@ngrx/store";
import {CodeService} from "../shared/services/code.service";
import {Observable} from "rxjs";
import * as TransformsSelectors from "../transforms/store/transforms.selectors";
import * as TransformsActions from "../transforms/store/transforms.actions";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-transforms',
  standalone: true,
  imports: [CommonModule, CodeContainerComponent, TransformsSidebarComponent, TransformsContainerComponent],
  templateUrl: './transforms.component.html',
  styleUrls: ['./transforms.component.scss']
})
export class TransformsComponent implements OnInit, OnDestroy {
  private store = inject(Store);
  private codeService = inject(CodeService);
  private titleService = inject(Title);
  private metaTagService = inject(Meta);

  activeTab$!: Observable<string>;
  showCodeContainer$!: Observable<boolean>;
  transformsHtml$ = this.codeService.transformsHtml$;
  transformsCss$ = this.codeService.transformsCss$;

  ngOnInit() {
    this.activeTab$ = this.store.select(TransformsSelectors.selectActiveTab);
    this.showCodeContainer$ = this.store.select(TransformsSelectors.selectShowCodeContainer);
  }

  ngOnDestroy() {
    this.titleService.setTitle('CSS Playroom - Transforms');
    this.metaTagService.addTags([
      {name: 'description', content: 'cssplayroom.com is your first and best source for the css information you’re looking for. Select an item, a transformation function and visualize how it behaves'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom,  Transforms, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
    this.store.dispatch(TransformsActions.resetDefaults());
  }
}
