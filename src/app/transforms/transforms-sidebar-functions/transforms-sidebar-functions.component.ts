import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {
  Transform2DValues,
  Transform3DValues,
  TransformedItem,
  TransformFunction3D,
  TransformOptions
} from "../store/transforms.reducer";
import * as TransformsSelectors from "../store/transforms.selectors";
import {
  TransformsSidebarItemFormComponent
} from "../transforms-sidebar-item-form/transforms-sidebar-item-form.component";
import {
    TransformsSidebarFunctionsFormTwodComponent
} from "../transforms-sidebar-functions-form-twod/transforms-sidebar-functions-form-twod.component";
import {
  TransformsSidebarFunctionsFormThreedComponent
} from "../transforms-sidebar-functions-form-threed/transforms-sidebar-functions-form-threed.component";

@Component({
  selector: 'app-transforms-sidebar-functions',
  standalone: true,
  imports: [CommonModule, TransformsSidebarItemFormComponent, TransformsSidebarFunctionsFormTwodComponent, TransformsSidebarFunctionsFormThreedComponent],
  templateUrl: './transforms-sidebar-functions.component.html',
  styleUrls: ['./transforms-sidebar-functions.component.scss']
})
export class TransformsSidebarFunctionsComponent {
  private store = inject(Store);

  transformFunctions2D$!: Observable<Transform2DValues | null>;
  transformFunctions3D$!: Observable<Transform3DValues | null>;
  transformedItem$!: Observable<TransformedItem>;

  ngOnInit() {
    this.transformFunctions2D$ = this.store.pipe(select(TransformsSelectors.selectTransformFunctions2D));
    this.transformFunctions3D$ = this.store.pipe(select(TransformsSelectors.selectTransformFunctions3D));
    this.transformedItem$ = this.store.pipe(select(TransformsSelectors.selectTransformedItem));
  }
}
