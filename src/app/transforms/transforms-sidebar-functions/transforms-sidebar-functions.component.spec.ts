import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsSidebarFunctionsComponent } from './transforms-sidebar-functions.component';

describe('TransformsSidebarFunctionsComponent', () => {
  let component: TransformsSidebarFunctionsComponent;
  let fixture: ComponentFixture<TransformsSidebarFunctionsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsSidebarFunctionsComponent]
    });
    fixture = TestBed.createComponent(TransformsSidebarFunctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
