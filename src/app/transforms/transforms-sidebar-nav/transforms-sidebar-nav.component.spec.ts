import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsSidebarNavComponent } from './transforms-sidebar-nav.component';

describe('TransformsSidebarNavComponent', () => {
  let component: TransformsSidebarNavComponent;
  let fixture: ComponentFixture<TransformsSidebarNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsSidebarNavComponent]
    });
    fixture = TestBed.createComponent(TransformsSidebarNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
