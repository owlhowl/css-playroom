import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import {ActiveTab} from "../store/transforms.reducer";
import {Observable} from "rxjs";
import * as TransformsSelectors from "../store/transforms.selectors";
import * as TransformsActions from "../store/transforms.actions";
import {TransformsSidebarNavTabComponent} from "../transforms-sidebar-nav-tab/transforms-sidebar-nav-tab.component";
import {ToggleViewService} from "../services/toggle-view.service";

@Component({
  selector: 'app-transforms-sidebar-nav',
  standalone: true,
  imports: [CommonModule, TransformsSidebarNavTabComponent],
  templateUrl: './transforms-sidebar-nav.component.html',
  styleUrls: ['./transforms-sidebar-nav.component.scss']
})
export class TransformsSidebarNavComponent implements OnInit {
  private store = inject(Store);
  private toggleViewService = inject(ToggleViewService);

  viewOriginal$ = this.toggleViewService.viewOriginal$;
  tabs: {
    name: string,
    key: ActiveTab
  }[] = [
    { name: 'Item', key: 'item' },
    { name: 'Transforms', key: 'transforms' },
  ];
  activeTab$!: Observable<string>;
  showCodeContainer$!: Observable<boolean>;

  ngOnInit() {
    this.activeTab$ = this.store.pipe(select(TransformsSelectors.selectActiveTab));
    this.showCodeContainer$ = this.store.pipe(select(TransformsSelectors.selectShowCodeContainer));
  }

  setActiveTab(key: ActiveTab) {
    this.store.dispatch(TransformsActions.setActiveTab({value: key}));
  }

  resetDefaults() {
    this.store.dispatch(TransformsActions.resetDefaults());
  }

  toggleCodeContainer() {
    this.store.dispatch(TransformsActions.toggleCodeContainer());
  }

  toggleOriginal() {
    this.toggleViewService.viewOriginal$.next(!this.toggleViewService.viewOriginal$.value);
  }
}
