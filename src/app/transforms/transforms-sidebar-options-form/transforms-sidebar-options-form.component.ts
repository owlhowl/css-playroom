import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TransformOptions} from "../store/transforms.reducer";

@Component({
  selector: 'app-transforms-sidebar-options-form',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './transforms-sidebar-options-form.component.html',
  styleUrls: ['./transforms-sidebar-options-form.component.scss']
})
export class TransformsSidebarOptionsFormComponent {
  @Input() transformOptionsState!: TransformOptions;
}
