import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsSidebarOptionsFormComponent } from './transforms-sidebar-options-form.component';

describe('TransformsSidebarOptionsFormComponent', () => {
  let component: TransformsSidebarOptionsFormComponent;
  let fixture: ComponentFixture<TransformsSidebarOptionsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsSidebarOptionsFormComponent]
    });
    fixture = TestBed.createComponent(TransformsSidebarOptionsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
