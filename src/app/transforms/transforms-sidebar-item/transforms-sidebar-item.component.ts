import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Observable} from "rxjs";
import {TransformedItem, TransformOptions} from "../store/transforms.reducer";
import {select, Store} from "@ngrx/store";
import * as TransformsSelectors from "../store/transforms.selectors";
import {
  TransformsSidebarItemFormComponent
} from "../transforms-sidebar-item-form/transforms-sidebar-item-form.component";
import {
  FlexboxSidebarContainerSelectComponent
} from "../../flexbox/flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {FormControlPipe} from "../../shared/pipes/form-control.pipe";
import {ReactiveFormsModule} from "@angular/forms";

@Component({
  selector: 'app-transforms-sidebar-item',
  standalone: true,
  imports: [CommonModule, TransformsSidebarItemFormComponent, FlexboxSidebarContainerSelectComponent, FormControlPipe, ReactiveFormsModule],
  templateUrl: './transforms-sidebar-item.component.html',
  styleUrls: ['./transforms-sidebar-item.component.scss']
})
export class TransformsSidebarItemComponent implements OnInit {
  private store = inject(Store);

  transformOptionsState$!: Observable<TransformOptions>;
  transformedItemState$!: Observable<TransformedItem>;

  ngOnInit() {
    this.transformedItemState$ = this.store.pipe(select(TransformsSelectors.selectTransformedItem));
    this.transformOptionsState$ = this.store.pipe(select(TransformsSelectors.selectTransformOptions));
  }
}
