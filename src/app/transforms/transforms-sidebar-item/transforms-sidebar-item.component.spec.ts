import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsSidebarItemComponent } from './transforms-sidebar-item.component';

describe('TransformsSidebarItemComponent', () => {
  let component: TransformsSidebarItemComponent;
  let fixture: ComponentFixture<TransformsSidebarItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsSidebarItemComponent]
    });
    fixture = TestBed.createComponent(TransformsSidebarItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
