import {Component, inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Transform2DValues, Transform3DValues, TransformedItem, transforms3D} from "../store/transforms.reducer";
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Store} from "@ngrx/store";
import {debounceTime, Subject, takeUntil, tap} from "rxjs";
import * as TransformActions from "../store/transforms.actions";
import {DegreeUnitPickerComponent} from "../../shared/ui/degree-unit-picker/degree-unit-picker.component";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";
import {setTransformFunctions3D} from "../store/transforms.actions";

@Component({
  selector: 'app-transforms-sidebar-functions-form-threed',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DegreeUnitPickerComponent, UnitPickerComponent],
  templateUrl: './transforms-sidebar-functions-form-threed.component.html',
  styleUrls: ['./transforms-sidebar-functions-form-threed.component.scss']
})
export class TransformsSidebarFunctionsFormThreedComponent implements OnChanges, OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() transformFunctions3D!: Transform3DValues | null;
  @Input() transformedItem!: TransformedItem | null;
  transformFunctions3DForm= this.fb.group({});
  transforms3D = [...transforms3D];
  destroy$: Subject<void> = new Subject();

  ngOnChanges(changes: SimpleChanges) {
    // if(
    //   changes['transformFunctions2D']
    //   && !changes['transformFunctions2D'].firstChange
    //   && Object.keys(changes['transformFunctions2D'].currentValue).length == 0
    // ) {
    //   this.build3dForm(false, true);
    // }
  }

  ngOnInit() {
    this.build3dForm();

    this.transformFunctions3DForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(500),
      tap(() => {
        const extractedValues: Transform3DValues = {};
        const formValue = this.transformFunctions3DForm.value as Partial<any>;
        for (const key of this.transforms3D) {
          let checkboxKey = `${key}Checkbox`;
          if (formValue.hasOwnProperty(checkboxKey) && formValue[checkboxKey] === true) {
            extractedValues[key] = formValue[key];
          }
        }
        this.store.dispatch(TransformActions.setTransformFunctions3D({value: extractedValues}));
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getForm3dControl(name: string) {
    return this.transformFunctions3DForm.get(name) as FormControl;
  }

  getForm3dGroup(name: string) {
    return this.transformFunctions3DForm.get(name) as FormGroup;
  }

  private build3dForm(emitEvent = true, reset = false) {
    if (reset) {
      this.transformFunctions3DForm.reset({}, {emitEvent: false});
    }

    for (let func of this.transforms3D) {
      this.transformFunctions3DForm.addControl(
        `${func}Checkbox`,
        this.fb.control(this.transformFunctions3D && func in this.transformFunctions3D),
        {emitEvent: emitEvent}
      );

      switch (func) {
        case 'matrix3d':
          let matrixVal;
          if (this.transformFunctions3D && func in this.transformFunctions3D) {
            matrixVal = this.transformFunctions3D[func] as {
              a1: number, b1: number, c1: number, d1: number,
              a2: number, b2: number, c2: number, d2: number,
              a3: number, b3: number, c3: number, d3: number,
              a4: number, b4: number, c4: number, d4: number,
            };
          }
          this.transformFunctions3DForm.addControl(func, this.fb.group({
            a1: this.fb.control<number | null>(matrixVal?.a1 ?? null),
            b1: this.fb.control<number | null>(matrixVal?.b1 ?? null),
            c1: this.fb.control<number | null>(matrixVal?.c1 ?? null),
            d1: this.fb.control<number | null>(matrixVal?.d1 ?? null),
            a2: this.fb.control<number | null>(matrixVal?.a2 ?? null),
            b2: this.fb.control<number | null>(matrixVal?.b2 ?? null),
            c2: this.fb.control<number | null>(matrixVal?.c2 ?? null),
            d2: this.fb.control<number | null>(matrixVal?.d2 ?? null),
            a3: this.fb.control<number | null>(matrixVal?.a3 ?? null),
            b3: this.fb.control<number | null>(matrixVal?.b3 ?? null),
            c3: this.fb.control<number | null>(matrixVal?.c3 ?? null),
            d3: this.fb.control<number | null>(matrixVal?.d3 ?? null),
            a4: this.fb.control<number | null>(matrixVal?.a4 ?? null),
            b4: this.fb.control<number | null>(matrixVal?.b4 ?? null),
            c4: this.fb.control<number | null>(matrixVal?.c4 ?? null),
            d4: this.fb.control<number | null>(matrixVal?.d4 ?? null),
          }), { emitEvent: emitEvent });
          break;
        case 'perspective':
        case 'rotateX':
        case 'rotateY':
        case 'rotateZ':
        case 'translateX':
        case 'translateY':
        case 'translateZ':
          let valUnit;
          if (this.transformFunctions3D && func in this.transformFunctions3D) {
            // @ts-ignore
            valUnit = this.transformFunctions3D[func] as { value: number, unit: string };
          }
          this.transformFunctions3DForm.addControl(func, this.fb.group({
            value: this.fb.control<number | null>(valUnit?.value ?? null),
            unit: this.fb.control<string | null>(valUnit?.unit ?? null),
          }), {emitEvent: emitEvent});
          break;
        case 'scaleX':
        case 'scaleY':
        case 'scaleZ':
          let scaleAxis;
          if (this.transformFunctions3D && func in this.transformFunctions3D) {
            // @ts-ignore
            scaleAxis = this.transformFunctions3D[func] as number;
          }
          this.transformFunctions3DForm.addControl(
            func, this.fb.control<number | null>(scaleAxis ?? null),
            {emitEvent: emitEvent}
          );
          break;
        case 'rotate3d':
          let rotate3d;
          if (this.transformFunctions3D && func in this.transformFunctions3D) {
            // @ts-ignore
            rotate3d = this.transformFunctions3D[func] as {
              x: { value: number, unit: string },
              y: { value: number, unit: string },
              z: { value: number, unit: string },
              a: { value: number, unit: string },
            };
          }
          this.transformFunctions3DForm.addControl(func, this.fb.group({
            x: this.fb.control<number | null>(rotate3d?.x.value ?? null),
            y: this.fb.control<number | null>(rotate3d?.y.value ?? null),
            z: this.fb.control<number | null>(rotate3d?.z.value ?? null),
            a: this.fb.group({
              value: this.fb.control<number | null>(rotate3d?.a.value ?? null),
              unit: this.fb.control<string | null>(rotate3d?.a.unit ?? null),
            }),
          }), {emitEvent: emitEvent});
          break;
        case 'translate3d':
          let translate3d;
          if (this.transformFunctions3D && func in this.transformFunctions3D) {
            // @ts-ignore
            translate3d = this.transformFunctions3D[func] as {
              x: { value: number, unit: string },
              y: { value: number, unit: string },
              z: { value: number, unit: string },
            };
          }
          this.transformFunctions3DForm.addControl(func, this.fb.group({
            x: this.fb.group({
              value: this.fb.control<number | null>(translate3d?.x.value ?? null),
              unit: this.fb.control<string | null>(translate3d?.x.unit ?? null),
            }),
            y: this.fb.group({
              value: this.fb.control<number | null>(translate3d?.y.value ?? null),
              unit: this.fb.control<string | null>(translate3d?.y.unit ?? null),
            }),
            z: this.fb.group({
              value: this.fb.control<number | null>(translate3d?.z.value ?? null),
              unit: this.fb.control<string | null>(translate3d?.z.unit ?? null),
            }),
          }), {emitEvent: emitEvent});
          break;
        case 'scale3d':
          let scale3d;
          if (this.transformFunctions3D && func in this.transformFunctions3D) {
            // @ts-ignore
            scale3d = this.transformFunctions3D[func] as { x: number, y: number, z: number };
          }
          this.transformFunctions3DForm.addControl(func, this.fb.group({
            x: this.fb.control<number | null>(scale3d?.x ?? null),
            y: this.fb.control<number | null>(scale3d?.y ?? null),
            z: this.fb.control<number | null>(scale3d?.z ?? null),
          }), {emitEvent: emitEvent});
          break;
      }
    }
  }
}
