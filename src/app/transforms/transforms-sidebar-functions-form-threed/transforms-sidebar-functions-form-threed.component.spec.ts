import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsSidebarFunctionsFormThreedComponent } from './transforms-sidebar-functions-form-threed.component';

describe('TransformsSidebarFunctionsFormThreedComponent', () => {
  let component: TransformsSidebarFunctionsFormThreedComponent;
  let fixture: ComponentFixture<TransformsSidebarFunctionsFormThreedComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsSidebarFunctionsFormThreedComponent]
    });
    fixture = TestBed.createComponent(TransformsSidebarFunctionsFormThreedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
