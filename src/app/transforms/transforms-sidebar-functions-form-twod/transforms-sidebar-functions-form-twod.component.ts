import {Component, inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Store} from "@ngrx/store";
import {Transform2DValues, Transform3DValues, TransformedItem, transforms2D} from "../store/transforms.reducer";
import {debounceTime, Subject, takeUntil, tap} from "rxjs";
import * as TransformActions from "../store/transforms.actions";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";
import {DegreeUnitPickerComponent} from "../../shared/ui/degree-unit-picker/degree-unit-picker.component";

@Component({
  selector: 'app-transforms-sidebar-functions-form-twod',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, UnitPickerComponent, DegreeUnitPickerComponent],
  templateUrl: './transforms-sidebar-functions-form-twod.component.html',
  styleUrls: ['./transforms-sidebar-functions-form-twod.component.scss']
})
export class TransformsSidebarFunctionsFormTwodComponent implements OnChanges, OnInit, OnDestroy {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() transformFunctions2D!: Transform2DValues | Transform3DValues | null;
  @Input() transformedItem!: TransformedItem | null;
  transformFunctions2DForm= this.fb.group({});
  transforms2D = [...transforms2D];
  destroy$: Subject<void> = new Subject();

  ngOnChanges(changes: SimpleChanges) {
    // this is not really ok but too late to change
    // if(
    //   changes['transformFunctions2D']
    //   && !changes['transformFunctions2D'].firstChange
    //   && Object.keys(changes['transformFunctions2D'].currentValue).length == 0
    // ) {
    //   this.build2dForm(false, true);
    // }
  }

  ngOnInit() {
    this.build2dForm();

    this.transformFunctions2DForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(500),
      tap(() => {
        const extractedValues: Transform2DValues = {};
        const formValue = this.transformFunctions2DForm.value as Partial<any>;
        for (const key of this.transforms2D) {
          let checkboxKey = `${key}Checkbox`;
          if (formValue.hasOwnProperty(checkboxKey) && formValue[checkboxKey] === true) {
            extractedValues[key] = formValue[key];
          }
        }
        this.store.dispatch(TransformActions.setTransformFunctions2D({value: extractedValues}));
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getForm2dControl(name: string) {
    return this.transformFunctions2DForm.get(name) as FormControl;
  }

  getForm2dGroup(name: string) {
    return this.transformFunctions2DForm.get(name) as FormGroup;
  }

  private build2dForm(emitEvent = true, reset = false) {
    if (reset) {
      this.transformFunctions2DForm.reset({}, {emitEvent: false});
    }

    for (let func of this.transforms2D) {
      this.transformFunctions2DForm.addControl(
        `${func}Checkbox`,
        this.fb.control(this.transformFunctions2D && func in this.transformFunctions2D),
        { emitEvent: emitEvent }
      );

      switch (func) {
        case 'matrix':
          let matrixVal;
          if (this.transformFunctions2D && func in this.transformFunctions2D) {
            matrixVal = this.transformFunctions2D[func] as { a: 0, b: 0, c: 0, d: 0, tx: 0, ty: 0, };
          }
          this.transformFunctions2DForm.addControl(func, this.fb.group({
            a: this.fb.control<number | null>(matrixVal?.a??null),
            b: this.fb.control<number | null>(matrixVal?.b??null),
            c: this.fb.control<number | null>(matrixVal?.c??null),
            d: this.fb.control<number | null>(matrixVal?.d??null),
            tx: this.fb.control<number | null>(matrixVal?.tx??null),
            ty: this.fb.control<number | null>(matrixVal?.ty??null),
          }),{ emitEvent: emitEvent });
          break;
        case 'translate':
          let translate;
          if (this.transformFunctions2D && func in this.transformFunctions2D) {
            translate = this.transformFunctions2D[func] as {
              x: { value: number, unit: string},
              y: { value: number, unit: string},
            };
          }
          this.transformFunctions2DForm.addControl(func, this.fb.group({
            x: this.fb.group({
              value: this.fb.control<number | null>(translate?.x.value??null),
              unit: this.fb.control<string | null>(translate?.x.unit??null),
            }),
            y: this.fb.group({
              value: this.fb.control<number | null>(translate?.y.value??null),
              unit: this.fb.control<string | null>(translate?.y.unit??null),
            }),
          }), { emitEvent: emitEvent });
          break;
        case 'scale':
        case 'scaleX':
        case 'scaleY':
          let scaleAxis;
          if (this.transformFunctions2D && func in this.transformFunctions2D) {
            // @ts-ignore
            scaleAxis = this.transformFunctions2D[func] as number;
          }
          this.transformFunctions2DForm.addControl(
            func,
            this.fb.control<number | null>(scaleAxis??null),
            { emitEvent: emitEvent }
          );
          break;
        case 'translateX':
        case 'translateY':
        case 'rotate':
        case 'rotateX':
        case 'rotateY':
        case 'skewX':
        case 'skewY':
          let unitVal;
          if (this.transformFunctions2D && func in this.transformFunctions2D) {
            // @ts-ignore
            unitVal = this.transformFunctions2D[func] as { value: number, unit: string };
          }
          this.transformFunctions2DForm.addControl(func, this.fb.group({
            value: this.fb.control<number | null>(unitVal?.value??null),
            unit: this.fb.control<string | null>(unitVal?.unit??null),
          }),{ emitEvent: emitEvent });
          break;
      }
    }
  }
}
