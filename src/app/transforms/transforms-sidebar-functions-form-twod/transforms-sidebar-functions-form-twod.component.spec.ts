import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransformsSidebarFunctionsFormTwodComponent } from './transforms-sidebar-functions-form-twod.component';

describe('TransformsSidebarFunctionsFormTwodComponent', () => {
  let component: TransformsSidebarFunctionsFormTwodComponent;
  let fixture: ComponentFixture<TransformsSidebarFunctionsFormTwodComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TransformsSidebarFunctionsFormTwodComponent]
    });
    fixture = TestBed.createComponent(TransformsSidebarFunctionsFormTwodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
