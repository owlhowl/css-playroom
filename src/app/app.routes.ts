import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./home/home.component').then(c => c.HomeComponent),
  },
  {
    path: '',
    loadComponent: () => import('./shared/ui/playground-layout/playground-layout.component').then(c => c.PlaygroundLayoutComponent),
    children: [
      {
        path: 'flexbox',
        loadComponent: () => import('./flexbox/flexbox.component').then(c => c.FlexboxComponent),
      },
      {
        path: 'grid',
        loadComponent: () => import('./grid/grid.component').then(c => c.GridComponent),
      },
      {
        path: 'animations',
        loadComponent: () => import('./animations/animations.component').then(
          c => c.AnimationsComponent
        ),
      },
      {
        path: 'transitions',
        loadComponent: () => import('./transitions/transitions.component').then(
          c => c.TransitionsComponent
        ),
      },
      {
        path: 'pseudo-classes',
        loadComponent: () => import('./pseudo-classes/pseudo-classes.component').then(
          c => c.PseudoClassesComponent
        ),
      },
      {
        path: 'pseudo-elements',
        loadComponent: () => import('./pseudo-elements/pseudo-elements.component').then(
          c => c.PseudoElementsComponent
        ),
      },
      {
        path: 'transforms',
        loadComponent: () => import('./transforms/transforms.component').then(c => c.TransformsComponent),
      },
      {
        path: 'positioning',
        loadComponent: () => import('./positioning/positioning.component').then(c => c.PositioningComponent),
      },
      {
        path: 'box-model',
        loadComponent: () => import('./box-model/box-model.component').then(c => c.BoxModelComponent),
      },
      {
        path: 'scroll',
        loadComponent: () => import('./scroll/scroll.component').then(c => c.ScrollComponent),
      },
      {
        path: 'filters',
        loadComponent: () => import('./filters/filters.component').then(c => c.FiltersComponent),
      },
    ]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];
