import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComingSoonComponent} from "../shared/ui/coming-soon/coming-soon.component";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-pseudo-elements',
  standalone: true,
    imports: [CommonModule, ComingSoonComponent],
  templateUrl: './pseudo-elements.component.html',
  styleUrls: ['./pseudo-elements.component.scss']
})
export class PseudoElementsComponent implements OnInit {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Pseudo Elements');
    this.metaTagService.addTags([
      // {name: 'description', content: 'Add div items, set css flexbox properties, visualize the output and get the css code'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Pseudo Elements, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
  }

}
