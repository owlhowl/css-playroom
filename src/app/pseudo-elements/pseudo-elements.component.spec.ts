import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PseudoElementsComponent } from './pseudo-elements.component';

describe('PseudoElementsComponent', () => {
  let component: PseudoElementsComponent;
  let fixture: ComponentFixture<PseudoElementsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PseudoElementsComponent]
    });
    fixture = TestBed.createComponent(PseudoElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
