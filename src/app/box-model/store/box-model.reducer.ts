import {createReducer, on} from "@ngrx/store";
import {UnitPicker} from "../../shared/models/unit-picker";
import {Border} from "../../shared/models/border";
import * as BoxModelActions from './box-model.actions';

export const boxSizing = ['content-box', 'border-box']
export type BoxSizing = typeof boxSizing[number];

export interface BoxModelState {
  boxSizing: BoxSizing,
  content: {
    width: UnitPicker,
    height: UnitPicker,
  },
  padding: {
    top: UnitPicker,
    right: UnitPicker,
    bottom: UnitPicker,
    left: UnitPicker,
  },
  border: {
    top: Border,
    right: Border,
    bottom: Border,
    left: Border,
  },
  margin: {
    top: UnitPicker,
    right: UnitPicker,
    bottom: UnitPicker,
    left: UnitPicker,
  }
}
export interface State {
  boxModel: BoxModelState,
  showCodeContainer: boolean,
}

const initialState: State = {
  boxModel: {
    boxSizing: 'content-box',
    content: {
      height: {
        value: 200,
        unit: 'px',
      },
      width: {
        value: 200,
        unit: 'px',
      }
    },
    padding: {
      bottom: {
        value: 10,
        unit: 'px',
      },
      left: {
        value: 10,
        unit: 'px',
      },
      right: {
        value: 10,
        unit: 'px',
      },
      top: {
        value: 10,
        unit: 'px',
      }
    },
    border: {
      bottom: {
        size: {
          value: 2,
          unit: 'px',
        },
        style: 'solid',
        color: '#4f46e5'
      },
      left: {
        size: {
          value: 2,
          unit: 'px',
        },
        style: 'solid',
        color: '#4f46e5'
      },
      right: {
        size: {
          value: 2,
          unit: 'px',
        },
        style: 'solid',
        color: '#4f46e5'
      },
      top: {
        size: {
          value: 2,
          unit: 'px',
        },
        style: 'solid',
        color: '#4f46e5'
      }
    },
    margin: {
      bottom: {
        value: 15,
        unit: 'px',
      },
      left: {
        value: 15,
        unit: 'px',
      },
      right: {
        value: 15,
        unit: 'px',
      },
      top: {
        value: 15,
        unit: 'px',
      }
    },
  },
  showCodeContainer: false
};

export const boxModelFeatureKey = 'boxModel';

export const boxModelReducer = createReducer(
  initialState,
  on(BoxModelActions.setBoxModel, (state, action) => {
    return {
      ...state,
      boxModel: {
        ...state.boxModel,
        boxSizing: action.value.boxSizing,
        content: {
          ...state.boxModel.content,
          height: {
            ...state.boxModel.content.height,
            value: action.value.content.height.value,
            unit: action.value.content.height.unit
          },
          width: {
            ...state.boxModel.content.width,
            value: action.value.content.width.value,
            unit: action.value.content.width.unit
          }
        },
        padding: {
          ...state.boxModel.padding,
          top: {
            ...state.boxModel.padding.top,
            value: action.value.padding.top.value,
            unit: action.value.padding.top.unit,
          },
          right: {
            ...state.boxModel.padding.right,
            value: action.value.padding.right.value,
            unit: action.value.padding.right.unit,
          },
          bottom: {
            ...state.boxModel.padding.bottom,
            value: action.value.padding.bottom.value,
            unit: action.value.padding.bottom.unit,
          },
          left: {
            ...state.boxModel.padding.left,
            value: action.value.padding.left.value,
            unit: action.value.padding.left.unit,
          }
        },
        border: {
          ...state.boxModel.border,
          top: {
            ...state.boxModel.border.top,
            size: {
              ...state.boxModel.border.top.size,
              value: action.value.border.top.size.value,
              unit: action.value.border.top.size.unit,
            },
            style: action.value.border.top.style,
            color: action.value.border.top.color,
          },
          right: {
            ...state.boxModel.border.right,
            size: {
              ...state.boxModel.border.right.size,
              value: action.value.border.right.size.value,
              unit: action.value.border.right.size.unit,
            },
            style: action.value.border.right.style,
            color: action.value.border.right.color,
          },
          bottom: {
            ...state.boxModel.border.bottom,
            size: {
              ...state.boxModel.border.bottom.size,
              value: action.value.border.bottom.size.value,
              unit: action.value.border.bottom.size.unit,
            },
            style: action.value.border.bottom.style,
            color: action.value.border.bottom.color,
          },
          left: {
            ...state.boxModel.border.left,
            size: {
              ...state.boxModel.border.left.size,
              value: action.value.border.left.size.value,
              unit: action.value.border.left.size.unit,
            },
            style: action.value.border.left.style,
            color: action.value.border.left.color,
          }
        },
        margin: {
          ...state.boxModel.margin,
          top: {
            ...state.boxModel.margin.top,
            value: action.value.margin.top.value,
            unit: action.value.margin.top.unit,
          },
          right: {
            ...state.boxModel.margin.right,
            value: action.value.margin.right.value,
            unit: action.value.margin.right.unit,
          },
          bottom: {
            ...state.boxModel.margin.bottom,
            value: action.value.margin.bottom.value,
            unit: action.value.margin.bottom.unit,
          },
          left: {
            ...state.boxModel.margin.left,
            value: action.value.margin.left.value,
            unit: action.value.margin.left.unit,
          }
        }
      }
    };
  }),
  on(BoxModelActions.toggleCodeContainer, (state) => {
    return {
      ...state,
      showCodeContainer: !state.showCodeContainer,
    }
  }),
  on(BoxModelActions.resetDefaults, () => initialState),
);
