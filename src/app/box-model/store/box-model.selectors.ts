import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromBoxModel from './box-model.reducer';

export const selectBoxModelState = createFeatureSelector<fromBoxModel.State>(
  fromBoxModel.boxModelFeatureKey
);

export const selectBoxModel = createSelector(
  selectBoxModelState,
  (state: fromBoxModel.State) => state.boxModel,
);

export const selectShowCodeContainer = createSelector(
  selectBoxModelState,
  (state: fromBoxModel.State) => state.showCodeContainer,
);
