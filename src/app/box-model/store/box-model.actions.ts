import {createAction, createSelector, props} from "@ngrx/store";
import {BoxModelState} from "./box-model.reducer";

export const setBoxModel = createAction(
  '[BoxModel] Set box model',
  props<{value: BoxModelState}>(),
);

export const resetDefaults = createAction(
  '[BoxModel] Reset defaults',
);

export const toggleCodeContainer = createAction(
  '[BoxModel] Toggle Code Container',
);
