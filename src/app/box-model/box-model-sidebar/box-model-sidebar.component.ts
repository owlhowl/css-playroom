import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BoxModelSidebarNavComponent} from "../box-model-sidebar-nav/box-model-sidebar-nav.component";
import {BoxModelSidebarPropsComponent} from "../box-model-sidebar-props/box-model-sidebar-props.component";

@Component({
  selector: 'app-box-model-sidebar',
  standalone: true,
  imports: [CommonModule, BoxModelSidebarNavComponent, BoxModelSidebarPropsComponent],
  templateUrl: './box-model-sidebar.component.html',
  styleUrls: ['./box-model-sidebar.component.scss']
})
export class BoxModelSidebarComponent {

}
