import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxModelSidebarComponent } from './box-model-sidebar.component';

describe('BoxModelSidebarComponent', () => {
  let component: BoxModelSidebarComponent;
  let fixture: ComponentFixture<BoxModelSidebarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoxModelSidebarComponent]
    });
    fixture = TestBed.createComponent(BoxModelSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
