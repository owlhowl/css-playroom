import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {select, Store} from "@ngrx/store";
import * as BoxModelSelectors from "../../box-model/store/box-model.selectors"
import * as BoxModelActions from "../../box-model/store/box-model.actions"
import {Observable} from "rxjs";
import {BoxModelSidebarNavTabComponent} from "../box-model-sidebar-nav-tab/box-model-sidebar-nav-tab.component";

@Component({
  selector: 'app-box-model-sidebar-nav',
  standalone: true,
  imports: [CommonModule, BoxModelSidebarNavTabComponent],
  templateUrl: './box-model-sidebar-nav.component.html',
  styleUrls: ['./box-model-sidebar-nav.component.scss']
})
export class BoxModelSidebarNavComponent implements OnInit {
  private store = inject(Store);
  showCodeContainer$!: Observable<boolean>;

  ngOnInit() {
    this.showCodeContainer$ = this.store.pipe(select(BoxModelSelectors.selectShowCodeContainer));
  }

  resetDefaults() {
    this.store.dispatch(BoxModelActions.resetDefaults());
  }

  toggleCodeContainer() {
    this.store.dispatch(BoxModelActions.toggleCodeContainer());
  }
}
