import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxModelSidebarNavComponent } from './box-model-sidebar-nav.component';

describe('BoxModelSidebarNavComponent', () => {
  let component: BoxModelSidebarNavComponent;
  let fixture: ComponentFixture<BoxModelSidebarNavComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoxModelSidebarNavComponent]
    });
    fixture = TestBed.createComponent(BoxModelSidebarNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
