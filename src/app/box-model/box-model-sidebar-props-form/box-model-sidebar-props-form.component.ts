import {Component, inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { CommonModule } from '@angular/common';
import {BoxModelState, boxSizing} from "../store/box-model.reducer";
import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {Store} from "@ngrx/store";
import {debounceTime, Subject, takeUntil, tap} from "rxjs";
import {kebabize} from "../../shared/utils/kebabize";
import * as BoxModelActions from "../../box-model/store/box-model.actions";
import {
  FlexboxSidebarContainerSelectComponent
} from "../../flexbox/flexbox-sidebar-container-select/flexbox-sidebar-container-select.component";
import {FormControlPipe} from "../../shared/pipes/form-control.pipe";
import {TimeUnitPickerComponent} from "../../shared/ui/time-unit-picker/time-unit-picker.component";
import {UnitPickerComponent} from "../../shared/ui/unit-picker/unit-picker.component";
import {borderStyles} from "../../shared/models/border";

@Component({
  selector: 'app-box-model-sidebar-props-form',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, FlexboxSidebarContainerSelectComponent, FormControlPipe, TimeUnitPickerComponent, UnitPickerComponent],
  templateUrl: './box-model-sidebar-props-form.component.html',
  styleUrls: ['./box-model-sidebar-props-form.component.scss']
})
export class BoxModelSidebarPropsFormComponent implements OnChanges, OnInit, OnDestroy  {
  private fb = inject(FormBuilder);
  private store = inject(Store);

  @Input() boxModelState!: BoxModelState;

  boxModelForm!: FormGroup;
  boxSizing = [...boxSizing];
  borderStyles = [...borderStyles];
  destroy$: Subject<void> = new Subject();
  kebabize = kebabize;

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnInit() {
    this.boxModelForm = this.fb.group({
      boxSizing: [this.boxModelState.boxSizing],
      content: this.fb.group({
        width: this.fb.group({
          value: [this.boxModelState.content.width.value],
          unit: [this.boxModelState.content.width.unit],
        }),
        height: this.fb.group({
          value: [this.boxModelState.content.height.value],
          unit: [this.boxModelState.content.height.unit],
        }),
      }),
      padding: this.fb.group({
        top: this.fb.group({
          value: [this.boxModelState.padding.top.value],
          unit: [this.boxModelState.padding.top.unit],
        }),
        right: this.fb.group({
          value: [this.boxModelState.padding.right.value],
          unit: [this.boxModelState.padding.right.unit],
        }),
        bottom: this.fb.group({
          value: [this.boxModelState.padding.bottom.value],
          unit: [this.boxModelState.padding.bottom.unit],
        }),
        left: this.fb.group({
          value: [this.boxModelState.padding.left.value],
          unit: [this.boxModelState.padding.left.unit],
        }),
      }),
      border: this.fb.group({
        top: this.fb.group({
          size: this.fb.group({
            value: [this.boxModelState.border.top.size.value],
            unit: [this.boxModelState.border.top.size.unit],
          }),
          style: [this.boxModelState.border.top.style],
          color: [this.boxModelState.border.top.color]
        }),
        right: this.fb.group({
          size: this.fb.group({
            value: [this.boxModelState.border.right.size.value],
            unit: [this.boxModelState.border.right.size.unit],
          }),
          style: [this.boxModelState.border.right.style],
          color: [this.boxModelState.border.right.color]
        }),
        bottom: this.fb.group({
          size: this.fb.group({
            value: [this.boxModelState.border.bottom.size.value],
            unit: [this.boxModelState.border.bottom.size.unit],
          }),
          style: [this.boxModelState.border.bottom.style],
          color: [this.boxModelState.border.bottom.color]
        }),
        left: this.fb.group({
          size: this.fb.group({
            value: [this.boxModelState.border.left.size.value],
            unit: [this.boxModelState.border.left.size.unit],
          }),
          style: [this.boxModelState.border.left.style],
          color: [this.boxModelState.border.left.color]
        }),
      }),
      margin: this.fb.group({
        top: this.fb.group({
          value: [this.boxModelState.margin.top.value],
          unit: [this.boxModelState.margin.top.unit],
        }),
        right: this.fb.group({
          value: [this.boxModelState.margin.right.value],
          unit: [this.boxModelState.margin.right.unit],
        }),
        bottom: this.fb.group({
          value: [this.boxModelState.margin.bottom.value],
          unit: [this.boxModelState.margin.bottom.unit],
        }),
        left: this.fb.group({
          value: [this.boxModelState.margin.left.value],
          unit: [this.boxModelState.margin.left.unit],
        }),
      }),
    });

    this.boxModelForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(500),
      tap((formValue) => {
        this.store.dispatch(BoxModelActions.setBoxModel({value: formValue}));
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getFormGroup(name: string) {
    return this.boxModelForm.get(name) as FormGroup;
  }

  getFormControl(name: string) {
    return this.boxModelForm.get(name) as FormControl;
  }
}
