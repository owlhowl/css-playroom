import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxModelSidebarPropsFormComponent } from './box-model-sidebar-props-form.component';

describe('BoxModelSidebarPropsFormComponent', () => {
  let component: BoxModelSidebarPropsFormComponent;
  let fixture: ComponentFixture<BoxModelSidebarPropsFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoxModelSidebarPropsFormComponent]
    });
    fixture = TestBed.createComponent(BoxModelSidebarPropsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
