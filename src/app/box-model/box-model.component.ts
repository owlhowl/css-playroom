import {Component, inject, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {CodeContainerComponent} from "../shared/ui/code-container/code-container.component";
import {BoxModelSidebarComponent} from "./box-model-sidebar/box-model-sidebar.component";
import {BoxModelContainerComponent} from "./box-model-container/box-model-container.component";
import {Store} from "@ngrx/store";
import {CodeService} from "../shared/services/code.service";
import {Observable} from "rxjs";
import * as BoxModelSelectors from "../box-model/store/box-model.selectors";
import * as BoxModelActions from "../box-model/store/box-model.actions";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-box-model',
  standalone: true,
  imports: [CommonModule, CodeContainerComponent, BoxModelSidebarComponent, BoxModelContainerComponent],
  templateUrl: './box-model.component.html',
  styleUrls: ['./box-model.component.scss']
})
export class BoxModelComponent implements OnInit, OnDestroy {
  private titleService = inject(Title);
  private metaTagService = inject(Meta);
  private store = inject(Store);
  private codeService = inject(CodeService);

  showCodeContainer$!: Observable<boolean>;
  boxModelHtml$ = this.codeService.boxModelHtml$;
  boxModelCss$ = this.codeService.boxModelCss$;

  ngOnInit() {
    this.titleService.setTitle('CSS Playroom - Box Model');
    this.metaTagService.addTags([
      {name: 'description', content: 'cssplayroom.com is your first and best source for the css information you’re looking for. Understand the css box model by editing its components'},
      {name: 'keywords', content: 'CSS, CSS playground, Css playroom, Box Model, Angular, NgRx, State Management, Css Code, Git, Git Code'}
    ]);
    this.showCodeContainer$ = this.store.select(BoxModelSelectors.selectShowCodeContainer);
  }

  ngOnDestroy() {
    this.store.dispatch(BoxModelActions.resetDefaults());
  }
}
