import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxModelSidebarNavTabComponent } from './box-model-sidebar-nav-tab.component';

describe('BoxModelSidebarNavTabComponent', () => {
  let component: BoxModelSidebarNavTabComponent;
  let fixture: ComponentFixture<BoxModelSidebarNavTabComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoxModelSidebarNavTabComponent]
    });
    fixture = TestBed.createComponent(BoxModelSidebarNavTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
