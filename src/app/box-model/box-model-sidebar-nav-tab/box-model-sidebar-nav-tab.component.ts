import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-box-model-sidebar-nav-tab',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './box-model-sidebar-nav-tab.component.html',
  styleUrls: ['./box-model-sidebar-nav-tab.component.scss']
})
export class BoxModelSidebarNavTabComponent {
  @Input() name!: string;
  @Input() isActive!: boolean;
}
