import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxModelSidebarPropsComponent } from './box-model-sidebar-props.component';

describe('BoxModelSidebarPropsComponent', () => {
  let component: BoxModelSidebarPropsComponent;
  let fixture: ComponentFixture<BoxModelSidebarPropsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoxModelSidebarPropsComponent]
    });
    fixture = TestBed.createComponent(BoxModelSidebarPropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
