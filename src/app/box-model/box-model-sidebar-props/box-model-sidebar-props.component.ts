import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import * as BoxModelSelectors from "../../box-model/store/box-model.selectors";
import {
  BoxModelSidebarPropsFormComponent
} from "../box-model-sidebar-props-form/box-model-sidebar-props-form.component";
import {BoxModelState} from "../store/box-model.reducer";

@Component({
  selector: 'app-box-model-sidebar-props',
  standalone: true,
  imports: [CommonModule, BoxModelSidebarPropsFormComponent],
  templateUrl: './box-model-sidebar-props.component.html',
  styleUrls: ['./box-model-sidebar-props.component.scss']
})
export class BoxModelSidebarPropsComponent {
  boxModelState$!: Observable<BoxModelState>;
  private store = inject(Store);

  ngOnInit() {
    this.boxModelState$ = this.store.pipe(select(BoxModelSelectors.selectBoxModel));
  }
}
