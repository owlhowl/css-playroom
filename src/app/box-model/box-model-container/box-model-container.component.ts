import {Component, inject, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {BoxModelState} from "../store/box-model.reducer";
import * as BoxModelSelectors from "../store/box-model.selectors";

@Component({
  selector: 'app-box-model-container',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './box-model-container.component.html',
  styleUrls: ['./box-model-container.component.scss']
})
export class BoxModelContainerComponent implements OnInit {
  private store = inject(Store);

  boxModel$!: Observable<BoxModelState>;

  ngOnInit() {
    this.boxModel$ = this.store.select(BoxModelSelectors.selectBoxModel)
  }
}
