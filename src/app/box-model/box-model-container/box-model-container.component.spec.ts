import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxModelContainerComponent } from './box-model-container.component';

describe('BoxModelContainerComponent', () => {
  let component: BoxModelContainerComponent;
  let fixture: ComponentFixture<BoxModelContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoxModelContainerComponent]
    });
    fixture = TestBed.createComponent(BoxModelContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
