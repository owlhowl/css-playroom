## Angular CSS Playground with NgRx State Management

This Angular project was build with the aim of leveling up my skills in NgRx state management while offering a fun and interactive CSS playground.
It provides an excellent opportunity to delve into NgRx and its intricacies, all within the context of experimenting with creative styling techniques.

If you've somehow found your way here and have any questions, run into issues, or have exciting proposals for this project, please don't hesitate 
to reach out by creating an issue in this repository. Your friendly feedback is warmly welcomed and will greatly contribute to this

_P.S. In some places the code might not be very beautiful, due to my laziness and "leaving optimizations for later"._ :)
